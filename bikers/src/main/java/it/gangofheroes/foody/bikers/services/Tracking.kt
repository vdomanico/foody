package it.gangofheroes.foody.bikers.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationCallback
import android.content.pm.PackageManager
import android.Manifest.permission.ACCESS_FINE_LOCATION
import androidx.core.content.ContextCompat
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationRequest
import it.gangofheroes.foody.bikers.util.FirebaseDB


class Tracking : Service() {

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate")
        //buildNotification()
        requestLocationUpdates()
    }

    private fun requestLocationUpdates() {
        val request = LocationRequest()
        request.interval = INTERVAL
        request.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        val client = LocationServices.getFusedLocationProviderClient(this)

        val permission = ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION)
        if (permission == PackageManager.PERMISSION_GRANTED) {
            client.requestLocationUpdates(request, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    val location = locationResult!!.lastLocation
                    location?.let {
                        Log.d(TAG, "Update Location on Firebase")
                        FirebaseDB.getInstance.updateUserPosition(it)
                    }
                }
            }, null)
        }
    }

    companion object {
        const val TAG = "Tracking"
        const val INTERVAL = 50000L
    }
}
