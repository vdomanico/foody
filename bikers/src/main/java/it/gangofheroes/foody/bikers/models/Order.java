package it.gangofheroes.foody.bikers.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Order implements Parcelable {

    public Order(){}

    private String orderId;
    private String restaurateurId;
    private String delivererId;
    private String customerId;
    private String restaurantAddress;
    private String customerName;
    private String customerSurname;
    private String customerAddress;
    private String customerPhone;
    private OrderState orderState = OrderState.DELIVERED;
    private String notes;
    private Long deliveryTime;
    private Long orderDate;

    protected Order(Parcel in) {
        orderId = in.readString();
        restaurateurId = in.readString();
        delivererId = in.readString();
        customerId = in.readString();
        restaurantAddress = in.readString();
        customerName = in.readString();
        customerSurname = in.readString();
        customerAddress = in.readString();
        customerPhone = in.readString();
        notes = in.readString();
        if (in.readByte() == 0) {
            deliveryTime = null;
            orderDate = null;
        } else {
            orderDate = in.readLong();
            deliveryTime = in.readLong();
        }
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRestaurateurId() {
        return restaurateurId;
    }

    public void setRestaurateurId(String restaurateurId) {
        this.restaurateurId = restaurateurId;
    }

    public String getDelivererId() {
        return delivererId;
    }

    public void setDelivererId(String delivererId) {
        this.delivererId = delivererId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public Long getOrderDate() {
        return orderDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(restaurateurId);
        dest.writeString(delivererId);
        dest.writeString(customerId);
        dest.writeString(restaurantAddress);
        dest.writeString(customerName);
        dest.writeString(customerSurname);
        dest.writeString(customerAddress);
        dest.writeString(customerPhone);
        dest.writeString(notes);
        if (deliveryTime == null) {
            dest.writeByte((byte) 0);
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(orderDate);
            dest.writeLong(deliveryTime);
        }
    }

    public static class builder {

        Order order = new Order();

        public Order build() {
            return order;
        }

        public builder setOrderId(String orderId){
            order.setOrderId(orderId);
            return this;
        }

        public builder setRestaurateurId(String restaurateurId) {
            order.setRestaurateurId(restaurateurId);
            return this;
        }

        public builder setCustomerId(String customerId) {
            order.setCustomerId(customerId);
            return this;
        }

        public builder setDelivererId(String uid) {
            order.setDelivererId(uid);
            return this;
        }
    }
}
