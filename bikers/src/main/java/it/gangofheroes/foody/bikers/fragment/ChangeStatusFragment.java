package it.gangofheroes.foody.bikers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import it.gangofheroes.foody.bikers.R;
import it.gangofheroes.foody.bikers.models.Biker;

public class ChangeStatusFragment extends DialogFragment {

    public static final String TAG = ChangeStatusFragment.class.getCanonicalName();
    private ChangeStatusFragmentListener mListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_Dialog_Alert);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.change_status_fragment, container, false);

        view.findViewById(R.id.aviable).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on aviable");
                mListener.changeStatus(Biker.BIKER_STATUS.AVAILABLE);
            }
        });
        view.findViewById(R.id.delivering).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on delivering");
                mListener.changeStatus(Biker.BIKER_STATUS.DELIVERING);
            }
        });
        view.findViewById(R.id.busy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on delivering");
                mListener.changeStatus(Biker.BIKER_STATUS.BUSY);
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        if (context instanceof ChangeStatusFragmentListener){
            mListener = (ChangeStatusFragmentListener) context;
        }
    }

    public static ChangeStatusFragment newInstance() {
        return new ChangeStatusFragment();
    }

    public interface ChangeStatusFragmentListener {
        void changeStatus(Biker.BIKER_STATUS newStatus);
    }
}
