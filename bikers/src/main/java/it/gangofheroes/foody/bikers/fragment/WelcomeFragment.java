package it.gangofheroes.foody.bikers.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.picasso.Picasso;

import java.util.LinkedList;
import java.util.List;

import it.gangofheroes.foody.bikers.R;
import it.gangofheroes.foody.bikers.adapter.OrderAdapter;
import it.gangofheroes.foody.bikers.models.Biker;
import it.gangofheroes.foody.bikers.models.Firebase;
import it.gangofheroes.foody.bikers.models.Order;
import it.gangofheroes.foody.bikers.models.UserType;
import it.gangofheroes.foody.bikers.util.DownloadAvatarUtil;

import static it.gangofheroes.foody.bikers.models.Firebase.Bikers.DELIVERER_ID;
import static it.gangofheroes.foody.bikers.models.Firebase.Orders.INTRO;

public class WelcomeFragment extends Fragment implements OrderAdapter.OrderAdapterCallback,
        NavigationView.OnNavigationItemSelectedListener {

    public static final String TAG = WelcomeFragment.class.getCanonicalName();

    private static final String KEY_USER = "KEY_USER";
    private static final String KEY_BIKER = "KEY_BIKER";

    private WelcomeFragmentInteractionListener mListener;
    private FirebaseUser mUser;
    private Biker mBiker;
    private DatabaseReference mDatabase;
    private DatabaseReference orderRef;
    private Location mylocation;
    private GoogleApiClient googleApiClient;

    private Button btnStatus;
    private TextView tvNickName;
    private TextView tvTotDel;
    private TextView distance;
    private TextView name;
    private TextView headerName;
    private TextView tvTodayDel;
    private TextView pendingOrders;
    private ImageView userImage;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView currentStatus;
    private ImageView imageView;
    private List<Order> newOrders = new LinkedList<>();
    private List<Order> orders = new LinkedList<>();
    private OrderAdapter orderAdapter;
    private FragmentActivity fragmentActivity;

    public static WelcomeFragment newInstance(FirebaseUser mUser, Biker currentUserInfo) {
        WelcomeFragment fragment = new WelcomeFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_USER, mUser);
        args.putParcelable(KEY_BIKER, currentUserInfo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        orderRef = FirebaseDatabase.getInstance().getReference(INTRO);
        if (getArguments() != null) {
            mUser = getArguments().getParcelable(KEY_USER);
            mBiker = getArguments().getParcelable(KEY_BIKER);
            updateRegistrationToken();
        }
        getAssignedOrders();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        mListener.updateUserStatus(Biker.BIKER_STATUS.AVAILABLE);
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");

        /* Name */
        name = view.findViewById(R.id.tvName);

        /* NickName */
        tvNickName = view.findViewById(R.id.tvNickName);
        setNickname();

        pendingOrders = view.findViewById(R.id.pendingOrders);

        tvTotDel = view.findViewById(R.id.tvTotDel);
        tvTodayDel = view.findViewById(R.id.tvTodayDel);
        setupDeliveries();

        /* RV */
        setupAdapter(view);

        /* Navigation View */
        drawerLayout = view.findViewById(R.id.drawer_layout);
        navigationView = view.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        /* Menu */
        ImageView menu = view.findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Open side menù");
                drawerLayout.openDrawer(Gravity.LEFT);
            }
        });

        /* User Image */
        userImage = view.findViewById(R.id.userImage);
        imageView = header.findViewById(R.id.imageView);
        currentStatus = header.findViewById(R.id.currentStatus);
        DownloadAvatarUtil.downloadUserAvatar(mUser.getUid(), UserType.BIKER, userImage, fragmentActivity, WelcomeFragment.TAG);
        DownloadAvatarUtil.downloadUserAvatar(mUser.getUid(), UserType.BIKER, imageView, fragmentActivity, WelcomeFragment.TAG);

        /* Tot Distance */
        distance = header.findViewById(R.id.distance);

        /* Status */
        btnStatus = view.findViewById(R.id.btnStatus);
        btnStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on btn Status");
                mListener.changeStatus();
            }
        });
        currentStatus.setText(String.format(getString(R.string.user_status),Biker.BIKER_STATUS.AVAILABLE.toString()));

        headerName = header.findViewById(R.id.tvName);
        setUserName();
        setUserTotDistance();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        if (context instanceof WelcomeFragmentInteractionListener) {
            mListener = (WelcomeFragmentInteractionListener) context;
        }
        fragmentActivity = (FragmentActivity) context;
    }

    private void getAssignedOrders() {
        Log.d(TAG, "getAssignedOrders");

        Query query = orderRef.orderByChild(DELIVERER_ID);
        query.equalTo(mUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                newOrders.clear();
                WelcomeFragment.this.orders.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    Order order = child.getValue(Order.class);
                    if (order != null) {
                        switch (order.getOrderState()) {
                            case ASSIGNED:
                            case DELIVERING:
                            case ACCEPTED:
                                newOrders.add(order);
                                break;
                            case DELIVERED:
                                WelcomeFragment.this.orders.add(order);
                        }
                        orderAdapter.notifyDataSetChanged();
                    }
                }
                if (newOrders.size() > 0) {
                    pendingOrders.setText(R.string.pending_orders);
                } else {
                    pendingOrders.setText(R.string.no_pending_orders);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }

    private void setNickname() {
        Log.d(TAG, "setNickname");
        mDatabase.child(Firebase.Bikers.BIKERS)
                .child(mUser.getUid())
                .child(Firebase.Bikers.NICKNAME)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            tvNickName.setText((dataSnapshot.getValue(String.class)));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
    }

    private void setUserImage(Uri uri) {
        Picasso.get().load(uri).into(userImage);
        Picasso.get().load(uri).into(imageView);
    }

    private void setUserName() {
        name.setText(String.format("%s %s", mBiker.getBikerName(), mBiker.getBikerSurname()));
        headerName.setText(String.format("%s %s", mBiker.getBikerName(), mBiker.getBikerSurname()));
    }

    private void setUserTotDistance(){
        Log.d(TAG, "setUserTotDistance");
        mDatabase.child(Firebase.Bikers.BIKERS)
                .child(mUser.getUid())
                .child(Firebase.Bikers.TOT_DISTANCE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue(Float.class) != null){
                            Float totDistance = dataSnapshot.getValue(Float.class);
                            distance.setText(String.format(getString(R.string.tot_distance), totDistance));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) { }
                });
    }

    private void setupAdapter(View view) {
        RecyclerView recyclerView = view.findViewById(R.id.rvContainer);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        orderAdapter = new OrderAdapter(newOrders);
        orderAdapter.setListener(this);

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(orderAdapter);
    }

    private void updateRegistrationToken() {
        Log.d(TAG, "updateRegistrationToken");
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.d(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        if (task.getResult() != null) {
                            final String token = task.getResult().getToken();
                            Log.d(TAG, token);
                            FirebaseDatabase
                                    .getInstance()
                                    .getReference(Firebase.Bikers.BIKERS)
                                    .child(mUser.getUid() + "/tokens/" + token).setValue(true);
                        }

                    }
                });
    }

    private void setupDeliveries() {
        mDatabase.child(Firebase.Bikers.BIKERS)
                .child(mUser.getUid())
                .addValueEventListener(new ValueEventListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Long totDel = dataSnapshot.child(Firebase.Bikers.TOTAL_DELIVERIES).getValue(Long.class);
                        Long todayDel = dataSnapshot.child(Firebase.Bikers.TODAY_DELIVERIES).getValue(Long.class);
                        if (totDel != null) {
                            tvTotDel.setText(totDel.toString());
                        } else {
                            mDatabase.child(Firebase.Bikers.BIKERS)
                                    .child(mUser.getUid())
                                    .child(Firebase.Bikers.TOTAL_DELIVERIES)
                                    .setValue(0);
                        }
                        if (todayDel != null) {
                            tvTodayDel.setText(todayDel.toString());
                        } else {
                            mDatabase.child(Firebase.Bikers.BIKERS)
                                    .child(mUser.getUid())
                                    .child(Firebase.Bikers.TODAY_DELIVERIES)
                                    .setValue(0);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
    }

    public void updateUserStatus(Biker.BIKER_STATUS status) {
        int drawableId = R.drawable.button_aviable;
        switch (status) {
            case AVAILABLE:
                currentStatus.setText(String.format(getString(R.string.user_status),Biker.BIKER_STATUS.AVAILABLE.toString()));
                break;

            case DELIVERING:
                currentStatus.setText(String.format(getString(R.string.user_status),Biker.BIKER_STATUS.DELIVERING.toString()));
                drawableId = R.drawable.button_delivering;
                break;

            case BUSY:
                currentStatus.setText(String.format(getString(R.string.user_status),Biker.BIKER_STATUS.BUSY.toString()));
                drawableId = R.drawable.button_busy;
                break;
        }
        if (getContext() != null) {
            btnStatus.setBackground(getContext().getDrawable(drawableId));
        }
    }

    /**
     * OrderAdapterCallback
     */
    @Override
    public void orderInfo(Order order) {
        Log.d(TAG, "orderInfo");
        mListener.showOrderInfo(order);
    }

    /**
     * NavigationView.OnNavigationItemSelectedListener
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.editProfile:
                mListener.editProfile(getString(R.string.edit_profile));
                break;

            case R.id.logout:
                mListener.logout();
                break;
        }
        return true;
    }

    public interface WelcomeFragmentInteractionListener {

        void updateUserStatus(Biker.BIKER_STATUS userStatus);

        void changeStatus();

        void editProfile(String title);

        void showOrderInfo(Order order);

        void logout();

        Context getContext();
    }
}