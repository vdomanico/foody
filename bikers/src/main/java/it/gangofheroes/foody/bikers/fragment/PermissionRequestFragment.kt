package it.gangofheroes.foody.bikers.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import it.gangofheroes.foody.bikers.R
import kotlinx.android.synthetic.main.permission_request_fragment.*

class PermissionRequestFragment: DialogFragment() {

    private var mListener: PermissionRequestFragmentListener? = null
    private var textInfo: String? = null

    @SuppressLint("LongLogTag")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_Dialog_Alert)
        arguments?.let {
            textInfo = it.getString(KEY_INFO)
        }
    }

    @SuppressLint("LongLogTag")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.permission_request_fragment, container, false  )
    }

    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        ok.setOnClickListener {
            Log.d(TAG, "Click on OK")
            mListener?.onRequestPermissionsClick()
        }
        close.setOnClickListener {
            Log.d(TAG, "Click on close App")
            mListener?.onCloseClicked()
        }
        textInfo?.let {
            tvInfo.text = it
            ok.visibility = View.GONE
            separator.visibility = View.GONE
        }
    }

    @SuppressLint("LongLogTag")
    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttach")
        if (context is PermissionRequestFragmentListener){
            mListener = context
        }
    }

    interface PermissionRequestFragmentListener {
        fun onRequestPermissionsClick()
        fun onCloseClicked()
    }

    companion object {
        const val TAG = "PermissionRequestFragment"

        private const val KEY_INFO = "KEY_INFO"

        fun newInstance(textInfo: String? = null) = PermissionRequestFragment().apply{
            arguments = Bundle().apply {
                putString(KEY_INFO, textInfo)
            }
        }
    }
}