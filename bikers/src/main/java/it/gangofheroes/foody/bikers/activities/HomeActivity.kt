package it.gangofheroes.foody.bikers.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import it.gangofheroes.foody.bikers.R
import it.gangofheroes.foody.bikers.fragment.*
import it.gangofheroes.foody.bikers.models.*
import it.gangofheroes.foody.bikers.services.Tracking
import it.gangofheroes.foody.bikers.util.DownloadAvatarUtil
import it.gangofheroes.foody.bikers.util.FirebaseDB
import kotlinx.android.synthetic.main.home_activity_layout.*
import kotlinx.android.synthetic.main.home_activity_layout.animation
import kotlinx.android.synthetic.main.prelogin_activity.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity: AppCompatActivity(), WelcomeFragment.WelcomeFragmentInteractionListener,
        EditProfileFragment.EditProfileFragmentListener,
        ChangeStatusFragment.ChangeStatusFragmentListener,
        PermissionRequestFragment.PermissionRequestFragmentListener,
        EnableGPSFragment.EnableGPSFragmentListener,
        OrderInfoFragment.OrderInfoFragmentListener {

    private var mUser: FirebaseUser? = null
    private lateinit var mDatabase: DatabaseReference
    private lateinit var mCurrentUser: Biker
    private lateinit var locationManager: LocationManager
    private lateinit var serviceIntent: Intent
    private var mSelectedImage: String? = null

    private var permisssionRequestClick = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setContentView(R.layout.home_activity_layout)

        mUser = FirebaseAuth.getInstance().currentUser
        mDatabase = FirebaseDatabase.getInstance().reference
        serviceGPS()
    }

    override fun onBackPressed() {
        Log.d(TAG, "onBackPressed")
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)

        if (fragment is EditProfileFragment) {
            if (mCurrentUser.isValidUser) {
                super.onBackPressed()
            }
        }

        if (fragment is OrderInfoFragment) {
            super.onBackPressed()
        }

        if (fragment is PermissionRequestFragment
                || fragment is WelcomeFragment
                || fragment is EnableGPSFragment
                || fragment is PermissionRequestFragment){
            if (permisssionRequestClick == 1){
                finishAffinity()
            }
            else {
                Toast.makeText(this, "Double click to exit", Toast.LENGTH_SHORT).show()
                permisssionRequestClick++
                Handler().postDelayed({
                    permisssionRequestClick = 0
                }, DELAY)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy")
        stopService(serviceIntent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        Log.d(TAG, "onRequestPermissionsResult")
        when (requestCode) {
            PERMISSION_REQUEST -> {
                if (grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                    checkUserInfo()
                } else {
                    var textInfo: String? = null
                    if (!shouldShowRequestPermissionRationale(permissions.first())){
                        /* Never ask again checked */
                        textInfo = "Please go to settings and enable GPS permission"
                    }
                    addFragment(PermissionRequestFragment.newInstance(textInfo), PermissionRequestFragment.TAG)
                }
            }

            CODE_PERMISSION_READ_EXTERNAL -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED){
                    uploadPhotoUsingGallery()
                }
            }

            CODE_PERMISSION_CAMERA -> {
                if (grantResults.isNotEmpty() && grantResults.first() == PackageManager.PERMISSION_GRANTED){
                    uploadPhotoUsingCamera()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d(TAG, "onActivityResult")
        when(requestCode){
            PICK_IMAGE -> {
                if (resultCode == Activity.RESULT_OK && data != null) {
                    data.data?.let{
                        setImage(it)
                    }
                }
            }

            PICK_IMAGE_CAMERA -> {
                if (resultCode == Activity.RESULT_OK) {
                    setImage(Uri.parse(mSelectedImage))
                }
            }
        }
    }

    private fun serviceGPS() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            /* Ask to turn on gps */
            addFragment(EnableGPSFragment.newInstance(), EnableGPSFragment.TAG)
        }
        else
        {
            /* Check permissions */
            val permissions = ContextCompat
                    .checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            if (permissions != PackageManager.PERMISSION_GRANTED) {
                /* Request permissions */
                requestGPSPermissions()
            } else {
                checkUserInfo()
            }
        }
    }

    private fun requestGPSPermissions(){
        Log.d(TAG, "requestGPSPermissions")
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST)
    }

    private fun startFragment(fragment: Fragment, tag: String) {
        Log.d(TAG, "startFragment")
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment, tag)
                .addToBackStack(tag)
                .setCustomAnimations(R.anim.enter,
                        R.anim.exit,
                        R.anim.pop_enter,
                        R.anim.pop_exit)
                .commit()
    }

    private fun addFragment(fragment: Fragment, tag: String){
        Log.d(TAG, "addFragment")
        supportFragmentManager
                .beginTransaction()
                .add(fragment, tag)
                .setCustomAnimations(R.anim.enter,
                        R.anim.exit,
                        R.anim.pop_enter,
                        R.anim.pop_exit)
                .addToBackStack(tag)
                .commitAllowingStateLoss()
    }

    private fun checkUserInfo(){
        animation.visibility = View.VISIBLE
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                mCurrentUser = dataSnapshot.getValue(Biker::class.java) ?: Biker()
                animation.visibility = View.GONE
                if (mCurrentUser.isValidUser){
                    FirebaseDB.getInstance.setDatabaseReference(mDatabase)
                    mUser?.let{
                        FirebaseDB.getInstance.setFirebaseUser(it)
                        FirebaseDB.getInstance.saveCurrentDistance()
                        FirebaseDB.getInstance.setBiker(mCurrentUser)
                    }
                    serviceIntent = Intent(context, Tracking::class.java)
                    startService(serviceIntent)
                    startFragment(WelcomeFragment.newInstance(mUser, mCurrentUser), WelcomeFragment.TAG)
                }
                else {
                    editProfile("Please complete your account info")
                }
            }

            override fun onCancelled(databaseError: DatabaseError) { }
        }

        mUser?.let{
            mDatabase.child(Firebase.Bikers.BIKERS)
                    .child(it.uid).addListenerForSingleValueEvent(postListener)
        }
    }

    private fun uploadPhotoUsingCamera() {
        Log.d(TAG, "uploadPhotoUsingCamera")
        if (hasPermission(Manifest.permission.CAMERA)) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                val photoFile: File?
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    val errMessage = "Error loading photo.."
                    Toast.makeText(applicationContext, errMessage, Toast.LENGTH_SHORT).show()
                    return
                }

                val photoURI = FileProvider.getUriForFile(this,
                        "it.gangofheroes.foody.bikers.android.fileprovider",
                        photoFile)
                mSelectedImage = photoURI.toString()
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, PICK_IMAGE_CAMERA)
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CAMERA),
                    CODE_PERMISSION_CAMERA)
        }
    }

    private fun uploadPhotoUsingGallery() {
        Log.d(TAG, "uploadPhotoUsingGallery")
        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            val i = Intent(
                    Intent.ACTION_PICK,
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(i, PICK_IMAGE)
        } else {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    CODE_PERMISSION_READ_EXTERNAL)
        }
    }

    private fun hasPermission(permission: String): Boolean {
        Log.d(TAG, "hasPermission")
        return ContextCompat
                .checkSelfPermission(this,
                        permission) == PackageManager.PERMISSION_GRANTED
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        )
    }

    private fun setImage(uriImg: Uri){
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)

        if (fragment != null && fragment is EditProfileFragment){
            fragment.updatePhoto(uriImg)
        }
        DownloadAvatarUtil.uploadUserAvatar(uriImg, uriImg, mUser?.uid, UserType.BIKER, this, TAG)
    }

    /**
     * WelcomeFragmentInteractionListener
     */
    override fun updateUserStatus(userStatus: Biker.BIKER_STATUS?) {
        Log.d(TAG, "updateUserStatus: " + userStatus.toString())
        mCurrentUser.userStatus = userStatus
        mUser?.let{
            mDatabase.child(Firebase.Bikers.BIKERS)
                    .child(it.uid)
                    .child(Firebase.Bikers.USER_STATUS)
                    .setValue(userStatus.toString())
        }
    }

    override fun changeStatus() {
        Log.d(TAG, "changeStatus")
        addFragment(ChangeStatusFragment.newInstance(), ChangeStatusFragment.TAG)
    }

    override fun editProfile(title: String) {
        Log.d(TAG, "editProfile")
        startFragment(EditProfileFragment.newInstance(title, mCurrentUser), EditProfileFragment.TAG)
    }

    override fun showOrderInfo(order: Order?) {
        Log.d(TAG, "showOrderInfo")
        order?.let {
            startFragment(OrderInfoFragment.newInstance(it), OrderInfoFragment.TAG)
        }
    }

    override fun logout() {
        Log.d(TAG, "logout")
        /* Stop Service */
        stopService(serviceIntent)
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener {
                    Log.d(TAG, "logout completed")
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
    }

    /**
     * OrderInfoFragmentListener
     */
    override fun acceptOrder(order: Order) {
        Log.d(TAG, "acceptOrder")
        FirebaseDB.getInstance.updateOrderStatus(order, OrderState.ACCEPTED)
        updateUserStatus(Biker.BIKER_STATUS.DELIVERING)
    }

    override fun declineOrder(order: Order) {
        Log.d(TAG, "declineOrder")
        FirebaseDB.getInstance.updateOrderStatus(order, OrderState.DECLINED)
        supportFragmentManager.popBackStack()
    }

    override fun setOrderState(order: Order, orderState: OrderState) {
        Log.d(TAG, "setOrderState")
        FirebaseDB.getInstance.updateOrderStatus(order, orderState)
        if (orderState == OrderState.DELIVERED){
            updateUserStatus(Biker.BIKER_STATUS.AVAILABLE)
        }
    }

    override fun startNavigationToRestaurant(order: Order) {
        Log.d(TAG, "startNavigationToRestaurant")
        Log.d(TAG, "startNavigationToCustomer")
        val uri = Uri.parse("google.navigation:q=${order.restaurantAddress}")
        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
        supportFragmentManager.popBackStack()
    }

    override fun startNavigationToCustomer(order: Order) {
        Log.d(TAG, "startNavigationToCustomer")
        val uri = Uri.parse("google.navigation:q=${order.customerAddress}")
        val mapIntent = Intent(Intent.ACTION_VIEW, uri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

    /**
     * ChangeStatusFragmentListener
     */
    override fun changeStatus(newStatus: Biker.BIKER_STATUS?) {
        Log.d(TAG, "changeStatus")
        val fragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)
        if (fragment is WelcomeFragment){
            fragment.updateUserStatus(newStatus)
        }
        updateUserStatus(newStatus)
        supportFragmentManager.popBackStack()
    }

    /**
     * EditProfileFragmentListener
     */
    override fun onEditUser(user: Biker) {
        Log.d(TAG, "onEditUser")
        mUser?.let {
            user.bikerId = it.uid
            mDatabase.child(Firebase.Bikers.BIKERS)
                    .child(it.uid)
                    .setValue(user)
        }
        startFragment(WelcomeFragment.newInstance(mUser, mCurrentUser), WelcomeFragment.TAG)
    }

    override fun showUploadDialog() {
        Log.d(TAG, "showUploadDialog")
        val dialog = AlertDialog.Builder(this)
        dialog.setTitle(getString(R.string.new_photo))
                .setMessage(getString(R.string.new_photo_message))
                .setNegativeButton(getString(R.string.camera)) { _, _ -> uploadPhotoUsingCamera() }
                .setPositiveButton(getString(R.string.gallery)) { _, _ -> uploadPhotoUsingGallery() }
                .create()
                .show()
    }

    /**
     * EnableGPSFragmentListener
     */
    override fun onGPSEnabled() {
        Log.d(TAG, "onGPSEnabled")
        supportFragmentManager.popBackStack()
        serviceGPS()
    }

    /**
     * PermissionRequestFragmentListener
     */
    override fun onRequestPermissionsClick() {
        Log.d(TAG, "onRequestPermissionsClick")
        supportFragmentManager.popBackStack()
        requestGPSPermissions()
    }

    override fun onCloseClicked() {
        Log.d(TAG, "onCloseClicked")
        finishAffinity()
    }

    override fun getContext(): Context {
        return this
    }

    companion object {
        const val TAG = "HomeActivity"

        private val CODE_PERMISSION_READ_EXTERNAL = 500
        private val CODE_PERMISSION_CAMERA = 200
        private val PICK_IMAGE = 900
        private val PICK_IMAGE_CAMERA = 800

        const val PERMISSION_REQUEST = 100
        const val DELAY = 700L
    }
}