package it.gangofheroes.foody.bikers.models;

public enum UserType {
    RESTAURATEUR, CUSTOMER, BIKER
}
