package it.gangofheroes.foody.bikers.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import it.gangofheroes.foody.bikers.R;
import it.gangofheroes.foody.bikers.models.Biker;
import it.gangofheroes.foody.bikers.models.UserType;
import it.gangofheroes.foody.bikers.util.DownloadAvatarUtil;

import static it.gangofheroes.foody.bikers.models.Firebase.Bikers.BIKERS;


public class ShowYourProfileFragment extends Fragment{

    private static int TITLE = R.string.your_profile;
    public static final String TAG = ShowYourProfileFragment.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private ShowYourProfileFragmentEventListener eventListener;
    private FirebaseUser firebaseUser;
    private DatabaseReference bikersRef;
    private TextView tvName;
    private TextView tvSurname;
    private TextView tvMail;
    private TextView tvPhone;
    private ImageView profileIv;

    public interface ShowYourProfileFragmentEventListener{
        void onProfileEdit();
    }

    public static Fragment newInstance() {
        return new ShowYourProfileFragment();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(TITLE);
        eventListener = (ShowYourProfileFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        bikersRef = FirebaseDatabase.getInstance().getReference(BIKERS);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        Log.d(TAG,"onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_showprofile, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        Log.d(TAG,"onPrepareOptionsMenu");
        super.onPrepareOptionsMenu(menu);
        MenuItem menuItem = menu.findItem(R.id.edit_profile_show); // You can change the state of the menu item here if you call getActivity().supportInvalidateOptionsMenu(); somewhere in your code
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        Log.d(TAG,"onOptionsItemSelected");
        if (item.getItemId() == R.id.edit_profile_show) {
            eventListener.onProfileEdit();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_activity, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        tvName = view.findViewById(R.id.name);
        tvSurname = view.findViewById(R.id.surname);
        tvMail = view.findViewById(R.id.mail);
        tvPhone = view.findViewById(R.id.phone_number);
        profileIv = view.findViewById(R.id.profileImage);
        getPreferences();
    }

    private void getPreferences() {
        Log.d(TAG,"getPreferences()");
        DownloadAvatarUtil.downloadUserAvatar(firebaseUser.getUid(),
                UserType.BIKER,
                profileIv,
                fragmentActivity, ShowYourProfileFragment.TAG);
        bikersRef.
                child(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Biker biker = null;
                        if (dataSnapshot.exists()) {
                            biker = dataSnapshot.getValue(Biker.class);
                            if(biker!=null){
                                tvName.setText(biker.getBikerName());
                                tvSurname.setText(biker.getBikerSurname());
                                tvMail.setText(biker.getBikerMail());
                                tvPhone.setText(biker.getBikerPhone());
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }


}
