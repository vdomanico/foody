package it.gangofheroes.foody.bikers.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import it.gangofheroes.foody.bikers.R
import it.gangofheroes.foody.bikers.models.Order
import it.gangofheroes.foody.bikers.models.OrderState
import kotlinx.android.synthetic.main.order_info.*
import java.text.SimpleDateFormat
import java.util.*
import android.content.Intent
import android.net.Uri


class OrderInfoFragment: Fragment(){

    private var mListner: OrderInfoFragmentListener? = null
    private var mOrder: Order? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        arguments?.let {
            mOrder = it.getParcelable(KEY_ORDER)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.order_info, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d(TAG, "onViewCreated")
        mOrder?.let {
            orderState.text = "${getString(R.string.order_state)} ${it.orderState}"
            orderTime.text = "${getString(R.string.order_time)} ${convertLongToTime(it.orderDate)}"
            customerAddress.text = "${getString(R.string.customer_address)} ${it.customerAddress}"
            customerPhone.text = it.customerPhone
            restaurantAddress.text = "${getString(R.string.restaurant_address)} ${it.restaurantAddress}"
            customerPhone.setOnClickListener { _ ->
                Log.d(TAG, "open dialer with customer phone")
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse(it.customerPhone)
                startActivity(intent)
            }

            when (it.orderState) {
                OrderState.NEW -> { /* Not used */ }

                OrderState.ASSIGNED -> {
                    decisionContainer.visibility = View.VISIBLE
                    accept.setOnClickListener {
                        Log.d(TAG, "click on Accept order")
                        mOrder?.let { order ->
                            mListner?.acceptOrder(order)
                            order.orderState = OrderState.ACCEPTED
                            decisionContainer.visibility = View.GONE
                            mapContainer.visibility = View.VISIBLE
                            navigationText.text = "${getString(R.string.start_navigation_to)} restaurant"
                            mapIcon.setOnClickListener {
                                Log.d(TAG, "click on gps icon")
                                mOrder?.let { order ->
                                    mListner?.startNavigationToRestaurant(order)
                                }
                            }
                        }
                    }
                    decline.setOnClickListener {
                        Log.d(TAG, "click on decline")
                        mOrder?.let { order ->
                            mListner?.declineOrder(order)
                        }
                    }
                }

                OrderState.ACCEPTED -> {
                    /* Show map to Restaurant */
                    mapContainer.visibility = View.VISIBLE
                    navigationText.text = "${getString(R.string.start_navigation_to)} restaurant"
                    mapIcon.setOnClickListener {
                        Log.d(TAG, "click on gps icon")
                        mOrder?.let { order ->
                            mListner?.startNavigationToRestaurant(order)
                        }
                    }
                }

                OrderState.DECLINED -> { }

                OrderState.DELIVERING -> {
                    /* Show map to customer */
                    mapContainer.visibility = View.VISIBLE
                    navigationText.text = "${getString(R.string.start_navigation_to)} customer"
                    mapIcon.setOnClickListener {
                        Log.d(TAG, "click on gps icon")
                        mOrder?.let { order ->
                            mListner?.startNavigationToCustomer(order)
                            navigationText.text = getString(R.string.order_as_delivered)
                            mapIcon.setImageDrawable(context?.getDrawable(R.drawable.ic_box))
                            mapIcon.setOnClickListener {
                                Log.d(TAG, "order delivered")
                                mListner?.setOrderState(order, OrderState.DELIVERED)
                            }
                        }
                    }
                }

                OrderState.DELIVERED -> { }

                else -> { }
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttach")
        if (context is OrderInfoFragmentListener){
            mListner = context
        }
    }

    /**
     * Private fun
     */
    @SuppressLint("SimpleDateFormat")
    fun convertLongToTime(time: Long): String {
        val date = Date(time)
        val format = SimpleDateFormat(DATE_FORMAT)
        return format.format(date)
    }

    interface OrderInfoFragmentListener {
        fun acceptOrder(order: Order)
        fun declineOrder(order: Order)
        fun startNavigationToRestaurant(order: Order)
        fun startNavigationToCustomer(order: Order)
        fun setOrderState(order: Order, orderState: OrderState)
    }

    companion object {
        const val TAG = "OrderInfoFragment"
        private const val KEY_ORDER = "KEY_ORDER"
        private const val DATE_FORMAT = "dd/MM/yyyy HH:mm"

        fun newInstance(order: Order) = OrderInfoFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_ORDER, order)
            }
        }
    }
}