package it.gangofheroes.foody.bikers.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

import it.gangofheroes.foody.bikers.R;

import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.SHARED_PREF_KEY;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.SHARED_PREF_KEY_TEMP;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.URI_IMAGE;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_DESCRIPTION;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_MAIL;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_NAME;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_PHONE;

public class EditProfileActivity extends AppCompatActivity {
    public static final String TAG = "EditProfileActivity";

    EditText etName;
    EditText etMail;
    EditText etDesc;
    EditText etPhone;
    ImageView profilePhoto;

    private static final int CODE_PERMISSION_READ_EXTERNAL = 100;
    private static final int CODE_PERMISSION_CAMERA = 200;
    private static final int PICK_IMAGE = 900;
    private static final int PICK_IMAGE_CAMERA = 800;

    private static String mSelectedImage;
    private boolean objSaved;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate");
        setContentView(R.layout.edit_profile_activity);

        setView();
        if (getSharedPreferences(SHARED_PREF_KEY_TEMP, MODE_PRIVATE).getAll().size() > 0) {
            getValues(SHARED_PREF_KEY_TEMP);
        } else {
            getValues(SHARED_PREF_KEY);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult");

        switch (requestCode) {
            case CODE_PERMISSION_READ_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    uploadPhotoUsingGallery();
                break;

            case CODE_PERMISSION_CAMERA:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    uploadPhotoUsingCamera();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.edit_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.switchMode:
                Log.d(TAG, "Click on Home Menu");
                saveData();
                finish();
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.d(TAG, "onActivityResult");
        switch (requestCode) {
            case PICK_IMAGE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    setImage(Objects.requireNonNull(data.getData()));
                }
                break;

            case PICK_IMAGE_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    setImage(Uri.parse(mSelectedImage));
                }
                else if (resultCode == Activity.RESULT_CANCELED){
                    mSelectedImage = "";
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        if (!objSaved) {
            saveSession();
        }
    }

    /**
     * Private fun
     */
    private void setView() {
        Log.d(TAG, "setView");

        etName = findViewById(R.id.etName);
        etMail = findViewById(R.id.etMail);
        //etDesc = findViewById(R.id.etDesc);
        etPhone = findViewById(R.id.etPhone);
        profilePhoto = findViewById(R.id.iconImage);

        findViewById(R.id.iconImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Set new Image - Show dialog");

                AlertDialog.Builder dialog = new AlertDialog.Builder(EditProfileActivity.this);
                dialog.setTitle(getString(R.string.new_photo))
                        .setMessage(getString(R.string.new_photo_message))
                        .setNegativeButton(getString(R.string.camera), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                uploadPhotoUsingCamera();
                            }
                        })
                        .setPositiveButton(getString(R.string.gallery), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                uploadPhotoUsingGallery();
                            }
                        }).create().show();
            }
        });

        /*findViewById(R.id.btnClearData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on Clear Data");
                clearData(SHARED_PREF_KEY, SHARED_PREF_KEY_TEMP);
                objSaved = true;
                finish();
            }
        });*/
    }

    private void getValues(String key) {
        Log.d(TAG, "getValues");

        SharedPreferences mPref = getSharedPreferences(key, MODE_PRIVATE);
        String name = mPref.getString(USER_NAME, "");
        String mail = mPref.getString(USER_MAIL, "");
        String desc = mPref.getString(USER_DESCRIPTION, "");
        String phone = mPref.getString(USER_PHONE, "");
        mSelectedImage = mPref.getString(URI_IMAGE, "");

        etName.setText(name);
        etMail.setText(mail);
        etDesc.setText(desc);
        etPhone.setText(phone);

        if (mSelectedImage != null && !mSelectedImage.isEmpty()) {
            setImage(Uri.parse(mSelectedImage));
        }
    }

    private void saveData() {
        Log.d(TAG, "saveData");

        /* Clear Temp */
        clearData(SHARED_PREF_KEY_TEMP);

        objSaved = true;

        SharedPreferences.Editor mPref = getSharedPreferences(SHARED_PREF_KEY, MODE_PRIVATE).edit();
        mPref.putString(USER_NAME, etName.getText().toString());
        mPref.putString(URI_IMAGE, mSelectedImage != null ? mSelectedImage : "");
        mPref.putString(USER_MAIL, etMail.getText().toString());
        mPref.putString(USER_DESCRIPTION, etDesc.getText().toString());
        mPref.putString(USER_PHONE, etPhone.getText().toString());
        mPref.apply();
    }

    private void clearData(String... dataKey) {
        Log.d(TAG, "clearData");
        for (String key : dataKey) {
            getSharedPreferences(key, MODE_PRIVATE).edit().clear().apply();
        }
    }

    private void saveSession() {
        Log.d(TAG, "saveSession");
        clearData(SHARED_PREF_KEY_TEMP);
        SharedPreferences.Editor mPref = getSharedPreferences(SHARED_PREF_KEY_TEMP, MODE_PRIVATE).edit();
        mPref.putString(USER_NAME, etName.getText().toString());
        mPref.putString(USER_MAIL, etMail.getText().toString());
        mPref.putString(USER_DESCRIPTION, etDesc.getText().toString());
        mPref.putString(USER_PHONE, etPhone.getText().toString());
        mPref.putString(URI_IMAGE, mSelectedImage);
        mPref.apply();
    }

    private void uploadPhotoUsingCamera() {
        Log.d(TAG, "uploadPhotoUsingCamera");
        if (hasPermission(Manifest.permission.CAMERA)) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    String error_message = "Error loading photo..";
                    Toast.makeText(getApplicationContext(), error_message, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(this,
                            "it.gangofheroes.foody.bikers.android.fileprovider",
                            photoFile);
                    mSelectedImage = photoURI.toString();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, PICK_IMAGE_CAMERA);
                }
            }
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA},
                    CODE_PERMISSION_CAMERA);
        }
    }

    private void uploadPhotoUsingGallery() {
        Log.d(TAG, "uploadPhotoUsingGallery");
        if (hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE)) {
            Intent i = new Intent(
                    Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, PICK_IMAGE);
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    CODE_PERMISSION_READ_EXTERNAL);
        }
    }

    private boolean hasPermission(String permission) {
        Log.d(TAG, "hasPermission");
        return ContextCompat
                .checkSelfPermission(getApplicationContext(),
                        permission) == PackageManager.PERMISSION_GRANTED;
    }

    private void setImage(Uri selectedImage) {
        mSelectedImage = selectedImage.toString();
        profilePhoto.setImageURI(selectedImage);
    }

    private File createImageFile() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", java.util.Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                imageFileName,          /* prefix */
                ".jpg",           /* suffix */
                storageDir              /* directory */
        );
    }
}
