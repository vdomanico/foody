package it.gangofheroes.foody.bikers.fragment

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import it.gangofheroes.foody.bikers.R
import it.gangofheroes.foody.bikers.models.Biker
import it.gangofheroes.foody.bikers.models.UserType
import it.gangofheroes.foody.bikers.util.DownloadAvatarUtil
import kotlinx.android.synthetic.main.edit_profile_fragment.*
import kotlinx.android.synthetic.main.edit_profile_fragment.view.*

class EditProfileFragment: Fragment(){

    private lateinit var title: String
    private lateinit var biker: Biker

    private var mListener: EditProfileFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        title = arguments?.getString(TITLE) ?: "Update your info"
        biker = arguments?.getParcelable(BIKER) ?: Biker()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.edit_profile_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        view.title.text = title

        biker.bikerName?.let {
            etName.setText(it)
        }

        biker.bikerSurname?.let {
            etSurname.setText(it)
        }

        biker.bikerMail?.let {
            etMail.setText(it)
        }

        biker.bikerPhone?.let {
            etPhone.setText(it)
        }

        biker.nickName?.let{
            etNickname.setText(it)
        }

        btnSaveData.setOnClickListener {
            validateAndSave()
        }

        DownloadAvatarUtil.downloadUserAvatar(biker.bikerId, UserType.BIKER, iconImage, context, TAG)
        iconImage.setOnClickListener {
            Log.d(TAG, "Show dialog")
            mListener?.showUploadDialog()
        }
    }

    fun updatePhoto(uri: Uri){
        iconImage.setImageURI(uri)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EditProfileFragmentListener){
            mListener = context
        }
    }

    private fun validateAndSave(){
        Log.d(TAG, "validateAndSave")
        if (!etName.text.isNullOrEmpty()
                && !etSurname.text.isNullOrEmpty()
                && !etMail.text.isNullOrEmpty()
                && !etPhone.text.isNullOrEmpty()){
            biker.bikerName = etName.text.toString()
            biker.bikerSurname = etSurname.text.toString()
            biker.bikerMail = etMail.text.toString()
            biker.bikerPhone = etPhone.text.toString()
            if (etNickname.text.toString().isNotEmpty()){
                biker.nickName = etNickname.text.toString()
            }
            mListener?.onEditUser(biker)
        }
    }

    interface EditProfileFragmentListener {
        fun onEditUser(user: Biker)
        fun showUploadDialog()
    }

    companion object {
        const val TAG = "EditProfileFragment"

        private const val TITLE = "KEY_TITLE"
        private const val BIKER = "KEY_BIKER"

        fun newInstance(title: String, biker: Biker) = EditProfileFragment().apply {
            arguments = Bundle().apply {
                putString(TITLE, title)
                putParcelable(BIKER, biker)
            }
        }
    }
}