package it.gangofheroes.foody.bikers.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import it.gangofheroes.foody.bikers.R;
import it.gangofheroes.foody.bikers.fragment.CheckConnectionFragment;
import it.gangofheroes.foody.bikers.fragment.EditYourProfileFragment;
import it.gangofheroes.foody.bikers.fragment.LoginFragment;

public class LoginActivity
        extends AppCompatActivity
        implements LoginFragment.LoginFragmentInteractionListener,
        CheckConnectionFragment.CheckConnectionFragmentListener,
        EditYourProfileFragment.EditYourProfileFragmentEventListener {

    private static final String TAG = LoginActivity.class.getCanonicalName();

    private FirebaseUser mUser;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.login_activity);
        checkConnection();
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment == null){
            finishAffinity();
        }
    }

    private void startFragment(Fragment fragment, String fragmentTag) {
        Log.d(TAG, "startFragment: " + fragmentTag);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }

    private void checkConnection(){
        Log.d(TAG, "checkConnection");
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
            Log.d(TAG, "Connection OK");
            /* Check if user is signed in */
            mUser = FirebaseAuth.getInstance().getCurrentUser();
            if (mUser == null){
                startFragment(LoginFragment.newInstance(), LoginFragment.TAG);
            }
            else {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        }
        else {
            Log.d(TAG, "No connection, show dialog");
            startFragment(CheckConnectionFragment.newInstance(), CheckConnectionFragment.TAG);
        }
    }

    /**
     * LoginFragmentInteractionListener
     */
    @Override
    public void onLoginCompleted(final FirebaseUser user) {
        Log.d(TAG, "onLoginCompleted");
        mUser = user;
        mDatabase = FirebaseDatabase.getInstance().getReference();
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    /**
     * CheckConnectionFragmentListener
     */
    @Override
    public void onConnected() {
        checkConnection();
    }

    /**
     * OrderInfoFragmentListener
     */

    @Override
    public void onProfileSave() {
        Log.d(TAG,"onProfileSave()");
    }
}