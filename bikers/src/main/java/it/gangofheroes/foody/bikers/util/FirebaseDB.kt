package it.gangofheroes.foody.bikers.util

import android.location.Location
import android.net.Uri
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import it.gangofheroes.foody.bikers.models.Biker
import it.gangofheroes.foody.bikers.models.Firebase
import it.gangofheroes.foody.bikers.models.Order
import it.gangofheroes.foody.bikers.models.OrderState
import java.math.RoundingMode
import java.text.DecimalFormat

class FirebaseDB {

    private var mDatabase: DatabaseReference? = null
    private var mFirebaseUser: FirebaseUser? = null
    private var mLastLocation: Location? = null
    private lateinit var biker: Biker
    private var currDistance = 0F

    private object GetInstance {
        val INSTANCE = FirebaseDB()
    }

    fun setDatabaseReference(databaseReference: DatabaseReference){
        mDatabase = databaseReference
    }

    fun setFirebaseUser(firebaseUser: FirebaseUser){
        mFirebaseUser = firebaseUser
    }

    fun setBiker(biker: Biker){
        this.biker = biker
    }

    fun updateUserPosition(currentLocation: Location){
        mLastLocation?.let {
            updateUserDistance(currentLocation)
        }

        mLastLocation = currentLocation
        mDatabase?.let { db ->
            mFirebaseUser?.let { user ->
                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.CURRENT_LAT)
                        .setValue(currentLocation.latitude)

                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.CURRENT_LONG)
                        .setValue(currentLocation.longitude)

                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.CURRENT_ALTITUDE)
                        .setValue(currentLocation.altitude)
            }
        }
    }

    fun updateOrderStatus (order: Order, newOrderState: OrderState){
        mDatabase?.let { db ->
            db.child(Firebase.Orders.INTRO)
                    .child(order.orderId)
                    .child(Firebase.Orders.ORDERSTATE)
                    .setValue(newOrderState)
        }

        if (newOrderState == OrderState.DELIVERED){
            mDatabase?.let{ db ->
                mFirebaseUser?.let { user ->
                    db.child(Firebase.Bikers.BIKERS)
                            .child(user.uid)
                            .child(Firebase.Bikers.TOTAL_DELIVERIES)
                            .setValue(++biker.totDeliveries)

                    db.child(Firebase.Bikers.BIKERS)
                            .child(user.uid)
                            .child(Firebase.Bikers.TODAY_DELIVERIES)
                            .setValue(++biker.todayDeliveries)
                }
            }
        }
    }

    fun saveCurrentDistance(){
        mDatabase?.let { db ->
            mFirebaseUser?.let { user ->
                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.TOT_DISTANCE)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError) { }

                            override fun onDataChange(p0: DataSnapshot) {
                                val distance = p0.getValue(Float::class.java)
                                distance?.let {
                                    currDistance = it
                                }
                            }
                        })
            }
        }
    }

    fun updateImgUri(uri: Uri){
        mDatabase?.let { db ->
            mFirebaseUser?.let { user ->
                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.IMG_URI)
                        .setValue(uri)
            }
        }
    }

    private fun updateUserDistance(currentLocation: Location){
        currDistance = (currDistance * 1000 + currentLocation.distanceTo(mLastLocation)) / 1000

        mDatabase?.let{ db ->
            mFirebaseUser?.let { user ->
                db.child(Firebase.Bikers.BIKERS)
                        .child(user.uid)
                        .child(Firebase.Bikers.TOT_DISTANCE)
                        .setValue(currDistance)
            }
        }
    }

    companion object {
        val getInstance: FirebaseDB by lazy {
            GetInstance.INSTANCE
        }
    }
}