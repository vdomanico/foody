package it.gangofheroes.foody.bikers.activities

import android.animation.Animator
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import it.gangofheroes.foody.bikers.R
import kotlinx.android.synthetic.main.prelogin_activity.*
import java.util.*

class PreLoginActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setContentView(R.layout.prelogin_activity)
        animation.addAnimatorListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) { }

            override fun onAnimationEnd(animation: Animator) {
                Log.d(TAG, "onAnimationEnd")
                startActivity(Intent(this@PreLoginActivity, LoginActivity::class.java))
                finish()
            }

            override fun onAnimationCancel(animation: Animator) { }

            override fun onAnimationRepeat(animation: Animator) { }
        })
    }

    companion object {
        const val TAG = "PreLoginActivity"
    }
}