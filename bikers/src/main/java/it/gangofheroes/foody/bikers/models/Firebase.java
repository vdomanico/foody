package it.gangofheroes.foody.bikers.models;

import static it.gangofheroes.foody.bikers.models.Firebase.Bikers.BIKER_AVATAR;
import static it.gangofheroes.foody.bikers.models.Firebase.Customers.CUSTOMER_AVATAR;
import static it.gangofheroes.foody.bikers.models.Firebase.Restaurants.RESTAURATEUR_AVATAR;

public class Firebase {

    public class Bikers {
        public static final String BIKERS = "bikers-f";
        public static final String NICKNAME = "nickName";
        public static final String USER_STATUS = "userStatus";
        public static final String ASSIGNED_ORDERS = "assignedOrders";
        public static final String TODAY_DELIVERIES = "todayDeliveries";
        public static final String TOTAL_DELIVERIES = "totDeliveries";
        public static final String DELIVERER_ID = "delivererId";
        public static final String BIKER_NAME = "bikerName";
        public static final String BIKER_SURNAME = "bikerSurname";
        public static final String BIKER_MAIL = "bikerMail";
        public static final String BIKER_PHONE = "bikerPhone";
        public final static String BIKER_AVATAR = "bikerAvatar";
        public final static String CURRENT_LAT = "lat";
        public final static String CURRENT_LONG = "lng";
        public final static String CURRENT_ALTITUDE = "alt";
        public static final String TOT_DISTANCE = "totDistance";
        public static final String IMG_URI = "imgUri";
    }

    public class Orders {
        public static final String INTRO = "orders-f";
        public static final String ORDERSTATE = "orderState";
    }

    public class Restaurants{
        public static final String RESTAURATEUR_ID = "restaurateurId";
        public final static String RESTAURATEUR_AVATAR = "restaurateurAvatar";
    }

    public class Customers{
        public static final String CUSTOMER_NAME = "customerName";
        public static final String CUSTOMER_SURNAME = "customerSurname";
        public static final String CUSTOMER_MAIL = "customerMail";
        public static final String CUSTOMER_PHONE = "customerPhone";
        public static final String CUSTOMER_ADDRESS = "customerAddress";
        public static final String CUSTOMER_DESC = "customerDesc";
        public static final String CUSTOMER_ID = "customerId";
        public final static String CUSTOMERS = "customers-f";
        public final static String CUSTOMER_AVATAR = "customerAvatar";
    }



    public static String getRefAvatar(UserType userType){
        switch (userType){
            case RESTAURATEUR:
                return RESTAURATEUR_AVATAR;
            case BIKER:
                return BIKER_AVATAR;
            case CUSTOMER:
                return CUSTOMER_AVATAR;
        }
        return null;
    }


}
