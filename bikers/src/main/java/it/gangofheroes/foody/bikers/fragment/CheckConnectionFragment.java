package it.gangofheroes.foody.bikers.fragment;

import android.animation.Animator;
import android.animation.StateListAnimator;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.airbnb.lottie.LottieAnimationView;

import java.util.Objects;

import it.gangofheroes.foody.bikers.R;

public class CheckConnectionFragment extends Fragment {

    public static final String TAG = CheckConnectionFragment.class.getCanonicalName();

    private CheckConnectionFragmentListener mLister;
    private LottieAnimationView retryAnim;
    private Button btn;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.check_connection_fragment, container, false);

        retryAnim = view.findViewById(R.id.lottieAnim);
        btn = view.findViewById(R.id.btnRetry);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Click on btn retry");
                checkConnection();
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CheckConnectionFragmentListener){
            mLister = (CheckConnectionFragmentListener) context;
        }
    }

    private void checkConnection(){
        btn.setOnClickListener(null);
        retryAnim.setVisibility(View.VISIBLE);
        retryAnim.setAnimation(R.raw.retry);
        retryAnim.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) { }

            @Override
            public void onAnimationEnd(Animator animation) { }

            @Override
            public void onAnimationCancel(Animator animation) { }

            @Override
            public void onAnimationRepeat(Animator animation) {
                retryAnim.setVisibility(View.GONE);

                ConnectivityManager connectivityManager = (ConnectivityManager) Objects.requireNonNull(
                        getContext()).getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()){
                    /* Connection OK */
                    mLister.onConnected();
                }
                else {
                    /* Still no connection */
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "Click on btn retry");
                            checkConnection();
                        }
                    });
                }
            }
        });
        retryAnim.playAnimation();
    }

    public static CheckConnectionFragment newInstance(){
        return new CheckConnectionFragment();
    }

    /**
     * Callback
     */
    public interface CheckConnectionFragmentListener {
        void onConnected();
    }
}
