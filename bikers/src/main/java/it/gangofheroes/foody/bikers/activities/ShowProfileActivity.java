package it.gangofheroes.foody.bikers.activities;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import it.gangofheroes.foody.bikers.R;

import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.SHARED_PREF_KEY;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.URI_IMAGE;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_DESCRIPTION;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_MAIL;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_NAME;
import static it.gangofheroes.foody.bikers.util.SharedPrefKeysJava.USER_PHONE;

public class ShowProfileActivity extends AppCompatActivity {

    public static final String TAG = "ShowProfileActivity";

    private TextView mName;
    private TextView mMail;
    private TextView mDesc;
    private TextView mPhone;
    private ImageView mEditProfile;

    private Uri uriTemp;

    private static final int CODE_PERMISSION_READ_EXTERNAL = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.showprofile_activity);

        getView();
        setupView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        setupView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.show_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected");
        switch (item.getItemId()) {
            case R.id.switchMode:
                Log.d(TAG, "Click on EditProfile");
                Intent intent = new Intent(getApplicationContext(), EditProfileActivity.class);
                startActivity(intent);
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d(TAG, "onRequestPermissionsResult");

        switch (requestCode) {
            case CODE_PERMISSION_READ_EXTERNAL:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    setupImage(uriTemp);
                break;
        }
    }

    /**
     * Private Fun
     */
    private void getView() {
        Log.d(TAG, "getView");
        mName = findViewById(R.id.name);
        mMail = findViewById(R.id.mail);
        mPhone = findViewById(R.id.phone);
        mDesc = findViewById(R.id.desc);
        mEditProfile = findViewById(R.id.profileImage);
    }

    private void setupView() {
        Log.d(TAG, "setupView");

        SharedPreferences mPref = getSharedPreferences(SHARED_PREF_KEY, MODE_PRIVATE);
        final String name = mPref.getString(USER_NAME, "");
        final String mail = mPref.getString(USER_MAIL, "");
        final String desc = mPref.getString(USER_DESCRIPTION, "");
        final String phone = mPref.getString(USER_PHONE, "");
        final String imgUri = mPref.getString(URI_IMAGE, "");

        Log.d(TAG, String.format("Restored values:\nName: %s\nMail: %s\nDesc: %s\nPhone: %s", name, mail, desc, phone));

        mName.setText(name != null && name.isEmpty() ? "name not set" : name);
        mPhone.setText(phone != null && phone.isEmpty() ? "phone not set" : phone);
        mMail.setText(mail != null && mail.isEmpty() ? "e-mail not set" : mail);
        if (desc != null && !desc.isEmpty()) {
            findViewById(R.id.infoDesc).setVisibility(View.VISIBLE);
            mDesc.setVisibility(View.VISIBLE);
            mDesc.setText(desc);
        } else {
            mDesc.setVisibility(View.GONE);
            findViewById(R.id.infoDesc).setVisibility(View.GONE);
        }

        if (imgUri != null && !imgUri.isEmpty()) {
            setupImage(Uri.parse(imgUri));
        } else {
            mEditProfile.setImageResource(R.drawable.avatar);
        }
    }

    private void setupImage(Uri imageUri) {
        Log.d(TAG, String.format("setupImage - URI: %s", imageUri));
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            uriTemp = imageUri;
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    CODE_PERMISSION_READ_EXTERNAL);
        } else {
            mEditProfile.setImageURI(imageUri);
        }
    }
}
