package it.gangofheroes.foody.bikers.models;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Biker implements Parcelable {

    public enum BIKER_STATUS {

        AVAILABLE("available"),
        DELIVERING ("delivering"),
        BUSY ("busy"),
        OFFLINE("offline");

        private String userStatus;
        BIKER_STATUS(String userStatus) {
            this.userStatus = userStatus;
        }
        public String getUserStatus(){
            return userStatus;
        }
    }

    private String bikerMail;
    private String bikerName;
    private String bikerSurname;
    private String bikerPhone;
    private BIKER_STATUS userStatus;
    private int totDeliveries;
    private int todayDeliveries;
    private String nickName;
    private String bikerId;
    private Float totDistance;

    public Biker() { }

    public Float getTotDistance() { return this.totDistance; }

    public void setTotDistance(Float totDistance) { this.totDistance = totDistance; }

    public String getBikerMail() {
        return bikerMail;
    }

    public void setBikerMail(String bikerMail) {
        this.bikerMail = bikerMail;
    }

    public String getBikerName() {
        return bikerName;
    }

    public void setBikerName(String bikerName) {
        this.bikerName = bikerName;
    }

    public String getBikerSurname() {
        return bikerSurname;
    }

    public void setBikerSurname(String bikerSurname) {
        this.bikerSurname = bikerSurname;
    }

    public String getBikerPhone() {
        return bikerPhone;
    }

    public void setBikerPhone(String bikerPhone) {
        this.bikerPhone = bikerPhone;
    }

    public BIKER_STATUS getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(BIKER_STATUS userStatus) {
        this.userStatus = userStatus;
    }

    public int getTotDeliveries() {
        return totDeliveries;
    }

    public void setTotDeliveries(int totDeliveries) {
        this.totDeliveries = totDeliveries;
    }

    public int getTodayDeliveries() {
        return todayDeliveries;
    }

    public void setTodayDeliveries(int todayDeliveries) {
        this.todayDeliveries = todayDeliveries;
    }

    public void setNickName (String nickName) { this.nickName = nickName; }

    public String getNickName() {
        return nickName;
    }

    public boolean isValidUser(){
        return bikerMail != null && bikerName != null && bikerSurname != null && bikerPhone != null;
    }

    public void setBikerId(String bikerId){
        this.bikerId = bikerId;
    }

    public String getBikerId() { return bikerId; }

    protected Biker(Parcel in) {
        bikerMail = in.readString();
        bikerName = in.readString();
        bikerSurname = in.readString();
        bikerPhone = in.readString();
        totDeliveries = in.readInt();
        todayDeliveries = in.readInt();
        nickName = in.readString();
        bikerId = in.readString();
        totDistance = in.readFloat();
    }

    public static final Creator<Biker> CREATOR = new Creator<Biker>() {
        @Override
        public Biker createFromParcel(Parcel in) {
            return new Biker(in);
        }

        @Override
        public Biker[] newArray(int size) {
            return new Biker[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bikerMail);
        dest.writeString(bikerName);
        dest.writeString(bikerSurname);
        dest.writeString(bikerPhone);
        dest.writeInt(totDeliveries);
        dest.writeInt(todayDeliveries);
        dest.writeString(nickName);
        dest.writeString(bikerId);
        dest.writeFloat(totDistance);
    }
}
