package it.gangofheroes.foody.bikers.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import it.gangofheroes.foody.bikers.R
import kotlinx.android.synthetic.main.enable_gps_fragment.*

class EnableGPSFragment: DialogFragment(){

    private var mListener: EnableGPSFragmentListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_Dialog_Alert)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.enable_gps_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        ok.setOnClickListener {
            Log.d(TAG, "onViewCreated")
            mListener?.onGPSEnabled()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttach")
        if (context is EnableGPSFragmentListener){
            mListener = context
        }
    }

    interface EnableGPSFragmentListener {
        fun onGPSEnabled()
    }

    companion object {
        const val TAG = "EnableGPSFragment"

        fun newInstance() = EnableGPSFragment()
    }
}