package it.gangofheroes.foody.bikers.models;

public enum OrderState {
   NEW,              // Ordine appena effettuato dal customer
   ASSIGNED,         // Ordine assegnato dal rest al biker -> può essere rifiutato
   ACCEPTED,         // Ordine accettato dal biker -> è in ritiro presso il ristorante
   DECLINED,         // Il biker ha rifiutato l'ordine, il ristoratore deve scegliere un nuovo biker
   DELIVERING,       // Il biker ha ritirato il pacco presso il ristoratore ed è in consegna al customer
   DELIVERED         // Il biker ha consegnato al customer
}
