package it.gangofheroes.foody.bikers.adapter;

import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.gangofheroes.foody.bikers.R;
import it.gangofheroes.foody.bikers.models.Order;
import it.gangofheroes.foody.bikers.models.OrderState;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {
    private static final String TAG = OrderAdapter.class.getCanonicalName();

    private List<Order> orders;
    private OrderAdapterCallback mListener;

    public OrderAdapter(List<Order> orders){
        this.orders = orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(inflater
                .inflate(R.layout.order_adapter, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");

        final Order temp = orders.get(position);
        holder.order = temp;
        holder.customerName.setText(temp.getCustomerName());
        holder.customerAddress.setText(temp.getCustomerAddress());
        holder.customerPhone.setText(temp.getCustomerPhone());
        holder.restaurantAddress.setText(temp.getRestaurantAddress());

        if (temp.getOrderState() == OrderState.DELIVERING){
            holder.clContainer.setBackgroundColor(Color.parseColor("#8AFEA8"));
            // holder.startDeliveringContainer.setVisibility(View.GONE);
        }

        holder.clContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Click on Item Adapter");
                mListener.orderInfo(temp);
            }
        });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public void setListener(OrderAdapterCallback listener){
        mListener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private Order order;
        private ConstraintLayout clContainer;
        private TextView customerName;
        private TextView customerAddress;
        private TextView customerPhone;
        private TextView restaurantAddress;

        private ViewHolder(View v){
            super(v);
            clContainer = v.findViewById(R.id.clContainer);
            customerName = v.findViewById(R.id.tvCustomerName);
            customerAddress = v.findViewById(R.id.customerAddress);
            customerPhone = v.findViewById(R.id.customerPhone);
            restaurantAddress = v.findViewById(R.id.restaurantAddress);
        }
    }

    public interface OrderAdapterCallback {
        void orderInfo(Order order);
    }
}
