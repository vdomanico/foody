package it.gangofheroes.foody.bikers.util;

import static it.gangofheroes.foody.bikers.util.StringsUtil.PACKAGE_NAME;

public class SharedPrefKeysJava {

    public static final String SHARED_PREF_KEY = PACKAGE_NAME;
    public static final String USER_NAME = "KEY_NAME";
    public static final String USER_MAIL = "USER_MAIL";
    public static final String USER_DESCRIPTION = "USER_DESCRIPTION";
    public static final String USER_PHONE = "USER_PHONE";
    public static final String URI_IMAGE = "URI_IMAGE";
    public static final String SHARED_PREF_KEY_TEMP = "SHARED_PREF_KEY_TEMP";
    public static final String FIRST_TIME_CAMERA_REQUEST = "FIRST_TIME_CAMERA_REQUEST";
}
