package it.gangofheroes.foody.customers.activity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.activities.ShowProfileActivity;

import static androidx.test.espresso.Espresso.onView;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ShowProfileActivityTest {

    @Rule
    public ActivityTestRule<ShowProfileActivity> activityRule = new ActivityTestRule<>(ShowProfileActivity.class);

    @Test
    public void exampleTestMainActivity(){
        onView(ViewMatchers.withId(R.id.profileImage)).
                check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
    }
}
