package it.gangofheroes.foody.customers.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.List;
import java.util.Locale;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.model.Restaurateur;
import it.gangofheroes.foody.customers.view.FiveStarsView;

import static android.content.Context.MODE_PRIVATE;
import static it.gangofheroes.foody.customers.model.Firebase.RESTAURATEUR_AVATAR;


public class RestaurateurAdapter extends RecyclerView.Adapter<RestaurateurAdapter.ViewHolder> {

    private static final String SHARED_PREF = "it.gangofheroes.foody.customers";
    private static final String TAG = RestaurateurAdapter.class.getCanonicalName();
    private SharedPreferences mPreferences;
    private final Context context;
    private RestaurateurAdapterListener restaurateurAdapterListener;
    private List<Restaurateur> restaurateurs;


    public RestaurateurAdapter(List<Restaurateur> restaurateurs, RestaurateurAdapterListener listener, Context context){
        this.context = context;
        this.restaurateurs= restaurateurs;
        this.restaurateurAdapterListener = listener;
        mPreferences = context.getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.restaurant_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.restaurateur = restaurateurs.get(position);
        downloadRestaurateurAvatar(holder.restaurateur.getRestaurateurId(),holder.restaurantAvatarIv);
        holder.name.setText(holder.restaurateur.getRestaurantName() != null ? holder.restaurateur.getRestaurantName() : "");
        holder.address.setText(holder.restaurateur.getRestaurantAddress() != null ? holder.restaurateur.getRestaurantAddress() : "");
        holder.description.setText(holder.restaurateur.getDescription() != null ? holder.restaurateur.getDescription() : "");
        if(holder.restaurateur.getDistance()!=null){
            Float distance = holder.restaurateur.getDistance();
            String dValue = "";
            if (distance > 1000){
                float mDis = distance / 1000;
                dValue = String.format( Locale.getDefault(),"%.1f",mDis) + " Km";
            }
            else{
                dValue = distance.intValue() + " m";
            }
            holder.distanceTv.setText(dValue);
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                restaurateurAdapterListener.showRestaurantView(holder.restaurateur.getRestaurateurId() ,holder.restaurateur);
            }
        });

        holder.fiveStarsView.setStars(0);
        String reviews = 0 + " " + context.getString(R.string.your_reviews);
        holder.nReviewsTv.setText(reviews);
        getRestaurantReviews(holder);


    }

    private void getRestaurantReviews(final ViewHolder holder) {
        FirebaseDatabase.getInstance().getReference(Firebase.REVIEWS).child(holder.restaurateur.getRestaurateurId()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    Log.d(TAG, "getRestaurantReviews :" + holder.restaurateur.getRestaurateurId());
                    int sumReview = 0;
                    int nReviews = 0;
                    for(DataSnapshot ds : dataSnapshot.getChildren()){
                        Integer rev = ds.child(Firebase.GLOBAL_REVIEW).getValue(Integer.class);
                        if(rev!=null && rev!=0){
                            Log.d(TAG,"user Rev :" + rev);
                            sumReview+=rev;
                            nReviews++;
                        }
                    }
                    if(nReviews!=0){
                        double avgR = sumReview/ (double) nReviews;
                        holder.restaurateur.setAvgRatings(avgR);
                        holder.restaurateur.setNoRatings(nReviews);
                        holder.fiveStarsView.setStars((int)avgR);
                        String reviews = nReviews + " " + context.getString(R.string.your_reviews);
                        holder.nReviewsTv.setText(reviews);
                    }
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return restaurateurs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private Restaurateur restaurateur;
        private TextView name;
        private TextView address;
        private TextView description;
        private TextView distanceTv;
        private TextView nReviewsTv;
        private ImageView restaurantAvatarIv;
        private FiveStarsView fiveStarsView;
        private RelativeLayout layout;

        private ViewHolder(View v) {
            super(v);
            Log.d(TAG, "ViewHolder");
            name = v.findViewById(R.id.restaurant_name);
            address = v.findViewById(R.id.restaurant_address);
            description = v.findViewById(R.id.restaurant_description);
            distanceTv = v.findViewById(R.id.restaurant_distance);
            restaurantAvatarIv = v.findViewById(R.id.restaurant_avatar);
            fiveStarsView = v.findViewById(R.id.five_stars_global);
            nReviewsTv = v.findViewById(R.id.n_reviews);
            layout = v.findViewById(R.id.restaurant_view);
        }
    }

    public interface RestaurateurAdapterListener {
        void showRestaurantView(String id, Restaurateur restaurateur);
    }

    private void downloadRestaurateurAvatar(final String restaurateurId, final ImageView restaurateurAvatarIv) {

        final StorageReference storageReference = FirebaseStorage.
                getInstance().
                getReference().
                child(restaurateurId + "/" +RESTAURATEUR_AVATAR);

        final File file = getFile(RESTAURATEUR_AVATAR + "-" + restaurateurId);
        final boolean successFromDevice = uploadFromDevice(file,restaurateurAvatarIv);

        storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long timeUpdate = storageMetadata.getUpdatedTimeMillis();
                Log.d(TAG, "time of update" + timeUpdate);
                if(timeUpdate>retrieveLastModifiedOnThisDevice(RESTAURATEUR_AVATAR + "-" + restaurateurId) || !successFromDevice){
                    downloadImage(storageReference,file,restaurateurAvatarIv,timeUpdate,restaurateurId);
                }
            }
        });

    }

    private void downloadImage(StorageReference storageReference, final File file, final ImageView imageView, final Long timeUpdate, final String restaurateurId){
        storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG,"rest avatar downloaded");
                Uri uri = FileProvider.getUriForFile(context, "it.gangofheroes.foody.customers.android.fileprovider", file);
                imageView.setImageURI(uri);
                saveLastModifiedOnThisDevice(RESTAURATEUR_AVATAR + "-" + restaurateurId,timeUpdate);
            }
        });
    }


    private File getFile(String filename){
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir,filename);
    }

    private boolean uploadFromDevice(File file, ImageView imageView){
        if(file.exists() && file.length()>0) {
            Uri uri = Uri.fromFile(file);
            imageView.setImageURI(uri);
            return true;
        }
        return false;
    }

    private void saveLastModifiedOnThisDevice(String key, Long time){
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putLong(key, time);
        preferencesEditor.apply();
    }

    private Long retrieveLastModifiedOnThisDevice(String key){
        return mPreferences.getLong(key,0);
    }
}
