package it.gangofheroes.foody.customers.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DishSelection implements Parcelable {

    private String category;
    private String dish;
    private String commentReview;
    private int quantity;
    private double amount;
    private int review;

    public DishSelection() { }

    public DishSelection(String category, String dish, int quantity, double amount) {
        this.category = category;
        this.dish = dish;
        this.quantity = quantity;
        this.amount = amount;
    }

    private DishSelection(Parcel in) {
        category = in.readString();
        dish = in.readString();
        commentReview = in.readString();
        quantity = in.readInt();
        amount = in.readDouble();
        review = in.readInt();
    }

    public static final Creator<DishSelection> CREATOR = new Creator<DishSelection>() {
        @Override
        public DishSelection createFromParcel(Parcel in) {
            return new DishSelection(in);
        }

        @Override
        public DishSelection[] newArray(int size) {
            return new DishSelection[size];
        }
    };

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public String getCommentReview() {
        return commentReview;
    }

    public void setCommentReview(String commentReview) {
        this.commentReview = commentReview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(dish);
        dest.writeString(commentReview);
        dest.writeInt(quantity);
        dest.writeDouble(amount);
        dest.writeInt(review);
    }
}
