package it.gangofheroes.foody.customers.util;

public class StringsUtil {

    public static final String PACKAGE_NAME = "it.gangofheroes.foody.customers";
    public static final String SHARED_PREF = PACKAGE_NAME;
    public static final String FIRST_TIME_CAMERA_REQUEST = "FIRST_TIME_CAMERA_REQUEST";
    public static final String PROVIDER_NAME = "it.gangofheroes.foody.customers.android.fileprovider";
}
