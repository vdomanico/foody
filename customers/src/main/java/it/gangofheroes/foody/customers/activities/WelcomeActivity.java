package it.gangofheroes.foody.customers.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.fragments.AddCommentFragment;
import it.gangofheroes.foody.customers.fragments.BasketFragment;
import it.gangofheroes.foody.customers.fragments.CategoryFragment;
import it.gangofheroes.foody.customers.fragments.DishRatingsFragment;
import it.gangofheroes.foody.customers.fragments.EditYourProfileFragment;
import it.gangofheroes.foody.customers.fragments.RestaurantFragment;
import it.gangofheroes.foody.customers.fragments.RestaurantsFragment;
import it.gangofheroes.foody.customers.fragments.ShowYourProfileFragment;
import it.gangofheroes.foody.customers.fragments.YourOrderReviewFragment;
import it.gangofheroes.foody.customers.fragments.YourOrdersFragment;
import it.gangofheroes.foody.customers.model.Category;
import it.gangofheroes.foody.customers.model.Customer;
import it.gangofheroes.foody.customers.model.DishReview;
import it.gangofheroes.foody.customers.model.DishSelection;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.model.Order;
import it.gangofheroes.foody.customers.model.Restaurateur;
import it.gangofheroes.foody.customers.util.DownloadAvatarUtil;

import static it.gangofheroes.foody.customers.model.Firebase.CUSTOMERS;
import static it.gangofheroes.foody.customers.model.Firebase.CUSTOMER_ID;
import static it.gangofheroes.foody.customers.model.Firebase.DISHES;
import static it.gangofheroes.foody.customers.model.Firebase.ORDERS;
import static it.gangofheroes.foody.customers.model.Firebase.REVIEWS;
import static it.gangofheroes.foody.customers.model.UserType.CUSTOMER;

public class WelcomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        RestaurantsFragment.RestaurateurFragmentListener,
        RestaurantFragment.RestaurateurFragmentEventListener,
        CategoryFragment.EditMenuFragmentEventListener,
        BasketFragment.BasketFragmentEventListener,
        ShowYourProfileFragment.ShowYourProfileFragmentEventListener,
        EditYourProfileFragment.EditYourProfileFragmentEventListener,
        YourOrdersFragment.YourOrdersFragmentEventListener,
        YourOrderReviewFragment.YourOrderReviewFragmentListener,
        AddCommentFragment.AddCommentFragmentListener{

    private static final String TAG = WelcomeActivity.class.getCanonicalName();
    private FirebaseUser firebaseUser;
    private String restaurateurId;
    private Map<String, DishSelection> basket;
    private DatabaseReference customersRef;
    private DatabaseReference ordersRef;
    private DatabaseReference reviewsRef;
    private Customer customer;
    private ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);
        setupToolbar();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        customersRef = FirebaseDatabase.getInstance().getReference(CUSTOMERS);
        ordersRef = FirebaseDatabase.getInstance().getReference(ORDERS);
        reviewsRef = FirebaseDatabase.getInstance().getReference(REVIEWS);
        basket = new HashMap<>();
        updateRegistrationToken(firebaseUser.getUid());
        updateCustomerId(firebaseUser.getUid());
        loadCustomer(firebaseUser.getUid());
        showRestaurantsFragment();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_basket, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int id = menuItem.getItemId();
        if (id == R.id.your_basket) {
            showBasketFragment(new ArrayList<>(basket.values()));
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if (id == R.id.nav_your_basket) {
            drawer.closeDrawer(GravityCompat.START);
            showBasketFragment(new ArrayList<>(basket.values()));
        }
        else if(id == R.id.nav_your_profile){
            drawer.closeDrawer(GravityCompat.START);
            showYourProfileFragment();
        }
        else if(id == R.id.nav_restaurants){
            drawer.closeDrawer(GravityCompat.START);
            showRestaurantsFragment();
        }
        else if(id == R.id.nav_sign_out){
            drawer.closeDrawer(GravityCompat.START);
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            finish();
        }
        else if (id == R.id.nav_your_orders) {
            drawer.closeDrawer(GravityCompat.START);
            showYourOrdersFragment();
        }

        return true;

    }




    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (f instanceof RestaurantsFragment){
            finish();
        }
        else {
            int nFrag = getSupportFragmentManager().getBackStackEntryCount();
            getSupportFragmentManager().popBackStack();
            if(!toggle.isDrawerIndicatorEnabled() && (f instanceof ShowYourProfileFragment || f instanceof BasketFragment || f instanceof YourOrderReviewFragment)) {
                toggle.setDrawerIndicatorEnabled(true);
            }
        }
    }

    @Override
    public void onBasketNewDish(String restId , Map<String, DishSelection> dishes, String lastDishAdded) {
        Log.d(TAG,restId + " " + "Aggiungi al carrello");
        restaurateurId = restaurateurId == null ? restId : restaurateurId;
        if (isDishSelectionOfTheSameRestaurateur(restId)){
            for(String string : dishes.keySet()) {
                if (dishes.get(string).getQuantity() == 0) {
                    basket.remove(string);
                } else {
                    basket.put(string, dishes.get(string));
                }
            }
        }
        else{
            launchDropBasketDialogue(restId,dishes,lastDishAdded);
        }
    }

    @Override
    public void onSeeComment(DishReview dishReview) {
        startDishRatingsFragment(dishReview);
    }


    private void newBasket(Map<String, DishSelection> dishes) {
        for(String k : basket.keySet()){
            dishes.remove(k);
        }
        basket.clear();
        basket = dishes;
    }

    private boolean isDishSelectionOfTheSameRestaurateur(String restaurateurSelected){
        return restaurateurId == null || basket.isEmpty() || restaurateurId.equals(restaurateurSelected);
    }

    private void launchDropBasketDialogue(final String rId , final Map<String, DishSelection> dishes, final String lastDishAdded) {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(WelcomeActivity.this, R.style.DialogTheme);
        alertDiaolg.setTitle("Svuota Carrello");
        alertDiaolg.setMessage("Hai già aggiunto prodotti di un altro ristorante," +
                " vuoi svuotare il carrello e aggiungere prodotti di questo ristorante?");
        alertDiaolg.setPositiveButton("SI",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        restaurateurId = rId;
                        basket.clear();
                        basket.put(lastDishAdded,dishes.get(lastDishAdded));

                    }
                });
        alertDiaolg.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dishes.remove(lastDishAdded);
                //FIXME il pulsante di scelta più o meno rimane settato
            }
        });
        alertDiaolg.create().show();
    }


    private void sendOrder() {
        if(customer==null){
            Toast.makeText(getApplicationContext(), "Accedere per effettuare ordine!", Toast.LENGTH_SHORT).show();
            return;
        }
        ordersRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "adding order");
                DatabaseReference orderIdRef = dataSnapshot.getRef().push();
                Date now = new Date();
                Long orderDate = now.getTime();
                Order order = new Order.
                        builder().
                        setOrderId(orderIdRef.getKey()).
                        setDishes(new ArrayList<>(basket.values())).
                        setRestaurateurId(restaurateurId).
                        setCustomerId(customer.getCustomerId()).
                        setCustomerAddress(customer.getCustomerAddress()). //TODO finire di aggiungere le info
                        setCustomerPhone(customer.getCustomerPhone()).
                        setCustomerSurname(customer.getCustomerSurname()).
                        setCustomerName(customer.getCustomerName()).
                        setOrderDate(orderDate).
                        setDeliveryTime(orderDate + (30 * 60 * 1000)).
                        setReview(0).
                        build();
                orderIdRef.setValue(order);
                basket.clear();
                restaurateurId = null;
                Toast.makeText(getApplicationContext(), "Ordine effettuato!", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }

    private void loadCustomer(String user){
        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        ImageView nav_user = hView.findViewById(R.id.avatar);
        final TextView emailTv = hView.findViewById(R.id.customer_email);
        DownloadAvatarUtil.downloadUserAvatar(user,
                CUSTOMER,
                nav_user,
                getApplicationContext(),
                WelcomeActivity.TAG);
        customersRef.
                child(user).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.exists()) {
                            customer = dataSnapshot.getValue(Customer.class);
                            if(customer!=null){
                                emailTv.setText(customer.getCustomerMail());
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.d(TAG,"Controllare le regole di lettura e scrittura di firebase");
                    }
                });
    }

    private void updateRegistrationToken(final String user){
        if(user!=null){
            FirebaseInstanceId.getInstance().getInstanceId()
                    .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                        @Override
                        public void onComplete(@NonNull Task<InstanceIdResult> task) {
                            if (!task.isSuccessful()) {
                                Log.w(TAG, "getInstanceId failed", task.getException());
                                return;
                            }
                            if(task.getResult()!=null){
                                final String token = task.getResult().getToken();
                                Log.d(TAG,token);
                                FirebaseDatabase.
                                        getInstance()
                                        .getReference(CUSTOMERS).
                                        child(user + "/tokens/" + token).setValue(true);
                            }

                        }
                    });
        }

    }

    private void updateCustomerId(final String user){
        if(user!=null){
            customersRef.child(user).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()){
                        dataSnapshot.getRef().setValue(user);
                        dataSnapshot.getRef().child(CUSTOMER_ID).setValue(user);
                        Log.d(TAG,"Customer e CustomerId inseriti");
                    }else{
                        if(!dataSnapshot.child(CUSTOMER_ID).exists()){
                            dataSnapshot.child(CUSTOMER_ID).getRef().setValue(user);
                            Log.e(TAG,"CustomerId inserito");
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG,databaseError.getMessage());
                }
            });
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    }


    /*
    *
    * Fragments Lifecycle
    *
    * */

    @Override
    public void onBasketShow() {
        Log.d(TAG,"onBasketShow()");
        showBasketFragment(new ArrayList<>(basket.values()));
    }

    @Override
    public void onOrderConfirm() {
        Log.d(TAG,"onOrderConfirm()");
        if(!basket.isEmpty()) {
            sendOrder();
            getSupportFragmentManager().popBackStack();
            showRestaurantsFragment();//TODO mandato l'ordine decidere cosa fare, nuova activity con riepilogo ordini o altro..
        }
    }
    @Override
    public void onRestaurantShow(String id, Restaurateur restaurateur) {
        Log.d(TAG, "onRestaurantShow()");
        showRestaurateurFragment(restaurateur);
    }

    @Override
    public void onMenuCategoryShow(Restaurateur restaurateur, Category category, int position) {
        Log.d(TAG,"onMenuCategoryShow()");
        showMenuCategoryFragment(restaurateur,category,position);
    }

    @Override
    public void onProfileEdit() {
        Log.d(TAG,"onProfileEdit()");
        editYourProfileFragment();
    }
    @Override
    public void onProfileSave() {
        Log.d(TAG,"onProfileSave()");
        showRestaurantsFragment();
    }

    private void showRestaurateurFragment(Restaurateur restaurateur){
        Log.d(TAG,"showRestaurateurFragment()");
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer,
                        RestaurantFragment.newInstance(restaurateur))
                .addToBackStack(RestaurantFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void showMenuCategoryFragment(Restaurateur restaurateur, Category category, int position){
        Log.d(TAG,"showMenuCategoryFragment()");
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer,
                        CategoryFragment.newInstance(restaurateur, category, position, new ArrayList<>(basket.values())))
                .addToBackStack(CategoryFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void showRestaurantsFragment() {
        Log.d(TAG,"showRestaurantsFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof RestaurantsFragment)) {
            toggle.setDrawerIndicatorEnabled(true);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            RestaurantsFragment.newInstance())
                    .addToBackStack(RestaurantsFragment.TAG)
                    .commit();
        }
    }

    private void showBasketFragment(ArrayList<DishSelection> basket){
        Log.d(TAG,"showBasketFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof BasketFragment)){
            toggle.setDrawerIndicatorEnabled(false);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            BasketFragment.newInstance(restaurateurId, basket, customer))
                    .addToBackStack(BasketFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }

    }

    private void showYourOrdersFragment(){
        Log.d(TAG,"showYourOrdersFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof YourOrdersFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            YourOrdersFragment.newInstance())
                    .addToBackStack(YourOrdersFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void showYourProfileFragment(){
        Log.d(TAG,"showYourProfileFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof ShowYourProfileFragment)){
            toggle.setDrawerIndicatorEnabled(false);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            ShowYourProfileFragment.newInstance())
                    .addToBackStack(ShowYourProfileFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void editYourProfileFragment(){
        Log.d(TAG,"editYourProfileFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof EditYourProfileFragment)){
            toggle.setDrawerIndicatorEnabled(false);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            EditYourProfileFragment.newInstance())
                    .addToBackStack(EditYourProfileFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    @Override
    public void onEditReview(Order order) {
        Log.d(TAG,"onEditReview()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof YourOrderReviewFragment)){
            toggle.setDrawerIndicatorEnabled(false);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            YourOrderReviewFragment.newInstance(order))
                    .addToBackStack(YourOrderReviewFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void startDishRatingsFragment(DishReview dishReview){
        Log.d(TAG,"startDishRatingsFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (!(f instanceof DishRatingsFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentContainer,
                            DishRatingsFragment.Companion.newInstance(dishReview))
                    .addToBackStack(DishRatingsFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    @Override
    public void commentAdded(@NotNull Order order, @NotNull final DishSelection dishSelection) {
        Log.d(TAG, "comment added");

        reviewsRef.child(order.getRestaurateurId()).
                child(order.getOrderId()).child(DISHES).
                addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.child(dishSelection.getCategory()).child(dishSelection.getDish()).child(Firebase.COMMENT_REVIEW).getRef().setValue(dishSelection.getCommentReview());
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void cancelPressed(){
        Log.d(TAG, "cancelPressed");
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void addComment(Order order, DishSelection dishSelection) {
        Log.d(TAG, "addComment");
        getSupportFragmentManager()
                .beginTransaction()
                .add(AddCommentFragment.Companion.newInstance(order,dishSelection),
                        AddCommentFragment.TAG)
                .addToBackStack(AddCommentFragment.TAG)
                .commitAllowingStateLoss();
    }
}
