package it.gangofheroes.foody.customers.task;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class GetLatLngAsyncTask extends AsyncTask<Object, Void, LatLng> {

    private AsyncEventListener eventListener;

    public GetLatLngAsyncTask(AsyncEventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    protected LatLng doInBackground(Object... objects) {
        if(objects==null){
            return null;
        }
        Context context = (Context) objects[0];
        String addressString = (String) objects[1];
        List<Address> address;
        Geocoder coder= new Geocoder(context);
        LatLng p1 = null;
        try
        {
            address = coder.getFromLocationName(addressString, 1);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            Log.e(this.getClass().getCanonicalName(),e.getMessage());
        }
        return p1;
    }

    @Override
    protected void onPostExecute(LatLng latLng) {
        super.onPostExecute(latLng);
        eventListener.onTaskCompleted(latLng);
    }

    @Override
    protected void onCancelled() {
        eventListener.onTaskCompleted(null);
    }

    public interface AsyncEventListener {
        void onTaskCompleted(LatLng latLng);
    }


}
