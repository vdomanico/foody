package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.BasketAdapter;
import it.gangofheroes.foody.customers.model.Customer;
import it.gangofheroes.foody.customers.model.DishSelection;

public class BasketFragment extends Fragment {

    private static final String KEY_CUSTOMER = "KEY_CUSTOMER";
    private List<DishSelection> dishes;
    private String restaurateurId;
    private TextView confirmOrderTv;
    private TextView customerNameTv;
    private TextView customerAddressTv;
    private TextView customerPhoneTv;
    private TextView totalPriceTv;
    private Customer customer;
    private FragmentActivity fragmentActivity;

    public interface BasketFragmentEventListener{
        public void onOrderConfirm();
    }

    public static final String TAG = BasketFragment.class.getCanonicalName();
    private static final String KEY_DISHES = "KEY_DISHES";
    private BasketFragmentEventListener eventListener;
    private BasketAdapter basketAdapter;
    public static Fragment newInstance(String restaurateurId, ArrayList<DishSelection> dishes, Customer customer) {
        BasketFragment fragment = new BasketFragment();
        Bundle args = new Bundle();
        args.putString("RESTAURATEUR",restaurateurId);
        args.putParcelableArrayList(KEY_DISHES,dishes);
        args.putParcelable(KEY_CUSTOMER,customer);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        Log.d(TAG,"onPrepareOptionsMenu");
        MenuItem item = menu.findItem(R.id.your_basket);
        if(item!=null)
            item.setVisible(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(R.string.basket);
        this.eventListener = (BasketFragmentEventListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.basket_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        customerNameTv = view.findViewById(R.id.customerName);
        customerAddressTv = view.findViewById(R.id.customerAddress);
        customerPhoneTv = view.findViewById(R.id.customerPhone);
        confirmOrderTv = view.findViewById(R.id.confirm_order);
        totalPriceTv = view.findViewById(R.id.dishes_total_price);
        confirmOrderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventListener.onOrderConfirm();
            }
        });
        dishes = null;
        if(getArguments()!=null){
            restaurateurId = getArguments().getString("RESTAURATEUR");
            dishes = getArguments().getParcelableArrayList(KEY_DISHES);
            customer = getArguments().getParcelable(KEY_CUSTOMER);
        }
        if(dishes == null || dishes.isEmpty()){
            confirmOrderTv.setText(R.string.basket_empty);
            confirmOrderTv.setClickable(false);
        }else{
            double totAmount = 0;
            for(DishSelection dishSelection : dishes){
                totAmount += dishSelection.getAmount();
            }
            totalPriceTv.setText(String.valueOf(totAmount));
        }

        if(customer!=null){
            String fullName = customer.getCustomerName() +  " " + customer.getCustomerSurname();
            customerNameTv.setText(fullName);
            customerAddressTv.setText(customer.getCustomerAddress());
            customerPhoneTv.setText(customer.getCustomerPhone());
        }
        basketAdapter = new BasketAdapter(dishes);
        RecyclerView recyclerView = view.findViewById(R.id.dish_selection_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(basketAdapter);
    }
}
