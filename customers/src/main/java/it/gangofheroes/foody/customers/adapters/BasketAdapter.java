package it.gangofheroes.foody.customers.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.DishSelection;

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder>{

    private final List<DishSelection> dishes;

    public BasketAdapter(List<DishSelection> dishes) {
        this.dishes = dishes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.basket_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupView(dishes.get(position));
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView dish_name;
        TextView dish_qty;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            dish_name = itemView.findViewById(R.id.basket_dish_name);
            dish_qty = itemView.findViewById(R.id.basket_dish_qty);
        }
        void setupView(DishSelection dish) {
            if(dish!=null){
                String qty = "x" + dish.getQuantity();
                dish_name.setText(dish.getDish());
                dish_qty.setText(qty);
            }
        }
    }
}
