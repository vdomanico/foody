package it.gangofheroes.foody.customers.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import it.gangofheroes.foody.customers.R;

public class QuantityControllerView extends RelativeLayout {


    private TextView dish_qty;
    private EventListener eventListener;

    public QuantityControllerView(Context context) {
        super(context);
    }

    public QuantityControllerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public void setEventListener(EventListener eventListener){
        this.eventListener = eventListener;
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.quantity_controller, this);
        ImageView increaseQtyIv = findViewById(R.id.increase_qty);
        ImageView decreaseQtyIv = findViewById(R.id.decrease_qty);
        dish_qty = findViewById(R.id.qty);
        increaseQtyIv.setOnClickListener(onIncreaseListener);
        decreaseQtyIv.setOnClickListener(onDecreaseListener);
        setQty(0);
    }


    public void setQty(int qty){
        dish_qty.setText(String.valueOf(qty));
        if(qty>0){
            dish_qty.setTextColor(Color.RED);
        }
        else {
            dish_qty.setTextColor(Color.BLACK);
        }
    }

    public int getQty(){
        return Integer.valueOf(dish_qty.getText().toString());
    }

    public void increase(){
        int qty = getQty()+1;
        setQty(qty);
        eventListener.notifyQuantityChanged(qty);
    }

    public void decrease(){
        int qty = getQty();
        if(qty>0){
            setQty(--qty);
        }
        eventListener.notifyQuantityChanged(qty);
    }


    private View.OnClickListener onIncreaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            increase();
        }};
    private View.OnClickListener onDecreaseListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            decrease();
        }};

    public interface EventListener{
        void notifyQuantityChanged(int qty);
    }

}
