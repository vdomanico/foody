package it.gangofheroes.foody.customers.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.DishSelection;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.model.Order;
import it.gangofheroes.foody.customers.view.FiveStarsView;

import static it.gangofheroes.foody.customers.model.Firebase.DISHES;
import static it.gangofheroes.foody.customers.model.Firebase.REVIEWS;

public class DishReviewAdapter extends RecyclerView.Adapter<DishReviewAdapter.ViewHolder>{

    private static final String TAG = DishReviewAdapter.class.getCanonicalName();
    private static final String SHARED_PREF = "it.gangofheroes.foody.customers";
    private final Context context;
    private Order order;
    private List<DishSelection> dishes = new ArrayList<>();
    private DatabaseReference reviewsRef;
    private DishReviewAdapterListaner mListener;

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dishName;
        private DishSelection dish;
        private FiveStarsView fiveStarsView;
        private ImageView editReviewIv;
        private ImageView saveReviewIv;
        private ImageView commentReviewIv;
        private int review = 0;

        private ViewHolder(View v) {
            super(v);
            Log.d(TAG, "ViewHolder");
            dishName = v.findViewById(R.id.dishNameRev);
            fiveStarsView = v.findViewById(R.id.starsLayout);
            editReviewIv = v.findViewById(R.id.edit_review);
            saveReviewIv = v.findViewById(R.id.save_review);
            commentReviewIv = v.findViewById(R.id.edit_comment);
            editReviewIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fiveStarsView.setAlpha(1f);
                    editReviewIv.setVisibility(View.GONE);
                    saveReviewIv.setVisibility(View.VISIBLE);
                    fiveStarsView.setStarsClickable(true);
                }
            });

            saveReviewIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fiveStarsView.setAlpha(0.3f);
                    editReviewIv.setVisibility(View.VISIBLE);
                    saveReviewIv.setVisibility(View.GONE);
                    fiveStarsView.setStarsClickable(false);
                    review = fiveStarsView.getStars();


                    if(review!=0){
                        dish.setReview(review);
                        reviewsRef.child(order.getOrderId()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                int gRev = getReview(dishes);
                                dataSnapshot.child(DISHES).child(dish.getCategory()).child(dish.getDish()).child(Firebase.DISH_REVIEW).getRef().setValue(dish.getReview());
                                dataSnapshot.child(Firebase.GLOBAL_REVIEW).getRef().setValue(gRev);
                                if(!dataSnapshot.child(Firebase.CUSTOMER_NAME).exists()){
                                    dataSnapshot.child(Firebase.CUSTOMER_NAME).getRef().setValue(order.getCustomerName());
                                }
                                if(!dataSnapshot.child(Firebase.CUSTOMER_SURNAME).exists()){
                                    dataSnapshot.child(Firebase.CUSTOMER_SURNAME).getRef().setValue(order.getCustomerSurname());
                                }
                                if(!dataSnapshot.child(Firebase.ORDER_REVIEW_DATE).exists()){
                                    Date today = new Date();
                                    dataSnapshot.child(Firebase.ORDER_REVIEW_DATE).getRef().setValue(today.getTime());
                                }
                                if(!dataSnapshot.child(Firebase.ORDER_DATE).exists()){
                                    dataSnapshot.child(Firebase.ORDER_DATE).getRef().setValue(order.getOrderDate());
                                }
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                                Log.d(TAG,databaseError.getMessage());
                            }
                        });
                    }
                }
            });



            fiveStarsView.enableListener(true);
        }

    }

    private int getReview(List<DishSelection> dishes) {
        int sum = 0;
        int nCount = 0;
        int rev = 0;
        for (DishSelection d : dishes) {
            int r = d.getReview();
            if (r != 0) {
                sum += r;
                nCount++;
            }
        }
        if(sum!=0){
            rev = sum/nCount;
        }
        return rev;
    }

    public DishReviewAdapter(Context context, Order order) {
        this.context = context;
        this.order = order;
        this.dishes.clear();
        for(DishSelection d : order.getDishes()){
            this.dishes.add(d); //Non voglio il riferimento alla lista se no il general item mi viene aggiunto pìu volte
        }
        this.dishes.add(new DishSelection("Service","General",0,0));
        reviewsRef = FirebaseDatabase.getInstance().getReference(REVIEWS).child(order.getRestaurateurId());
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.dish_review_adapter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        DishSelection dish = dishes.get(position);
        holder.dish = dish;
        String dishN = dish.getDish();
        dishN = dishN.equals("GENERAL")? context.getResources().getString(R.string.general) : dishN;
        holder.dishName.setText(dishN);
        holder.fiveStarsView.setAlpha(0.3f);
        holder.fiveStarsView.setStarsClickable(false);
        holder.editReviewIv.setVisibility(View.VISIBLE);
        reviewsRef.child(order.getOrderId()).child(DISHES).child(dish.getCategory()).child(dish.getDish()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int rev = 0;
                String str = "";
                if(dataSnapshot.exists()){
                    Integer tmp = dataSnapshot.child(Firebase.DISH_REVIEW).getValue(Integer.class);
                    str = dataSnapshot.child(Firebase.COMMENT_REVIEW).getValue(String.class);
                    rev = tmp == null ? 0 : tmp;
                    str = str == null ? "" : str;
                    Log.d(TAG,"review :" + tmp + " " + str);
                }
                holder.dish.setReview(rev);
                holder.dish.setCommentReview(str);
                holder.fiveStarsView.setStars(rev);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        holder.commentReviewIv.setVisibility(View.VISIBLE);
        holder.commentReviewIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Add comment");
                mListener.addComment(holder.dish);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    public void addListener(DishReviewAdapterListaner listener){
        mListener = listener;
    }

    public interface DishReviewAdapterListaner {
        void addComment(DishSelection dish);
    }
}
