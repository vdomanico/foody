package it.gangofheroes.foody.customers.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Category;
import it.gangofheroes.foody.customers.model.Dish;
import it.gangofheroes.foody.customers.model.DishReview;
import it.gangofheroes.foody.customers.model.DishSelection;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.model.Rating;
import it.gangofheroes.foody.customers.view.FiveStarsView;
import it.gangofheroes.foody.customers.view.QuantityControllerView;

import static android.content.Context.MODE_PRIVATE;
import static it.gangofheroes.foody.customers.model.Firebase.DISHES;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.ViewHolder>{

    private static final String TAG = DishAdapter.class.getCanonicalName();
    private static final String SHARED_PREF = "it.gangofheroes.foody.customers";
    private String restaurateurId;
    private List<Dish> dishes;
    private Category category;
    private List<DishSelection> dishSelectionList;
    private DishAdapterEventListener dishAdapterEventListener;
    private SharedPreferences mPreferences;
    private final Context context;

    public interface DishAdapterEventListener {
        void onAmountUpdate(String dishName, DishSelection dishSelection);
        void onSeeReviews(DishReview dishReview);
    }

    public void setEventListener(DishAdapterEventListener eventListener){
        this.dishAdapterEventListener = eventListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dishName;
        private TextView dishDesc;
        private ImageView dishImage;
        private TextView dishPrice;
        private TextView dishCurrency;
        private LinearLayout selectedDishLl;
        private TextView dishAmountTv;
        private TextView nRatings;
        private TextView seeReviewsTv;
        private FiveStarsView fiveStarsView;
        private Dish dish;
        private DishReview dishReview = new DishReview();
        private QuantityControllerView quantityControllerView;

        private ViewHolder(View v) {
            super(v);

            dishName = v.findViewById(R.id.dishNameRv);
            dishDesc = v.findViewById(R.id.dishDescRv);
            dishImage = v.findViewById(R.id.dishImageRv);
            dishPrice = v.findViewById(R.id.dishPriceRv);
            dishCurrency = v.findViewById(R.id.dishCurrencyRv);
            selectedDishLl = v.findViewById(R.id.linear_dish_selected);
            dishAmountTv = v.findViewById(R.id.dish_amount);
            fiveStarsView = v.findViewById(R.id.five_stars_dish);
            nRatings = v.findViewById(R.id.n_reviews_dish);
            seeReviewsTv = v.findViewById(R.id.see_ratings);
            quantityControllerView = v.findViewById(R.id.quantity_widget);

            quantityControllerView.setEventListener(new QuantityControllerView.EventListener() {
                @Override
                public void notifyQuantityChanged(int qty) {
                    double price = qty * Double.parseDouble(dishPrice.getText().toString());
                    if(qty==0){
                        selectedDishLl.setVisibility(View.INVISIBLE);
                        dishAdapterEventListener.onAmountUpdate(dish.getName(), new DishSelection(category.getCategory(),dish.getName(),qty,price));
                    }else{
                        selectedDishLl.setVisibility(View.VISIBLE);
                        dishAmountTv.setText(String.valueOf(price));
                        dishAdapterEventListener.onAmountUpdate(dish.getName(),new DishSelection(category.getCategory(),dish.getName(),qty,price));
                    }
                }
            });

            seeReviewsTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dishAdapterEventListener.onSeeReviews(dishReview);
                }
            });

        }
    }

    public DishAdapter(String restaurateurId, Category category, List<DishSelection> list, Context context) {
        this.dishes = category.getOptions();
        this.restaurateurId = restaurateurId;
        this.category = category;
        this.context = context;
        mPreferences = context.getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        if(list == null){
            this.dishSelectionList = new ArrayList<>();
            return;
        }
        this.dishSelectionList = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.dish_adapter, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final Dish dish = dishes.get(position);
        holder.dish = dish;
        String dishN = dish.getName();
        String dishD = dish.getDescription();
        double dishP = dish.getPrice();
        if(dishN!=null && !dishN.equals("")){
            holder.dishName.setText(dishN);
        }
        if(dishD!=null && !dishD.equals("")){
            holder.dishDesc.setText(dishD);
        }
        holder.dishPrice.setText(String.valueOf(dishP));
        holder.dishCurrency.setText(Currency.getInstance(Locale.getDefault()).getSymbol().replaceAll("\\w", ""));
        if(dish.getTempUri()!=null){
            holder.dishImage.setImageURI(Uri.parse(dish.getTempUri()));
        }
        else{
            downloadImagesForDish(restaurateurId, dish, holder.dishImage);

        }

        for(DishSelection dishSelection : dishSelectionList){
            if(dishSelection.getDish().equals(dishN)){
                if(dishSelection.getAmount()>0){
                    holder.selectedDishLl.setVisibility(View.VISIBLE);
                    holder.dishAmountTv.setText(String.valueOf(dishSelection.getAmount()));
                }
                else {
                    holder.selectedDishLl.setVisibility(View.GONE);
                }
                holder.quantityControllerView.setQty(dishSelection.getQuantity());
            }
        }

        getAllDishRatings(holder);

    }

    private void getGlobalRatings(final ViewHolder holder){
        FirebaseDatabase.getInstance().getReference(Firebase.REVIEWS).child(restaurateurId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                List<Integer> sumRatings = new ArrayList<>();
                if(dataSnapshot.exists()){
                    for(DataSnapshot oSnapShot : dataSnapshot.getChildren()) {
                        //per ogni ordineId
                        if (oSnapShot.hasChildren()) {
                            Integer review = oSnapShot.child(category.getCategory()).child(holder.dish.getName()).child(Firebase.REVIEW).getValue(Integer.class);
                            if(review!=null){
                                sumRatings.add(review);
                            }
                        }
                    }
                }
                if(!sumRatings.isEmpty()){
                    int sum = 0;
                    for(int i : sumRatings){
                        sum += i;
                    }
                    holder.fiveStarsView.setStars(sum/sumRatings.size());
                    String ratings = sumRatings.size() + " " + context.getResources().getString(R.string.your_reviews);
                    holder.nRatings.setText(ratings);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
    }

    private void getAllDishRatings(final ViewHolder holder){

        FirebaseDatabase.getInstance().getReference(Firebase.REVIEWS).child(restaurateurId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.exists()){
                    List<Rating> ratings = new ArrayList<>();
                    for(DataSnapshot oSnapShot : dataSnapshot.getChildren()){
                        //per ogni ordine
                        DataSnapshot dishSnapS = oSnapShot.child(Firebase.DISHES).child(category.getCategory()).child(holder.dish.getName());
                        if(dishSnapS.exists()){
                            String cName = null;
                            String cSurname = null;
                            Long reviewDate = null;
                            if(oSnapShot.hasChild(Firebase.CUSTOMER_NAME)){
                                cName = oSnapShot.child(Firebase.CUSTOMER_NAME).getValue(String.class);
                            }
                            if(oSnapShot.hasChild(Firebase.CUSTOMER_SURNAME)){
                                cSurname = oSnapShot.child(Firebase.CUSTOMER_SURNAME).getValue(String.class);
                            }
                            if(oSnapShot.hasChild(Firebase.ORDER_REVIEW_DATE)){
                                reviewDate = oSnapShot.child(Firebase.ORDER_REVIEW_DATE).getValue(Long.class);
                            }

                            Integer review = dishSnapS.child(Firebase.REVIEW).getValue(Integer.class);
                            String commentReview = dishSnapS.child(Firebase.COMMENT_REVIEW).getValue(String.class);
                            if (review != null) {
                                ratings.add(new Rating(review, commentReview, cName, cSurname, reviewDate));
                            }
                        }
                        int tot = 0;
                        if(!ratings.isEmpty()){
                            for(Rating r : ratings){
                                tot += r.getReview();
                            }
                            holder.fiveStarsView.setStars(tot/ratings.size());
                            String rTxt = ratings.size() + " " + context.getResources().getString(R.string.your_reviews);
                            holder.nRatings.setText(rTxt);
                            holder.dishReview =  new DishReview(category.getCategory(),holder.dish.getName(),tot/ratings.size(),tot,ratings.size(),ratings, true);
                            holder.seeReviewsTv.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    private void downloadImagesForDish(String restaurateur, final Dish dish, final ImageView imageView) {

        final StorageReference storageReference = FirebaseStorage.
                getInstance().
                getReference().
                child(restaurateur + "/" + DISHES + "/" + dish.getFileName());

        final File file = getFile(dish.getFileName());

        final boolean successFromDevice = uploadFromDevice(file,imageView);

        storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long timeUpdate = storageMetadata.getUpdatedTimeMillis();
                Log.d(TAG, "time of update" + timeUpdate);
                if(timeUpdate>retrieveLastModifiedOnThisDevice(restaurateurId+"-"+dish.getFileName()) || !successFromDevice){
                    downloadImage(storageReference,file,imageView,timeUpdate,restaurateurId+"-"+dish.getFileName());
                }
            }
        });

    }

    private void downloadImage(StorageReference storageReference, final File file, final ImageView imageView, final Long timeUpdate, final String path){
        storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG,"rest avatar downloaded");
                Uri uri = FileProvider.getUriForFile(context, "it.gangofheroes.foody.customers.android.fileprovider", file);
                imageView.setImageURI(uri);
                saveLastModifiedOnThisDevice(path,timeUpdate);
            }
        });
    }


    private File getFile(String filename){
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir,filename);
    }

    private boolean uploadFromDevice(File file, ImageView imageView){
        if(file.exists() && file.length()>0) {
            Uri uri = Uri.fromFile(file);
            imageView.setImageURI(uri);
            return true;
        }
        return false;
    }

    private void saveLastModifiedOnThisDevice(String key, Long time){
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putLong(key, time);
        preferencesEditor.apply();
    }

    private Long retrieveLastModifiedOnThisDevice(String key){
        return mPreferences.getLong(key,0);
    }

}
