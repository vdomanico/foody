package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.OrderAdapter;
import it.gangofheroes.foody.customers.model.Order;
import it.gangofheroes.foody.customers.model.OrderState;
import it.gangofheroes.foody.customers.util.AnimationUtil;

import static it.gangofheroes.foody.customers.model.Firebase.ORDERS;

public class YourOrdersFragment extends Fragment {


    public static final String TAG = YourOrdersFragment.class.getCanonicalName();
    private YourOrdersFragmentEventListener eventListener;
    private FragmentActivity fragmentActivity;
    private FirebaseUser firebaseUser;
    private List<Order> orders;
    private OrderAdapter orderAdapter;
    private TextView noOrdersTv;
    private View waitingConnectionAnim;
    private boolean isGetOrderFinished = false;
    public static int TITLE = R.string.orders;

    public static Fragment newInstance() {
        return new YourOrdersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        orders = new LinkedList<>();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        if(firebaseUser!=null){
            getOrders();
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        MenuItem item = menu.findItem(R.id.your_basket);
        if(item!=null)
            item.setVisible(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(TITLE);
        eventListener = (YourOrdersFragmentEventListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.your_orders_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        waitingConnectionAnim = view.findViewById(R.id.loader_waiting);
        AnimationUtil.startWaitingAnimation(TAG,fragmentActivity,waitingConnectionAnim);
        noOrdersTv = view.findViewById(R.id.no_orders);
        orderAdapter = new OrderAdapter(getContext(),orders);
        RecyclerView recyclerView = view.findViewById(R.id.orders_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(fragmentActivity));
        recyclerView.setAdapter(orderAdapter);
        orderAdapter.setEventListener(new OrderAdapter.OrderAdapterEventListener() {
            @Override
            public void onEditReview(Order order) {
                eventListener.onEditReview(order);
            }
        });

        Log.d(TAG, "onViewCreated is finishing with isGetOrderFinished="+ isGetOrderFinished);
        if(isGetOrderFinished){
            AnimationUtil.cancelWaitingAnimation(TAG,fragmentActivity,waitingConnectionAnim);
        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle(TITLE);
    }

    private void getOrders(){
        Log.d(TAG, "getOrders()");
        FirebaseDatabase.
                getInstance().
                getReference(ORDERS).
                orderByChild(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "iterating over orders");
                for (DataSnapshot d : dataSnapshot.getChildren()) {
                    Order order = d.getValue(Order.class);
                    if(order!=null && order.getOrderId()!=null){
                        orders.add(order);
                    }
                }
                if(waitingConnectionAnim!=null){
                    AnimationUtil.cancelWaitingAnimation(TAG,fragmentActivity,waitingConnectionAnim);
                }
                if(orders.isEmpty()){
                    noOrdersTv.setVisibility(View.VISIBLE);
                }
                else{
                    noOrdersTv.setVisibility(View.GONE);
                }
                if(orderAdapter!=null){
                    sortOrdersByOrderState(orders);
                    orderAdapter.notifyDataSetChanged();
                }
                isGetOrderFinished = true;
                Log.d(TAG, "iterating over orders finished");



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
               Log.d(TAG,databaseError.getMessage());
                isGetOrderFinished = true;
                Log.d(TAG, "iterating over orders finished");
                if(waitingConnectionAnim!=null){
                    AnimationUtil.cancelWaitingAnimation(TAG,fragmentActivity,waitingConnectionAnim);
                }
            }
        });
    }

    public interface YourOrdersFragmentEventListener{
        void onEditReview(Order order);
    }

    private void sortOrdersByOrderState(List<Order> orders){
        Collections.sort(orders, new Comparator<Order>() {
            @Override
            public int compare(Order o1, Order o2) {

                OrderState oS1 = o1.getOrderState();
                OrderState oS2 = o2.getOrderState();

                if(oS1==oS2){
                    return (int) (o1.getOrderDate()-o2.getOrderDate());
                }

                if(oS1==OrderState.DELIVERED){
                    return 1;
                }

                return (int) (o1.getOrderDate()-o2.getOrderDate());
            }
        });

    }

}
