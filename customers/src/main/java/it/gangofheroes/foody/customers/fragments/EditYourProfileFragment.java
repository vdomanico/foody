package it.gangofheroes.foody.customers.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Customer;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.util.DownloadAvatarUtil;
import it.gangofheroes.foody.customers.util.StringsUtil;

import static android.app.Activity.RESULT_OK;
import static it.gangofheroes.foody.customers.model.Firebase.CUSTOMERS;
import static it.gangofheroes.foody.customers.model.Firebase.CUSTOMER_AVATAR;
import static it.gangofheroes.foody.customers.model.UserType.CUSTOMER;

public class EditYourProfileFragment extends Fragment{

    private static int TITLE = R.string.your_profile;
    public static final String TAG = EditYourProfileFragment.class.getCanonicalName();
    private static final int REQUEST_IMAGE_CAPTURE = 10;
    private static final int REQUEST_IMAGE_GALLERY = 11;
    private static final int CAMERA_PERMISSION = 70;
    private boolean firstTimeCameraRequest;
    private FragmentActivity fragmentActivity;
    private EditYourProfileFragmentEventListener eventListener;
    private FirebaseUser firebaseUser;
    private DatabaseReference customersRef;
    private EditText editName;
    private EditText editSurname;
    private EditText editMail;
    private EditText editAddress;
    private EditText editAddInfo; // TODO integrare
    private EditText editTel;
    private EditText editDesc;
    private ImageView profileIv;
    private Uri profilePictureUri = null;
    private boolean newImage = false;



    public interface EditYourProfileFragmentEventListener{
        void onProfileSave();
    }

    public static Fragment newInstance() {
        return new EditYourProfileFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(TITLE);
        eventListener = (EditYourProfileFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        customersRef = FirebaseDatabase.getInstance().getReference(CUSTOMERS);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_editprofile, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.your_basket);
        if(item!=null)
            item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.edit_profile_done) {
            savePreferences(
                    editName.getText().toString(),
                    editSurname.getText().toString(),
                    editMail.getText().toString(),
                    editTel.getText().toString(),
                    editAddress.getText().toString(),
                    editDesc.getText().toString(),
                    //editAddInfo.getText().toString(), //TODO
                    profilePictureUri);
        }
        eventListener.onProfileSave();
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        editName = view.findViewById(R.id.etName);
        editSurname = view.findViewById(R.id.etSurname);
        editMail = view.findViewById(R.id.etMail);
        editAddress = view.findViewById(R.id.etAddress);
        editAddInfo = view.findViewById(R.id.etAddInfo);
        editTel = view.findViewById(R.id.etPhone);
        editDesc = view.findViewById(R.id.etDesc);
        profileIv = view.findViewById(R.id.iconImage);
        profileIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alog();
            }
        });
        getPreferences();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            newImage = true;
            profileIv.setImageURI(profilePictureUri);
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {
            newImage = true;
            profilePictureUri = data.getData();
            profileIv.setImageURI(profilePictureUri);
        }
    }

    private void getPreferences() {
        Log.d(TAG,"getPreferences()");
        firstTimeCameraRequest = fragmentActivity.getSharedPreferences(StringsUtil.SHARED_PREF,Context.MODE_PRIVATE)
                .getBoolean(StringsUtil.FIRST_TIME_CAMERA_REQUEST, true);
        DownloadAvatarUtil.downloadUserAvatar(firebaseUser.getUid(),
                CUSTOMER,
                profileIv,
                fragmentActivity, EditYourProfileFragment.TAG);
        customersRef.
                child(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Customer customer = null;
                        if (dataSnapshot.exists()) {
                            customer = dataSnapshot.getValue(Customer.class);
                            if(customer!=null){
                                editName.setText(customer.getCustomerName());
                                editSurname.setText(customer.getCustomerSurname());
                                editMail.setText(customer.getCustomerMail());
                                editTel.setText(customer.getCustomerPhone());
                                editAddress.setText(customer.getCustomerAddress());
                                editDesc.setText(customer.getCustomerDesc());
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }

    private void savePreferences(final String name,
                                 final String surname,
                                 final String email,
                                 final String phone,
                                 final String address,
                                 final String desc,
                                 Uri uri) {

        customersRef.child(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "savePreferences.onDataChange()");
                dataSnapshot.child(Firebase.CUSTOMER_NAME).getRef().setValue(name);
                dataSnapshot.child(Firebase.CUSTOMER_SURNAME).getRef().setValue(surname);
                dataSnapshot.child(Firebase.CUSTOMER_MAIL).getRef().setValue(email);
                dataSnapshot.child(Firebase.CUSTOMER_PHONE).getRef().setValue(phone);
                dataSnapshot.child(Firebase.CUSTOMER_ADDRESS).getRef().setValue(address);
                dataSnapshot.child(Firebase.CUSTOMER_DESC).getRef().setValue(desc);
                Toast.makeText(fragmentActivity,R.string.data_saved_success,Toast.LENGTH_SHORT).show();
                eventListener.onProfileSave();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });

        if(uri!=null && newImage){
            Log.d(TAG,"savePreferences newImage");
            DownloadAvatarUtil.uploadUserAvatar(uri,
                    profilePictureUri,
                    firebaseUser.getUid(),
                    CUSTOMER,
                    fragmentActivity,
                    TAG);
        }

    }
    private void alog() {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(fragmentActivity, R.style.DialogTheme);
        alertDiaolg.setTitle("Upload Pictures Option"); //FIXME usare R.string.something
        alertDiaolg.setMessage("How do you want to set your picture?");
        alertDiaolg.setPositiveButton("Camera",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        takePictureFromCamera();
                    }
                });
        alertDiaolg.setNegativeButton("Gallery",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        selectPictureFromGallery();
                    }
                });
        alertDiaolg.create().show();
    }

    private void selectPictureFromGallery() {
        Intent selectPictureIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        selectPictureIntent.setType("image/*");
        startActivityForResult(selectPictureIntent, REQUEST_IMAGE_GALLERY);
    }

    private void takePictureFromCamera() {
        if (ContextCompat.checkSelfPermission(fragmentActivity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            loadFromCamera();
        } else {
            if (firstTimeCameraRequest || shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                if (ContextCompat.checkSelfPermission(fragmentActivity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(fragmentActivity, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
                }
            }
        }
    }

    private void loadFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(fragmentActivity.getPackageManager()) != null) {
            File photoFile;
            try {
                photoFile = new File(fragmentActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES),
                        CUSTOMER_AVATAR);
            } catch (Exception ex) {
                String error_message = getResources().getString(R.string.error_msg);
                Toast.makeText(fragmentActivity, error_message, Toast.LENGTH_SHORT).show();
                return;
            }
            profilePictureUri = FileProvider.getUriForFile(fragmentActivity, StringsUtil.PROVIDER_NAME, photoFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, profilePictureUri);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       if(requestCode == CAMERA_PERMISSION){
           if (firstTimeCameraRequest) {
               SharedPreferences.Editor preferencesEditor = fragmentActivity.getSharedPreferences(StringsUtil.SHARED_PREF,Context.MODE_PRIVATE).edit();
               preferencesEditor.putBoolean(StringsUtil.FIRST_TIME_CAMERA_REQUEST,false);
               preferencesEditor.apply();
           }
           if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
               loadFromCamera();
           } else if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
               if (!shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                   Toast.makeText(fragmentActivity, R.string.settings_to_get_camera, Toast.LENGTH_LONG).show();
               }
           }
        }
    }

}
