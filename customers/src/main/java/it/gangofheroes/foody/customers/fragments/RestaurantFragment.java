package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.MenuAdapter;
import it.gangofheroes.foody.customers.model.BasketInterface;
import it.gangofheroes.foody.customers.model.Category;
import it.gangofheroes.foody.customers.model.Restaurateur;

import static android.content.Context.MODE_PRIVATE;
import static android.view.View.GONE;
import static it.gangofheroes.foody.customers.model.Firebase.RESTAURATEUR_AVATAR;

public class RestaurantFragment extends Fragment implements OnMapReadyCallback {

    public static final String TAG = Restaurateur.class.getCanonicalName();
    private static final String SHARED_PREF = "it.gangofheroes.foody.customers";
    private static final String KEY_ORDER = "KEY_RESTAURATEUR";
    private DatabaseReference restaurantsRef;
    private ImageView restaurateurAvatarIv;
    private Restaurateur restaurateur;
    private MenuAdapter menuAdapter;
    private RestaurateurFragmentEventListener eventListener;
    private SharedPreferences mPreferences;
    private FragmentActivity fragmentActivity;
    private GoogleMap gMap;
    public static String TITLE = "";
    private MapView mapView;
    private LatLng loc;

    public interface RestaurateurFragmentEventListener extends BasketInterface {
        void onMenuCategoryShow(Restaurateur restaurateur, Category category, int position);
    }

    public static Fragment newInstance (Restaurateur restaurateur){
        Fragment fragment = new RestaurantFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_ORDER, restaurateur);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        eventListener = (RestaurateurFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        mPreferences = fragmentActivity.getSharedPreferences(SHARED_PREF, MODE_PRIVATE);
        if (getArguments() != null){
            restaurateur = (Restaurateur) getArguments().get(KEY_ORDER);
        }
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view =  inflater.inflate(R.layout.restaurant_fragment, container, false);
        restaurateurAvatarIv = view.findViewById(R.id.restaurant_avatar_frag);
        mapView = view.findViewById(R.id.restaurant_map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        downloadRestaurateurAvatar(restaurateur.getRestaurateurId(),restaurateurAvatarIv);
        return view;
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle(TITLE);
        fragmentActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        TextView name = view.findViewById(R.id.restaurant_name_frag);
        TextView address = view.findViewById(R.id.restaurant_address_frag);
        TextView description = view.findViewById(R.id.restaurant_description_frag);
        TabLayout tabLayout = view.findViewById(R.id.tablayout);
        final RecyclerView recyclerView = view.findViewById(R.id.lv_frag);
        if(restaurateur!=null){
            TITLE = restaurateur.getRestaurantName();
            fragmentActivity.setTitle(TITLE);
            menuAdapter = new MenuAdapter(restaurateur.getMenu());
            menuAdapter.setEventListener(new MenuAdapter.MenuAdapterEventListener() {
                @Override
                public void onMenuCategoryShow(Category category, int position) {
                    eventListener.onMenuCategoryShow(restaurateur,category,position);

                }
            });
            if(restaurateur.getRestaurantName() == null){
                name.setVisibility(GONE);
            }
            if(restaurateur.getRestaurantAddress() == null){
                address.setVisibility(GONE);
            }
            if(restaurateur.getDescription() == null){
                description.setVisibility(GONE);
            }
            name.setText(restaurateur.getRestaurantName() != null ? restaurateur.getRestaurantName() : "non disponibile");
            address.setText(restaurateur.getRestaurantAddress() != null ? restaurateur.getRestaurantAddress() : "non disponibile");
            description.setText(restaurateur.getDescription() != null ? restaurateur.getDescription() : "non disponibile");
            recyclerView.setAdapter(menuAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.BaseOnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getPosition()==0){
                    recyclerView.setVisibility(View.VISIBLE);
                    mapView.setVisibility(GONE);
                }
                else{
                    recyclerView.setVisibility(GONE);
                    mapView.setVisibility(View.VISIBLE);
                    setupRestaurantLocation();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG,"onMapReady()");
        gMap = googleMap;
    }

    private void downloadRestaurateurAvatar(final String restaurateurId, final ImageView restaurateurAvatarIv) {

        final StorageReference storageReference = FirebaseStorage.
                getInstance().
                getReference().
                child(restaurateurId + "/" + RESTAURATEUR_AVATAR);

        final File file = getFile("restaurateurAvatar-"+restaurateurId);
        final boolean successFromDevice = uploadFromDevice(file,restaurateurAvatarIv);

        storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long timeUpdate = storageMetadata.getUpdatedTimeMillis();
                Log.d(TAG, "time of update" + timeUpdate);
                if(timeUpdate>retrieveLastModifiedOnThisDevice("restaurateurAvatar-"+restaurateurId) || !successFromDevice){
                    downloadImage(storageReference,file,restaurateurAvatarIv,timeUpdate,restaurateurId);
                }
            }
        });

    }

    private void downloadImage(StorageReference storageReference, final File file, final ImageView imageView, final Long timeUpdate, final String restaurateurId){
        storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG,"rest avatar downloaded");
                Uri uri = FileProvider.getUriForFile(fragmentActivity, "it.gangofheroes.foody.customers.android.fileprovider", file);
                imageView.setImageURI(uri);
                saveLastModifiedOnThisDevice("restaurateurAvatar-"+restaurateurId,timeUpdate);
            }
        });
    }

    private File getFile(String filename){
        File storageDir = fragmentActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir,filename);
    }

    private boolean uploadFromDevice(File file, ImageView imageView){
        if(file.exists() && file.length()>0) {
            Uri uri = Uri.fromFile(file);
            imageView.setImageURI(uri);
            return true;
        }
        return false;
    }

    private void saveLastModifiedOnThisDevice(String key, Long time){
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putLong(key, time);
        preferencesEditor.apply();
    }

    private Long retrieveLastModifiedOnThisDevice(String key){
        return mPreferences.getLong(key,0);
    }

    private void setupRestaurantLocation() {
        Log.d(TAG,"setupRestaurantLocation()");
        if(gMap != null && restaurateur!=null && restaurateur.getRestaurantAddress()!=null){
            if(loc==null){
                loc = getLocationFromAddress(fragmentActivity,restaurateur.getRestaurantAddress());
            }
            if(loc!=null){
                gMap.addMarker(new MarkerOptions().position(loc).title(restaurateur.getRestaurantName()));
                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,16.0f));
            }

        }
    }

    private LatLng getLocationFromAddress(Context context, String strAddress){
        Geocoder coder= new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 1);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
        return p1;

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
