package it.gangofheroes.foody.customers.model;

public class Firebase {

    public final static String RESTAURATEURS = "restaurateurs-f";
    public final static String CUSTOMERS = "customers-f";
    public final static String BIKERS = "bikers-f";
    public final static String ORDERS = "orders-f";


    public final static String RESTAURATEUR_ID = "restaurateurId";
    public final static String RESTAURANT_NAME = "restaurantName";
    public static final String RESTAURANT_MAIL = "restaurantMail";
    public static final String RESTAURANT_PHONE = "restaurantPhone";
    public static final String RESTAURANT_ADDRESS = "restaurantAddress";
    public static final String RESTAURANT_DESC = "restaurantDescription";
    public final static String RESTAURATEUR_AVATAR = "restaurateurAvatar";

    public static final String CUSTOMER_ID = "customerId";
    public static final String CUSTOMER_NAME = "customerName";
    public static final String CUSTOMER_SURNAME = "customerSurname";
    public static final String CUSTOMER_MAIL = "customerMail";
    public static final String CUSTOMER_PHONE = "customerPhone";
    public static final String CUSTOMER_ADDRESS = "customerAddress";
    public static final String CUSTOMER_DESC = "customerDesc";
    public final static String CUSTOMER_AVATAR = "customerAvatar";

    public final static String BIKER_ID = "bikerId";
    public static final String BIKER_NAME = "bikerName";
    public static final String BIKER_SURNAME = "bikerSurname";
    public static final String BIKER_MAIL = "bikerMail";
    public static final String BIKER_PHONE = "bikerPhone";
    public final static String BIKER_AVATAR = "bikerAvatar";
    public final static String BIKER_STATUS = "userStatus";

    public final static String DISHES = "dishes";

    public static final String ORDER_DELIVERER_ID = "delivererId";
    public static final String ORDER_STATUS = "orderState";
    public static final String ORDER_DELIVERY_TIME = "deliveryTime";
    public static final String GLOBAL_REVIEW = "globalReview";
    public static final String COMMENT_REVIEW = "commentReview";
    public static final String ORDER_REVIEW_DATE = "reviewDate";
    public static final String ORDER_DISHES = "dishes";
    public static final String DISH_REVIEW = "review";

    public static final String REVIEWS = "reviews";
    public static final String REVIEW = "review";
    public static final String ORDER_DATE = "orderDate";


    public static String getRefAvatar(UserType userType){
        switch (userType){
            case RESTAURATEUR:
                return RESTAURATEUR_AVATAR;
            case BIKER:
                return BIKER_AVATAR;
            case CUSTOMER:
                return CUSTOMER_AVATAR;
        }
        return null;
    }

}
