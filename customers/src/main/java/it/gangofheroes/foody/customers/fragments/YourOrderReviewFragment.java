package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.annotations.NotNull;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.DishReviewAdapter;
import it.gangofheroes.foody.customers.model.DishSelection;
import it.gangofheroes.foody.customers.model.Order;

public class YourOrderReviewFragment extends Fragment {

    public static final String TAG = YourOrderReviewFragment.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private Order order;
    private YourOrderReviewFragmentListener mListener;

    public static Fragment newInstance(Order order) {
        YourOrderReviewFragment yourOrderReviewFragment = new YourOrderReviewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("ORDER",order);
        yourOrderReviewFragment.setArguments(bundle);
        return yourOrderReviewFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(getArguments()!=null){
            order = getArguments().getParcelable("ORDER");
        }
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        Log.d(TAG,"onPrepareOptionsMenu");
        MenuItem item = menu.findItem(R.id.your_basket);
        if(item!=null)
            item.setVisible(false);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(R.string.review);

        if (context instanceof YourOrderReviewFragmentListener){
            mListener = (YourOrderReviewFragmentListener) context;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.your_review_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        DishReviewAdapter dishReviewAdapter = new DishReviewAdapter(fragmentActivity, order);
        dishReviewAdapter.addListener(new DishReviewAdapter.DishReviewAdapterListaner() {
            @Override
            public void addComment(DishSelection dish) {
                Log.d(TAG, "addComment");
                mListener.addComment(order, dish);

            }
        });
        RecyclerView recyclerView = view.findViewById(R.id.order_review_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(fragmentActivity));
        recyclerView.setAdapter(dishReviewAdapter);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle(R.string.review);
    }

    public interface YourOrderReviewFragmentListener {
        void addComment(Order order, DishSelection dishSelection);
    }
}
