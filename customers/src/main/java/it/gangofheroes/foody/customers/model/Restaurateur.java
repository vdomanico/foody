package it.gangofheroes.foody.customers.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Restaurateur implements Parcelable{

    private String restaurateurId;
    private String restaurantName;
    private String restaurantAddress;
    private String restaurantPhone;
    private String restaurantMail;
    private String description;
    private String lunchAndDinner;
    private Float distance; //proprietà locale e non del db
    private Menu menu;
    private Double avgRatings;
    private Integer noRatings;

    public Restaurateur(){
        distance = 0f;
        avgRatings = 0.0;
        noRatings = 0;
    }

    private Restaurateur(Parcel in) {
        restaurateurId = in.readString();
        restaurantName = in.readString();
        restaurantAddress = in.readString();
        restaurantPhone = in.readString();
        restaurantMail = in.readString();
        description = in.readString();
        lunchAndDinner = in.readString();
        distance = in.readFloat();
        menu = in.readParcelable(Menu.class.getClassLoader());
        avgRatings = in.readDouble();
        noRatings = in.readInt();
    }

    public static final Creator<Restaurateur> CREATOR = new Creator<Restaurateur>() {
        @Override
        public Restaurateur createFromParcel(Parcel in) {
            return new Restaurateur(in);
        }

        @Override
        public Restaurateur[] newArray(int size) {
            return new Restaurateur[size];
        }
    };

    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public String getRestaurantPhone() {
        return restaurantPhone;
    }

    public void setRestaurantPhone(String restaurantPhone) {
        this.restaurantPhone = restaurantPhone;
    }

    public String getRestaurantMail() {
        return restaurantMail;
    }

    public void setRestaurantMail(String restaurantMail) {
        this.restaurantMail = restaurantMail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLunchAndDinner() {
        return lunchAndDinner;
    }

    public void setLunchAndDinner(String lunchAndDinner) {
        this.lunchAndDinner = lunchAndDinner;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public String getRestaurateurId() {
        return restaurateurId;
    }

    public void setRestaurateurId(String restaurateurId) {
        this.restaurateurId = restaurateurId;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public Double getAvgRatings() {
        return avgRatings;
    }

    public void setAvgRatings(Double avgRatings) {
        this.avgRatings = avgRatings;
    }

    public Integer getNoRatings() {
        return noRatings;
    }

    public void setNoRatings(Integer noRatings) {
        this.noRatings = noRatings;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(restaurateurId);
        dest.writeString(restaurantName);
        dest.writeString(restaurantAddress);
        dest.writeString(restaurantPhone);
        dest.writeString(restaurantMail);
        dest.writeString(description);
        dest.writeString(lunchAndDinner);
        dest.writeFloat(distance);
        dest.writeParcelable(menu, flags);
        dest.writeDouble(avgRatings);
        dest.writeInt(noRatings);
    }
}
