package it.gangofheroes.foody.customers.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Firebase;
import it.gangofheroes.foody.customers.model.Order;
import it.gangofheroes.foody.customers.model.OrderState;
import it.gangofheroes.foody.customers.model.UserType;
import it.gangofheroes.foody.customers.util.DownloadAvatarUtil;
import it.gangofheroes.foody.customers.view.FiveStarsView;

import static it.gangofheroes.foody.customers.model.Firebase.RESTAURANT_NAME;
import static it.gangofheroes.foody.customers.model.Firebase.RESTAURATEURS;
import static it.gangofheroes.foody.customers.model.Firebase.REVIEWS;


public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private static final String TAG = OrderAdapter.class.getCanonicalName();
    private final Context context;
    private OrderAdapterEventListener eventListener;

    private List<Order> orders;
    public OrderAdapter(Context context, List<Order> orders){
        this.context = context;
        this.orders= orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.order_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");

        holder.order = orders.get(position);

        DownloadAvatarUtil.downloadUserAvatar(holder.order.getRestaurateurId(),
                UserType.RESTAURATEUR,
                holder.restaurantAvatar,
                context,
                TAG);

        FirebaseDatabase.getInstance().
                getReference(REVIEWS).
                child(holder.order.getRestaurateurId()).
                child(holder.order.getOrderId()).
                child(Firebase.GLOBAL_REVIEW).
                addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int rev = 0;
                if(dataSnapshot.exists()){
                    Integer tmp = dataSnapshot.getValue(Integer.class);
                    rev = tmp == null ? 0 : tmp;
                }
                holder.fiveStarsView.setStars(rev);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        getRestaurantName(holder.order.getRestaurateurId(),holder.restaurantNameTv);
        String currency = Currency.getInstance(Locale.getDefault()).getSymbol().replaceAll("\\w", "") + " " + holder.order.getAmount();
        holder.orderPriceTv.setText(currency);
        if(pendingState.contains(holder.order.getOrderState())){
            holder.orderStatusTv.setText(R.string.pending);
        }else if(holder.order.getOrderState() == OrderState.DELIVERING){
            holder.orderStatusTv.setText(R.string.delivering);
        }
        else if(holder.order.getOrderState() == OrderState.DELIVERED){
            holder.orderStatusTv.setText(R.string.delivered);
        }
        Long millis = holder.order.getOrderDate();
        Long millisReview = holder.order.getReviewDate();
        if(millis!=null){
            holder.orderDateTv.setText(DateFormat.getDateInstance().format(new Date(millis)));
        }

        if(holder.order.getOrderState() == OrderState.DELIVERED){
            holder.reviewLayout.setVisibility(View.VISIBLE);
            Date today = new Date();
            long todayMillis = today.getTime();
            if(millisReview==null || (millisReview + 60*60*24*1000) >= todayMillis){
                holder.editReviewIv.setVisibility(View.VISIBLE);
            }
            holder.fiveStarsView.setAlpha(0.3f);
            holder.fiveStarsView.setStarsClickable(false);
        }


    }

    private void getRestaurantName(String restaurateurId, final TextView textView) {
        Log.d(TAG, "getRestaurantName()");
        FirebaseDatabase.
                getInstance().
                getReference(RESTAURATEURS).
                child(restaurateurId).
                child(RESTAURANT_NAME).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Log.d(TAG, "getRestaurantName.onDataChange()");
                        String restName = dataSnapshot.getValue(String.class);
                        if(restName!=null){
                            textView.setText(restName);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private Order order;
        private ImageView restaurantAvatar;
        private TextView restaurantNameTv;
        private TextView orderPriceTv;
        private TextView orderStatusTv;
        private TextView orderDateTv;
        private TextView yourReviewTextTv;
        private LinearLayout reviewLayout;
        private FiveStarsView fiveStarsView;
        private ImageView editReviewIv;


        private ViewHolder(View v) {
            super(v);
            Log.d(TAG, "ViewHolder");
            restaurantAvatar = v.findViewById(R.id.restaurant_avatar);
            restaurantNameTv = v.findViewById(R.id.restaurant);
            orderPriceTv = v.findViewById(R.id.order_totalPrice);
            orderStatusTv = v.findViewById(R.id.order_status_value);
            orderDateTv = v.findViewById(R.id.order_dates_value);
            reviewLayout = v.findViewById(R.id.bottomLinear);
            fiveStarsView = v.findViewById(R.id.starsLayout_f);
            editReviewIv = v.findViewById(R.id.edit_review_f);
            yourReviewTextTv = v.findViewById(R.id.your_review_text);

            editReviewIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "OrderAdapter -> ReviewOrderActivity");
                    eventListener.onEditReview(order);
                }
            });


        }
    }

    public void setEventListener(OrderAdapterEventListener eventListener){
        this.eventListener = eventListener;
    }

    public interface OrderAdapterEventListener{
        void onEditReview(Order order);
    }

    private static List<OrderState> pendingState = Arrays.asList(OrderState.ACCEPTED,OrderState.ASSIGNED,OrderState.DECLINED);
}
