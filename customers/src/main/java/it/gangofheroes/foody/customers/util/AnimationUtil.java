package it.gangofheroes.foody.customers.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;

public class AnimationUtil {

    public static void startWaitingAnimation(final String TAG, Context context, final View view) {
        Log.d(TAG, "startWaitingAnimation");
        Activity activity = (Activity) context;
        if(activity!=null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    view.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public static void cancelWaitingAnimation(final String TAG, Context context, final View view) {
        Log.d(TAG, "cancelWaitingAnimation");
        Activity activity = (Activity) context;
        if(activity!=null){
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    view.setVisibility(View.GONE);
                }
            });
        }
    }

}
