package it.gangofheroes.foody.customers.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import it.gangofheroes.foody.customers.R
import it.gangofheroes.foody.customers.model.DishSelection
import it.gangofheroes.foody.customers.model.Order
import kotlinx.android.synthetic.main.add_comment_fragment.*

class AddCommentFragment: DialogFragment(){

    private var mLIstener: AddCommentFragmentListener? = null
    private lateinit var order: Order
    private lateinit var ds: DishSelection

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setStyle(STYLE_NO_FRAME, R.style.AppTheme_Dialog_Alert)

        arguments?.let{
            order = it.getParcelable(KEY_ORDER) ?: Order()
            ds = it.getParcelable(KEY_DS) ?: DishSelection()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.add_comment_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")

        comment.setText(ds.commentReview ?: "");

        ok.setOnClickListener {
            Log.d(TAG, "click on OK")
            if (comment.text.isNotEmpty()){
                ds.commentReview = comment.text.toString();
                mLIstener?.commentAdded(order, ds)
            }
            else {
                Toast.makeText(context, "Please add a comment", Toast.LENGTH_SHORT).show()
            }
        }

        cancel.setOnClickListener {
            Log.d(TAG, "click ok cancel")
            mLIstener?.cancelPressed()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttach")
        if (context is AddCommentFragmentListener){
            mLIstener = context
        }
    }

    /**
     * Listener
     */
    interface AddCommentFragmentListener {
        fun commentAdded(order: Order, dish: DishSelection)
        fun cancelPressed()
    }

    /**
     * Companion Object
     */
    companion object {
        const val TAG = "AddCommentFragment"
        private const val KEY_ORDER = "KEY_ORDER"
        private const val KEY_DS = "KEY_DS"
        fun newInstance(order: Order, dishSelection: DishSelection) = AddCommentFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_ORDER, order)
                putParcelable(KEY_DS, dishSelection)
            }
        }
    }
}
