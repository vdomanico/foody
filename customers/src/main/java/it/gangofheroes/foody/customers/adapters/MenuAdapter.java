package it.gangofheroes.foody.customers.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Category;
import it.gangofheroes.foody.customers.model.Menu;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {

    public interface MenuAdapterEventListener {
        void onMenuCategoryShow(Category category, int position);
    }

    private static final String TAG = MenuAdapter.class.getCanonicalName();
    private List<Category> categories;
    private MenuAdapterEventListener eventListener;

    public void setEventListener(MenuAdapterEventListener eventListener){
        this.eventListener = eventListener;
    }

    public MenuAdapter(Menu menu){
        this.categories = menu!=null?menu.getCategories(): new ArrayList<Category>();

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.menu_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupViewHolder(categories.get(position), position);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ConstraintLayout editCategoryMenuIv;
        private TextView categoryMenuTv;
        private ImageView removeCategoryIv;
        private Category category;


        ViewHolder(@NonNull View itemView) {
            super(itemView);
            editCategoryMenuIv = itemView.findViewById(R.id.menu_category_layout);
            categoryMenuTv = itemView.findViewById(R.id.menu_category_value);
        }

        private void setupViewHolder(final Category c, final int position){
            this.category = c;
            categoryMenuTv.setText(category.getCategory());
            editCategoryMenuIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventListener.onMenuCategoryShow(category, position);
                }
            });
        }
    }
}
