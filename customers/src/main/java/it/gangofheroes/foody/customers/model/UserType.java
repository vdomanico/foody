package it.gangofheroes.foody.customers.model;

public enum UserType {
    RESTAURATEUR, CUSTOMER, BIKER
}
