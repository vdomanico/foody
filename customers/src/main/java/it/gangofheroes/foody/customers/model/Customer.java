package it.gangofheroes.foody.customers.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Customer implements Parcelable {

    private String customerName;
    private String customerSurname;
    private String customerMail;
    private String customerPhone;
    private String customerAddress;
    private String customerDesc;
    private String customerId;

    public Customer() {
    }

    protected Customer(Parcel in) {
        customerName = in.readString();
        customerSurname = in.readString();
        customerMail = in.readString();
        customerPhone = in.readString();
        customerAddress = in.readString();
        customerDesc = in.readString();
        customerId = in.readString();
    }

    public static final Creator<Customer> CREATOR = new Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCustomerMail() {
        return customerMail;
    }

    public void setCustomerMail(String customerMail) {
        this.customerMail = customerMail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerDesc() {
        return customerDesc;
    }

    public void setCustomerDesc(String customerDesc) {
        this.customerDesc = customerDesc;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerName);
        dest.writeString(customerSurname);
        dest.writeString(customerMail);
        dest.writeString(customerPhone);
        dest.writeString(customerAddress);
        dest.writeString(customerDesc);
        dest.writeString(customerId);
    }
}
