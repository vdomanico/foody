package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.DishAdapter;
import it.gangofheroes.foody.customers.model.BasketInterface;
import it.gangofheroes.foody.customers.model.Category;
import it.gangofheroes.foody.customers.model.DishReview;
import it.gangofheroes.foody.customers.model.DishSelection;
import it.gangofheroes.foody.customers.model.Restaurateur;

public class CategoryFragment extends Fragment {

    private int categoryPosition;
    private Map<String,DishSelection> dishes;
    private Restaurateur restaurateur;
    private FragmentActivity fragmentActivity;
    public static String TITLE = "";

    public static Fragment newInstance(Restaurateur restaurateur, Category category, int categoryPosition, ArrayList<DishSelection> dishes) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("RESTAURATEUR",restaurateur);
        bundle.putParcelable("CATEGORY", category);
        bundle.putInt("CATEGORY_POSITION",categoryPosition);
        bundle.putParcelableArrayList("DISHES", dishes);
        CategoryFragment categoryFragment = new CategoryFragment();
        categoryFragment.setArguments(bundle);
        return categoryFragment;
    }

    public interface EditMenuFragmentEventListener extends BasketInterface {
        void onBasketNewDish(String restaurateurId, Map<String, DishSelection> dishes, String lastDishAdded);
        void onSeeComment(DishReview dishReview);
    }

    public static final String TAG = CategoryFragment.class.getCanonicalName();
    private EditMenuFragmentEventListener eventListener;
    private DishAdapter dishAdapter;
    private Category category;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        eventListener = (EditMenuFragmentEventListener) context;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dishes = new HashMap<>();
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_category_fragment, container, false);

        if (getArguments()!=null){
            restaurateur = getArguments().getParcelable("RESTAURATEUR");
            category =  getArguments().getParcelable("CATEGORY");
            categoryPosition = getArguments().getInt("CATEGORY_POSITION");
            List<DishSelection> list = getArguments().getParcelableArrayList("DISHES");
            if(restaurateur!=null){
                TITLE = restaurateur.getRestaurantName();
                fragmentActivity.setTitle(TITLE);
            }
            if(list!=null){
                for(DishSelection d : list) {
                    dishes.put(d.getDish(),d);
                }
            }

            setupView(view);
        }
        return view;
    }


    private void setupView(View view) {

        if(category!=null){
            dishAdapter = new DishAdapter(restaurateur.getRestaurateurId(), category, new ArrayList<>(dishes.values()),fragmentActivity);
            dishAdapter.setEventListener(new DishAdapter.DishAdapterEventListener() {
                @Override
                public void onAmountUpdate(String dishName, DishSelection dishSelection) {
                    Log.d(TAG,dishName + " " + dishSelection.getQuantity() + " " + dishSelection.getAmount());
                    dishes.put(dishName,dishSelection);
                    eventListener.onBasketNewDish(restaurateur.getRestaurateurId(),dishes,dishName);
                }

                @Override
                public void onSeeReviews(DishReview dishReview) {
                    eventListener.onSeeComment(dishReview);
                }
            });
        }
        RecyclerView recyclerView = view.findViewById(R.id.category_menu);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(dishAdapter);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle(TITLE);
    }

}
