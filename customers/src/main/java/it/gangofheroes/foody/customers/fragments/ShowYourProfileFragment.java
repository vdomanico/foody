package it.gangofheroes.foody.customers.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.model.Customer;
import it.gangofheroes.foody.customers.util.DownloadAvatarUtil;

import static it.gangofheroes.foody.customers.model.Firebase.CUSTOMERS;
import static it.gangofheroes.foody.customers.model.UserType.CUSTOMER;

public class ShowYourProfileFragment extends Fragment{

    private static int TITLE = R.string.your_profile;
    public static final String TAG = ShowYourProfileFragment.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private ShowYourProfileFragmentEventListener eventListener;
    private FirebaseUser firebaseUser;
    private DatabaseReference customersRef;
    private TextView tvName;
    private TextView tvSurname;
    private TextView tvMail;
    private TextView tvShortDescription;
    private TextView tvAddress;
    private TextView tvPhone;
    private TextView tvAdditionalInfo; // TODO integrare
    private ImageView profileIv;

    public interface ShowYourProfileFragmentEventListener{
        void onProfileEdit();
    }

    public static Fragment newInstance() {
        return new ShowYourProfileFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(TITLE);
        eventListener = (ShowYourProfileFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        customersRef = FirebaseDatabase.getInstance().getReference(CUSTOMERS);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        Log.d(TAG,"onCreateOptionsMenu");
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_showprofile, menu);
    }

    @Override
    public void onPrepareOptionsMenu(@NotNull Menu menu) {
        Log.d(TAG,"onPrepareOptionsMenu");
        MenuItem item = menu.findItem(R.id.your_basket);
        if(item!=null)
            item.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        Log.d(TAG,"onOptionsItemSelected");
        if (item.getItemId() == R.id.edit_profile_show) {
            eventListener.onProfileEdit();
        }
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.edit_profile_activity, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        tvName = view.findViewById(R.id.name);
        tvSurname = view.findViewById(R.id.surname);
        tvMail = view.findViewById(R.id.mail);
        tvAddress = view.findViewById(R.id.delivery_address);
        tvAdditionalInfo = view.findViewById(R.id.add_info);
        tvPhone = view.findViewById(R.id.phone_number);
        tvShortDescription = view.findViewById(R.id.desc);
        profileIv = view.findViewById(R.id.show_profile_avatar_fragment);
        getPreferences();
    }

    private void getPreferences() {
        Log.d(TAG,"getPreferences()");
        DownloadAvatarUtil.downloadUserAvatar(firebaseUser.getUid(),
                CUSTOMER,
                profileIv,
                fragmentActivity, ShowYourProfileFragment.TAG);
        customersRef.
                child(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Customer customer = null;
                        if (dataSnapshot.exists()) {
                            customer = dataSnapshot.getValue(Customer.class);
                            if(customer!=null){
                                tvName.setText(customer.getCustomerName());
                                tvSurname.setText(customer.getCustomerSurname());
                                tvMail.setText(customer.getCustomerMail());
                                tvPhone.setText(customer.getCustomerPhone());
                                tvAddress.setText(customer.getCustomerAddress());
                                tvShortDescription.setText(customer.getCustomerDesc());
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }


}
