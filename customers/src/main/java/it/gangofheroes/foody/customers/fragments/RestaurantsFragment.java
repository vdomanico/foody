package it.gangofheroes.foody.customers.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.chip.Chip;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import it.gangofheroes.foody.customers.R;
import it.gangofheroes.foody.customers.adapters.RestaurateurAdapter;
import it.gangofheroes.foody.customers.model.Restaurateur;
import it.gangofheroes.foody.customers.services.FetchAddressIntentService;
import it.gangofheroes.foody.customers.task.GetLatLngAsyncTask;
import it.gangofheroes.foody.customers.util.AnimationUtil;

import static android.content.Context.LOCATION_SERVICE;
import static com.google.android.libraries.places.api.Places.createClient;
import static it.gangofheroes.foody.customers.model.Firebase.RESTAURANT_NAME;
import static it.gangofheroes.foody.customers.model.Firebase.RESTAURATEURS;

public class RestaurantsFragment extends Fragment implements
        RestaurateurAdapter.RestaurateurAdapterListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String TAG = RestaurantsFragment.class.getCanonicalName();

    private static final String KEY_RESTAURATEUR = "KEY_RESTAURATEUR";
    private static final int LOCATION_PERMISSION = 275;
    private ArrayList<Restaurateur> restaurateurs;
    private RestaurateurFragmentListener eventListener;
    private FragmentActivity fragmentActivity;
    public static int TITLE = R.string.your_area;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private AutoCompleteTextView yourAddressEt;
    private View waitingConnectionAnim;
    private RecyclerView recyclerView;
    private DatabaseReference restaurantsRef;
    private static int restaurantFound = 0;
    private RestaurateurAdapter restaurateurAdapter;
    private TextView searchRestaurantsTv;
    private PlacesClient placesClient;
    private AutocompleteSessionToken token;
    private Chip filterRatings;
    private Chip filterDistance;
    private Chip filterPopularity;
    private View.OnClickListener chipListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "chipGroup: some chip selected");
            applyFilter();
        }
    };

    private void applyFilter() {

        if(restaurateurs==null || restaurateurs.isEmpty()) {
            return;
        }

        if(filterRatings.isChecked() && filterDistance.isChecked() && filterPopularity.isChecked()){
            sortAscByAll();
        }
        else if(filterRatings.isChecked() && filterDistance.isChecked() && !filterPopularity.isChecked()){
            sortAscByRatingsAndDistance();
        }
        else if(!filterRatings.isChecked() && filterDistance.isChecked() && filterPopularity.isChecked()){
            sortAscByPopularityAndDistance();
        }
        else if(filterRatings.isChecked() && !filterDistance.isChecked() && filterPopularity.isChecked()){
            sortAscByRatingsAndPopularity();
        }
        else if(filterRatings.isChecked() && !filterDistance.isChecked() && !filterPopularity.isChecked()){
            sortAscByRatings();
        }
        else if(!filterRatings.isChecked() && filterDistance.isChecked() && !filterPopularity.isChecked()){
            sortAscByDistance();
        }
        else if(!filterRatings.isChecked() && !filterDistance.isChecked() && filterPopularity.isChecked()){
            sortAscByPopularity();
        }

        if(restaurateurAdapter!=null){
            restaurateurAdapter.notifyDataSetChanged();
        }

    }

    private void startFetchAddressIntentService(Location location) {
        Intent intent = new Intent(fragmentActivity, FetchAddressIntentService.class);
        intent.putExtra(FetchAddressIntentService.Constants.RECEIVER, new AddressResultReceiver(new Handler()));
        intent.putExtra(FetchAddressIntentService.Constants.LOCATION_DATA_EXTRA, location);
        fragmentActivity.startService(intent);
    }

    public static RestaurantsFragment newInstance() {
        return new RestaurantsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(TITLE);
        Log.d(TAG, "onAttach");
        if (context instanceof RestaurateurFragmentListener) {
            eventListener = (RestaurateurFragmentListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        restaurantsRef = FirebaseDatabase.getInstance().getReference(RESTAURATEURS);
        restaurateurs = new ArrayList<>();
        locationManager = (LocationManager) fragmentActivity.getSystemService(LOCATION_SERVICE);
        if(!Places.isInitialized()){
            Places.initialize(fragmentActivity, "AIzaSyDFr-yYMujoHMW8wccj7_cEpkdWbqU-7fA");
            Log.d(TAG,"api key initialized?" + Places.isInitialized());
        }
        placesClient = createClient(fragmentActivity);
        Log.d(TAG,"placesClient?" + placesClient.toString());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        return inflater.inflate(R.layout.restaurants_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        yourAddressEt = view.findViewById(R.id.your_address);
        View findMeIv = view.findViewById(R.id.findme);
        findMeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getAddressPosition();
            }
        });
        waitingConnectionAnim = view.findViewById(R.id.loader_waiting);
        recyclerView = view.findViewById(R.id.rv_restaurateurs);
        searchRestaurantsTv = view.findViewById(R.id.search_restaurants);
        filterRatings = view.findViewById(R.id.filter_ratings);
        filterPopularity = view.findViewById(R.id.filter_popularity);
        filterDistance = view.findViewById(R.id.filter_distance);
        filterRatings.setOnClickListener(chipListener);
        filterPopularity.setOnClickListener(chipListener);
        filterDistance.setOnClickListener(chipListener);
        token = AutocompleteSessionToken.newInstance();
        searchRestaurantsTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GetLatLngAsyncTask(new GetLatLngAsyncTask.AsyncEventListener() {
                    @Override
                    public void onTaskCompleted(LatLng latLng) {
                        Log.d(TAG,"onTaskCompleted()");
                        if(latLng!=null){
                            getRestaurants(latLng);
                        }
                        else{
                            Toast.makeText(fragmentActivity,"Indirizzo non trovato!",Toast.LENGTH_SHORT).show();
                        }
                    }
                }).execute(fragmentActivity,yourAddressEt.getText().toString());
            }
        });
        setupRecyclerView();
        if (restaurateurs.isEmpty()){
            getRestaurants(null);
        }
    }

    private void getRestaurants(final LatLng customerLatLng) {
        AnimationUtil.startWaitingAnimation(TAG, fragmentActivity, waitingConnectionAnim);
        restaurantFound = 0;
        restaurateurs.clear();
        Query query = restaurantsRef.orderByChild(RESTAURANT_NAME);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int nRestaurants = (int) dataSnapshot.getChildrenCount();
                Log.d(TAG, "Restaurants found: " + nRestaurants);
                if(nRestaurants>0){
                    Iterator itr = dataSnapshot.getChildren().iterator();
                    for(int i = 0; i < nRestaurants; i++){
                        DataSnapshot childSnapshot = (DataSnapshot) itr.next();
                        final Restaurateur restaurateur = childSnapshot.getValue(Restaurateur.class);
                        if(customerLatLng==null){
                            restaurateurs.add(restaurateur);
                            restaurantFound++;
                        }
                        else if(restaurateur!=null && restaurateur.getRestaurantAddress()!=null){
                            new GetLatLngAsyncTask(new GetLatLngAsyncTask.AsyncEventListener() {
                                @Override
                                public void onTaskCompleted(LatLng latLng) {
                                    if(latLng!=null){
                                        Float distance = getDistance(customerLatLng,latLng.latitude,latLng.longitude,"20");
                                        if(distance!=null){
                                            restaurateur.setDistance(distance);
                                            restaurateurs.add(restaurateur);
                                            restaurantFound++;
                                            applyFilter();
                                        }
                                    }
                                }
                            }).execute(fragmentActivity,restaurateur.getRestaurantAddress());
                        }
                    }
                    applyFilter();
                    Log.d(TAG,"restaurants found: " + restaurantFound);
                    AnimationUtil.cancelWaitingAnimation(TAG, fragmentActivity, waitingConnectionAnim);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                AnimationUtil.cancelWaitingAnimation(TAG, fragmentActivity, waitingConnectionAnim);
            }
        });
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle(TITLE);
    }


    private void setupRecyclerView() {
        restaurateurAdapter = new RestaurateurAdapter(restaurateurs, this, fragmentActivity);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(restaurateurAdapter);
    }

    @Override
    public void showRestaurantView(String id ,Restaurateur restaurateur) {
        eventListener.onRestaurantShow(id, restaurateur);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public interface RestaurateurFragmentListener {
        void onRestaurantShow(String id , Restaurateur restaurateur);
    }

    private void getAddressPosition(){
        if (fragmentActivity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                fragmentActivity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(fragmentActivity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}, LOCATION_PERMISSION);
            return;
        }
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG,"onLocationChanged()");
                startFetchAddressIntentService(location);
                locationManager.removeUpdates(this);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                //
            }

            @Override
            public void onProviderEnabled(String s) {
                //
            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);
            }
        };
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == LOCATION_PERMISSION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getAddressPosition();
            }
        }
    }

    class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultData == null) {
                return;
            }

            String addressOutput = resultData.getString(FetchAddressIntentService.Constants.RESULT_DATA_KEY);
            if (addressOutput != null) {
                yourAddressEt.setText(addressOutput);
            }


        }
    }
    /*
     * @param rangeDistance in km, 20,3 i.e.
     * @return null if distance is greater than rangeDistance
     * */
    private Float getDistance(LatLng refLocation, double latBiker, double lngBiker, String rangeDistance) {
        float[] results = new float[1]; //metres, while rangeDistance in km
        Location.distanceBetween(refLocation.latitude,refLocation.longitude,latBiker,lngBiker,results);
        Log.d(TAG,"getDistance() " + results[0] + "m >= " + rangeDistance + "km ?");
        return results[0] > Float.parseFloat(rangeDistance)*1000? null : results[0];
    }


    private void sortAscByDistance(){

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                return (int) (o1.getDistance() - o2.getDistance());
            }
        });

    }
    private void sortAscByRatings(){

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getAvgRatings() == 0){
                    return 1;
                }

                if(o2.getAvgRatings() == 0){
                    return -1;
                }

                return (int) (o2.getAvgRatings() - o1.getAvgRatings());
            }
        });
    }
    private void sortAscByPopularity(){

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getNoRatings() == 0){
                    return 1;
                }

                if(o2.getNoRatings() == 0){
                    return -1;
                }

                return o2.getNoRatings() - o1.getNoRatings();
            }
        });
    }

    private void sortAscByRatingsAndPopularity() {

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                    return (int) (o2.getAvgRatings() - o1.getAvgRatings());
                }
                if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                    return o2.getNoRatings() - o1.getNoRatings();
                }
                //enfasi sul maggior numero di valutazioni
                return o2.getNoRatings() - o1.getNoRatings();
            }
        });


    }
    private void sortAscByRatingsAndDistance() {

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                    return (int) (o2.getDistance() - o1.getDistance());
                }
                if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                    return (int) (o2.getAvgRatings()-o1.getAvgRatings());
                }
                //enfasi sulla distanza
                return (int) (o1.getDistance() - o2.getDistance());
            }
        });

    }
    private void sortAscByPopularityAndDistance() {

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                    return (int) (o1.getDistance() - o2.getDistance());
                }
                if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                    return o2.getNoRatings() - o1.getNoRatings();
                }

                //enfasi sulla distanza
                return (int) (o1.getDistance() - o2.getDistance());
            }
        });
    }
    private void sortAscByAll() {

        Collections.sort(restaurateurs, new Comparator<Restaurateur>() {
            @Override
            public int compare(Restaurateur o1, Restaurateur o2) {
                if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                    if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                        return (int) (o1.getAvgRatings()-o2.getAvgRatings());
                    }
                    return (int) (o1.getDistance() - o2.getDistance());
                }
                if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                    if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                        return (int) (o1.getDistance() - o2.getDistance());
                    }
                    return (int) (o1.getAvgRatings()-o2.getAvgRatings());
                }
                if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                    if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                        return o1.getNoRatings() - o2.getNoRatings();
                    }
                    return (int) (o1.getDistance() - o2.getDistance());
                }
                if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                    if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                        return (int) (o1.getDistance() - o2.getDistance());
                    }
                    return o1.getNoRatings() - o2.getNoRatings();
                }
                if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                    if(o1.getNoRatings().intValue() == o2.getNoRatings().intValue()){
                        return (int) (o1.getAvgRatings()-o2.getAvgRatings());
                    }
                    return o1.getNoRatings() - o2.getNoRatings();
                }
                if(o1.getDistance().floatValue() == o2.getDistance().floatValue() ){
                    if(o1.getAvgRatings().floatValue() == o2.getAvgRatings().floatValue() ){
                        return o1.getNoRatings() - o2.getNoRatings();
                    }
                    return (int) (o1.getAvgRatings()-o1.getAvgRatings());
                }
                //enfasi sulla distanza
                return (int) (o1.getDistance() - o2.getDistance());
            }
        });

    }


}
