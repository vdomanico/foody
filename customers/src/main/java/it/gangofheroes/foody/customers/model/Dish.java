package it.gangofheroes.foody.customers.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dish implements Parcelable {

    private String name;
    private String description;
    private int quantity;
    private double price;
    private String tempUri;
    private String fileName;

    public Dish() {
        fileName = "dish_" + new SimpleDateFormat("yyyyMMdd_HHmmssSSS", java.util.Locale.getDefault()).format(new Date());
    }

    public Dish(String name, String description, int quantity, double price) {
        this.name = name;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
        fileName = "dish_" + new SimpleDateFormat("yyyyMMdd_HHmmss", java.util.Locale.getDefault()).format(new Date());
    }

    private Dish(Parcel in) {
        name = in.readString();
        description = in.readString();
        quantity = in.readInt();
        price = in.readDouble();
        tempUri = in.readString();
        fileName = in.readString();
    }

    public static final Creator<Dish> CREATOR = new Creator<Dish>() {
        @Override
        public Dish createFromParcel(Parcel in) {
            return new Dish(in);
        }

        @Override
        public Dish[] newArray(int size) {
            return new Dish[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(description);
        dest.writeDouble(quantity);
        dest.writeInt(quantity);
        dest.writeString(tempUri);
        dest.writeString(fileName);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTempUri() { return tempUri; }

    public void setTempUri(String tempUri) { this.tempUri = tempUri; }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
