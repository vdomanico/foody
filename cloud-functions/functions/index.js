const functions = require('firebase-functions');
const admin = require('firebase-admin');

exports.sendRestaurateurNotificationF = functions.database.ref('/orders-f/{orderId}').onWrite((change, context) => {

  // Exit when the data is deleted
  if (!change.after.exists()) {
          return null;
       }

  const orderId = context.params.orderId;
  const restaurateurId = change.after.child('restaurateurId').val();
  const customerId = change.after.child('customerId').val();
  const delivererId = change.after.child('delivererId').val();
  const orderState = change.after.child('orderState').val();

  console.log('Order :', orderId, 'Restaurateur :', restaurateurId);

  var payload;
  var linkNotification;

  if(orderState==="NEW"){
    console.log('OrderState is NEW');
    linkNotification = `/restaurateurs-f/${restaurateurId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'You have a new order!',
        body: 'Have a look on your new order!'
      }
    };
  }
  else if(orderState==="ACCEPTED"){
    console.log('OrderState is ACCEPTED');
    linkNotification = `/restaurateurs-f/${restaurateurId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'Your order has been accepted!',
        body: 'Your order has been accepted!'
      }
    };
  }
  else if(orderState==="DECLINED"){
    console.log('OrderState is DECLINED');
    linkNotification = `/restaurateurs-f/${restaurateurId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'Your order has been declined!',
        body: 'Sorry your order has been declined, find a new deliverer!'
      }
    };
  }else if(orderState==="ASSIGNED"){
    console.log('OrderState is ASSIGNED');
    linkNotification = `/bikers-f/${delivererId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'You have a new order!',
        body: 'Have a look on your new order to deliver!'
      }
    };
  }
  else if(orderState==="DELIVERED"){
    console.log('OrderState is DELIVERED');
    linkNotification = `/restaurateurs-f/${restaurateurId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'You order have been delivered!',
        body: 'Congratulation You order have been delivered!'
      }
    };
  }
  else if(orderState==="DELIVERING"){
    console.log('OrderState is DELIVERING');
    linkNotification = `/customers-f/${customerId}/tokens`;
    console.log('linkNotification:', linkNotification);
    payload = {
      notification: {
        title: 'your order has been left now!',
        body: 'Congratulation you order has been left now!'
      }
    };
  }

  admin.database().ref(linkNotification).once('value').
  then((tokensSnapshot) => {
    if (tokensSnapshot.exists()) {
      console.log('tokens path found');
      let tokens = Object.keys(tokensSnapshot.val());
      admin.messaging().sendToDevice(tokens, payload);
    }else{
      console.log('tokens path not found');
    }

  });
   return null;
});

admin.initializeApp();


