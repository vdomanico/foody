package it.gangofheroes.foody.restaurantowners.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import it.gangofheroes.foody.restaurantowners.R
import it.gangofheroes.foody.restaurantowners.adapters.DishCommentsAdapter
import it.gangofheroes.foody.restaurantowners.model.DishReview
import it.gangofheroes.foody.restaurantowners.model.Rating
import java.util.*

class DishRatingsFragment : Fragment(){

    private lateinit var menu: Menu
    private lateinit var dishReview : DishReview
    private lateinit var ratings : MutableList<Rating>
    private lateinit var dishCommentsAdapter : DishCommentsAdapter
    private var sortByAsc = false;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate")
        setHasOptionsMenu(true)
        arguments?.let{
            dishReview = it.getParcelable(KEY_DR) ?: DishReview()

        }
        ratings = dishReview.ratings;
        ratings.sortedWith( compareBy(Rating::getReviewDate)  )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_filter,menu)
        this.menu = menu;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val showItem : Boolean
        val item1 = menu.findItem(R.id.item1);
        val item2 = menu.findItem(R.id.item2);
        val itemSortAsc = menu.findItem(R.id.sort_asc);
        val itemSortDesc = menu.findItem(R.id.sort_desc);

        if(item.itemId == R.id.item1 || item.itemId == R.id.item2){
            item.isChecked = !item.isChecked;
        }
        if(item1.isChecked && item2.isChecked){
            sortByReviewDateAndReview();
            showItem = true
        }
        else if(item1.isChecked){
            sortByReviewDate();
            showItem = true
        }
        else if(item2.isChecked){
            sortByReview();
            showItem = true
        }
        else{
            showItem = false
        }
        if(showItem){
            if(sortByAsc){
                itemSortAsc.isVisible = true
                itemSortDesc.isVisible = false
            }else{
                itemSortAsc.isVisible = false
                itemSortDesc.isVisible = true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.d(TAG, "onAttach")
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG, "onCreateView")
        return inflater.inflate(R.layout.dish_comments_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated")
        val recycleView : RecyclerView = view.findViewById(R.id.rv_dish_ratings);
        dishCommentsAdapter = DishCommentsAdapter(ratings);
        recycleView.layoutManager = LinearLayoutManager(context)
        recycleView.adapter = dishCommentsAdapter
    }

    companion object {
        const val TAG = "DishRatingsFragment"
        private const val KEY_DR = "KEY_DR"
        fun newInstance(dr : DishReview) = DishRatingsFragment().apply {
            arguments = Bundle().apply {
                putParcelable(KEY_DR,dr);
            }
        }
    }

    private fun sortByReview() {
        if(sortByAsc){
            ratings.sortWith(Comparator { o1, o2 ->  o1.review - o2.review })
            dishCommentsAdapter.notifyDataSetChanged();
        }else{
            ratings.sortWith(Comparator { o1, o2 ->  o2.review - o1.review })
            dishCommentsAdapter.notifyDataSetChanged();
        }
        sortByAsc = !sortByAsc
    }

    private fun sortByReviewDate() {
        if(sortByAsc){
            ratings.sortWith(Comparator { o1, o2 -> (o1.reviewDate - o2.reviewDate).toInt() })
            dishCommentsAdapter.notifyDataSetChanged();
        }else{
            ratings.sortWith(Comparator { o1, o2 -> (o2.reviewDate - o1.reviewDate).toInt() })
            dishCommentsAdapter.notifyDataSetChanged();
        }
        sortByAsc = !sortByAsc
    }

    private fun sortByReviewDateAndReview() {
        if(sortByAsc){
            ratings.sort();
            dishCommentsAdapter.notifyDataSetChanged();
        }else{
            Collections.sort(ratings, reverseOrder());
            dishCommentsAdapter.notifyDataSetChanged();
        }
        sortByAsc = !sortByAsc
    }


}