package it.gangofheroes.foody.restaurantowners.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.DishSelection;

public class OrderDetailAdapter extends BaseAdapter {

    private Context context;
    private List<DishSelection> dishes;

    public OrderDetailAdapter(Context context, List<DishSelection> dishes) {
        this.context = context;
        if(dishes!=null){
            this.dishes = dishes;
            return;
        }
        this.dishes = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return dishes.size();
    }

    @Override
    public Object getItem(int position) {
        return dishes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DishSelection dish = (DishSelection) getItem(position);

        if(convertView==null) {
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.list_item, parent, false);
        }

        TextView tv_dish = convertView.findViewById(R.id.itemlist_dish);
        TextView tv_qty = convertView.findViewById(R.id.itemlist_qty);

        if(dish!=null) {
            tv_dish.setText(dish.getDish());
            tv_qty.setText(String.valueOf(dish.getQuantity()));
        }

        return convertView;
    }
}
