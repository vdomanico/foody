package it.gangofheroes.foody.restaurantowners.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.DishRatingsFragment;
import it.gangofheroes.foody.restaurantowners.fragments.YourReviewsFragment;
import it.gangofheroes.foody.restaurantowners.model.DishReview;

public class YourReviewsActivity extends AppCompatActivity implements
        YourReviewsFragment.YourReviewsFragmentEventListener {

    private static final String TAG = YourReviewsActivity.class.getCanonicalName();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.your_reviews_activity);
        setupToolbar();
        startYourReviewsFragment();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.orders_toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void startYourReviewsFragment(){
        Log.d(TAG,"startYourReviewsFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentYourReviews);
        if (!(f instanceof YourReviewsFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentYourReviews,
                            YourReviewsFragment.newInstance())
                    .addToBackStack(YourReviewsFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void startDishRatingsFragment(DishReview dishReview){
        Log.d(TAG,"startDishRatingsFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentYourReviews);
        if (!(f instanceof DishRatingsFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentYourReviews,
                            DishRatingsFragment.Companion.newInstance(dishReview))
                    .addToBackStack(DishRatingsFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentYourReviews);
        if(f instanceof YourReviewsFragment){
            finish();
        }
        else{
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onDishReviewSelect(DishReview dishReview) {
        startDishRatingsFragment(dishReview);
    }
}
