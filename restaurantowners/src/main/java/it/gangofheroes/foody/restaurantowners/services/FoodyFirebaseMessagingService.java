package it.gangofheroes.foody.restaurantowners.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FoodyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = FoodyFirebaseMessagingService.class.getCanonicalName();


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d(TAG,remoteMessage.getMessageId());
    }

    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
    }

}
