package it.gangofheroes.foody.restaurantowners.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import it.gangofheroes.foody.restaurantowners.R
import it.gangofheroes.foody.restaurantowners.model.Rating
import it.gangofheroes.foody.restaurantowners.view.FiveStarsView
import java.text.SimpleDateFormat
import java.util.*

class DishCommentsAdapter(private var ratings: List<Rating>) : RecyclerView.Adapter<DishCommentsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater : LayoutInflater = LayoutInflater.from(parent.context)
        val view : View = layoutInflater.inflate(R.layout.comment_adapter,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return ratings.size;
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val rating : Rating = ratings.get(position)

        if(rating.reviewDate != null){
            val date = Date(rating.reviewDate)
            val format = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            holder.dateTv.setText(format.format(date))
        }
        if(rating.name != ""){
            holder.userNameTv.setText(rating.name)
        }
        if(rating.surname != ""){
            holder.userSurnameTv.setText(rating.surname)
        }

        holder.commentTv.setText(rating.commentReview)
        holder.ratings.stars = rating.review
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dateTv : TextView = itemView.findViewById(R.id.date)
        var userNameTv : TextView = itemView.findViewById(R.id.userName)
        var userSurnameTv : TextView = itemView.findViewById(R.id.userSurname)
        var commentTv : TextView = itemView.findViewById(R.id.comment)
        var ratings : FiveStarsView = itemView.findViewById(R.id.user_stars_ratings)
    }

}