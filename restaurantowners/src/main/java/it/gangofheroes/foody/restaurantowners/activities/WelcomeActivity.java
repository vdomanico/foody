package it.gangofheroes.foody.restaurantowners.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.OrderInfoFragment;
import it.gangofheroes.foody.restaurantowners.fragments.OrderListFragment;
import it.gangofheroes.foody.restaurantowners.model.Order;
import it.gangofheroes.foody.restaurantowners.model.Restaurateur;
import it.gangofheroes.foody.restaurantowners.model.UserType;
import it.gangofheroes.foody.restaurantowners.util.DownloadAvatarUtil;
import it.gangofheroes.foody.restaurantowners.util.StringsUtil;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEURS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEUR_ID;

public class WelcomeActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener,
        OrderListFragment.OrderListFragmentInteractionListener {

    private static final String TAG = WelcomeActivity.class.getCanonicalName();

    private Restaurateur restaurateur;
    private FirebaseUser firebaseUser;
    private DrawerLayout drawer;
    private DatabaseReference restaurantsRef;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);
        setupToolbar();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        restaurantsRef = FirebaseDatabase.getInstance().getReference(RESTAURATEURS);
        updateRegistrationToken();
        updateRestaurant(firebaseUser.getUid());
        loadRestaurateur(firebaseUser.getUid());
        showOrdersFragment();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void updateRestaurant(final String user){
        if(user!=null){
            restaurantsRef.child(user).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()){
                        dataSnapshot.getRef().setValue(user);
                        dataSnapshot.getRef().child(RESTAURATEUR_ID).setValue(user);
                        Log.d(TAG,"New Restaurant add to db ");
                    }else{
                        if(!dataSnapshot.child(RESTAURATEUR_ID).exists()){
                            dataSnapshot.child(RESTAURATEUR_ID).getRef().setValue(user);
                            Log.d(TAG,"RESTAURANT_ID missing and now added");
                        }
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.e(TAG,databaseError.getMessage());
                }
            });
        }

    }

    private void loadRestaurateur(String user){
        NavigationView navigationView = findViewById(R.id.nav_view);
        View hView =  navigationView.getHeaderView(0);
        ImageView nav_user = hView.findViewById(R.id.avatar);
        final TextView emailTv = hView.findViewById(R.id.customer_email);
        DownloadAvatarUtil.downloadUserAvatar(user,
                UserType.RESTAURATEUR,
                nav_user,
                getApplicationContext(),
                WelcomeActivity.TAG);

        restaurantsRef.
                child(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Restaurateur restaurateur = null;
                        if (dataSnapshot.exists()) {
                            restaurateur = dataSnapshot.getValue(Restaurateur.class);
                            if(restaurateur!=null){
                                if(StringsUtil.isAnyBlank(
                                        restaurateur.getRestaurantName() ,
                                        restaurateur.getRestaurantMail(),
                                        restaurateur.getRestaurantPhone(),
                                        restaurateur.getRestaurantAddress(),
                                        restaurateur.getRestaurantDescription())){
                                    Intent intent = new Intent(getApplicationContext(), YourProfileActivity.class);
                                    intent.putExtra("EDIT","EDIT");
                                    startActivity(intent);
                                }
                                if(!StringsUtil.isAnyBlank(restaurateur.getRestaurantMail())){
                                    emailTv.setText(restaurateur.getRestaurantMail());
                                }
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        int id = menuItem.getItemId();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        if(id == R.id.nav_your_profile){
            drawer.closeDrawer(GravityCompat.START);
            startActivity(new Intent(getApplicationContext(), YourProfileActivity.class));
        }else if(id == R.id.nav_edit_menu) {
            drawer.closeDrawer(GravityCompat.START);
            startActivity(new Intent(getApplicationContext(), EditMenuOffer.class));
        }
        else if(id == R.id.nav_your_reviews){
            drawer.closeDrawer(GravityCompat.START);
            startActivity(new Intent(getApplicationContext(), YourReviewsActivity.class));
        }
        else if(id == R.id.nav_sign_out){
            drawer.closeDrawer(GravityCompat.START);
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
            finish();
        }

        return true;

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
            if (f instanceof OrderListFragment){
                finish();
            }
            else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loader).setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loader).setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showOrder(Order order) {
        Log.d(TAG, "showOrder");
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer,
                        OrderInfoFragment.newInstance(order))
                .addToBackStack(OrderInfoFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


    private void updateRegistrationToken(){
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        if(task.getResult()!=null){
                            final String token = task.getResult().getToken();
                            Log.d(TAG,token);
                            FirebaseDatabase.
                                    getInstance()
                                    .getReference(RESTAURATEURS).
                                    child(firebaseUser.getUid() + "/tokens/" + token).setValue(true);
                        }

                    }
                });
    }


    private void showOrdersFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer,
                        OrderListFragment.newInstance())
                .addToBackStack(OrderListFragment.TAG)
                .commit();
    }
}
