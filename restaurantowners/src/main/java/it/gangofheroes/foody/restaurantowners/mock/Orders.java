package it.gangofheroes.foody.restaurantowners.mock;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.gangofheroes.foody.restaurantowners.model.DishSelection;
import it.gangofheroes.foody.restaurantowners.model.Order;
import it.gangofheroes.foody.restaurantowners.model.OrderState;

public class Orders {

    private static int mTime = 1;

    public static Order getMockOrder(OrderState orderState){
        Order temp = new Order();
        List<DishSelection> dishes = new ArrayList<>();
        dishes.add(new DishSelection("Pizza Margerita",2,10));
        dishes.add(new DishSelection("Bibita 0.33L",2,5));
/*        dishes.add(new DishSelection("Gelato 0.5k 0.33L",1,12.50));
        dishes.add(new DishSelection("Patatine",2,2.50));
        dishes.add(new DishSelection("Spicy spinaci ",2,4.50));*/
        temp.setCustomerName("C. Name" + mTime);
        temp.setCustomerSurname("C. Surname " + mTime);
        temp.setCustomerAddress("Via degli eroi n." + mTime);
        temp.setOrderState(orderState);
        temp.setDeliveryTime(new Date().getTime()+45*60*1000);
        temp.setDishes(dishes);
        temp.setCustomerPhone("+39 3887827127");
        temp.setNotes("Citofono guasto, chiamare al telefono per consegnare");
        mTime++;
        return temp;
    }

}
