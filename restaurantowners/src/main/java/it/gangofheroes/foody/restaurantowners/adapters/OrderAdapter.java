package it.gangofheroes.foody.restaurantowners.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Order;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.ViewHolder> {

    private static final String TAG = OrderAdapter.class.getCanonicalName();
    private final Context context;
    private OrderAdapterCallback orderAdapterListener;
    private List<Order> orders;

    public OrderAdapter(Context context, List<Order> orders){
        this.context = context;
        this.orders= orders;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");
        holder.order = orders.get(position);
        Log.d(TAG," order position: " + position);
        holder.deliveryAddress.setText(holder.order.getCustomerAddress() != null ? holder.order.getCustomerAddress() : "undefined");
        holder.totalPrice.setText(String.valueOf(holder.order.getAmount()));
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.getDefault());
        String dateString = holder.order.getDeliveryTime()!=null ? format.format(holder.order.getDeliveryTime()) : "undefined";
        holder.deliveryTime.setText(dateString);
        switch(holder.order.getOrderState()){
            case ASSIGNED:
                holder.orderNewIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setVisibility(View.GONE);
                holder.orderAssignedIv.setVisibility(View.VISIBLE);
                holder.orderAssignedIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderAdapterListener.onAssignOrder(holder.order);
                    }
                });
                break;
            case ACCEPTED:
                holder.orderNewIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setVisibility(View.GONE);
                holder.orderAssignedIv.setVisibility(View.GONE);
                holder.orderHandledIv.setVisibility(View.VISIBLE);
                holder.orderHandledIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleOrderToBiker(holder.order, holder.orderHandledIv);
                    }
                });
                break;
            case DECLINED:
                holder.orderNewIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setVisibility(View.VISIBLE);
                holder.orderAssignedIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderAdapterListener.onAssignOrder(holder.order);

                    }
                });
                break;
            case DELIVERED:
                holder.orderNewIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setVisibility(View.GONE);
                holder.orderAssignedIv.setVisibility(View.GONE);
                break;
            case DELIVERING:
                holder.orderNewIv.setVisibility(View.GONE);
                holder.orderDeclinedIv.setVisibility(View.GONE);
                holder.orderAssignedIv.setVisibility(View.GONE);
                break;
            case NEW:
                holder.orderDeclinedIv.setVisibility(View.GONE);
                holder.orderAssignedIv.setVisibility(View.GONE);
                holder.orderNewIv.setVisibility(View.VISIBLE);
                holder.orderNewIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderAdapterListener.onAssignOrder(holder.order);

                    }
                });
        }
        holder.openOrderDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "open order detail");
                orderAdapterListener.showOrder(holder.order);
            }
        });

    }

    private void handleOrderToBiker(final Order order, final ImageView view) {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(context, R.style.DialogTheme);
        alertDiaolg.setTitle("Stai consengnando l'ordine al biker");
        alertDiaolg.setMessage("Hai consegnato l'ordine al biker?");
        alertDiaolg.setPositiveButton("Y",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        view.setVisibility(View.GONE);
                        orderAdapterListener.onDeliveringOrder(order);
                    }
                });
        alertDiaolg.setNegativeButton("N",null);
        alertDiaolg.create().show();
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private Order order;
        private LinearLayout deliveryTimeContainer;
        private TextView deliveryAddress;
        private ImageView openOrderDetail;
        private ImageView orderNewIv;
        private ImageView orderDeclinedIv;
        private ImageView orderAssignedIv;
        private ImageView orderHandledIv;
        private TextView deliveryTime;
        private TextView totalPrice;

        private ViewHolder(View v) {
            super(v);
            Log.d(TAG, "ViewHolder");
            deliveryAddress = v.findViewById(R.id.order_deliveryAddress);
            openOrderDetail = v.findViewById(R.id.openOrderDetailRv);
            orderNewIv = v.findViewById(R.id.findDeliver);
            orderDeclinedIv = v.findViewById(R.id.orderDeclined);
            orderAssignedIv = v.findViewById(R.id.orderAssigned);
            orderHandledIv = v.findViewById(R.id.orderTaken);
            deliveryTimeContainer = v.findViewById(R.id.deliveryTimeContainer);
            deliveryTime = v.findViewById(R.id.order_deliverytime);
            totalPrice = v.findViewById(R.id.order_totalPrice);
        }
    }

    public void setListener (OrderAdapterCallback listener){
        orderAdapterListener = listener;
    }

    public void replaceWithListAndNotify(List<Order> list) {
        Log.d(TAG,"replaceWithListAndNotify");
        Collections.sort(list, new Comparator<Order>() {
            @Override
            public int compare(Order oA, Order oB)
            {
                return  oA.getDeliveryTime().compareTo(oB.getDeliveryTime());
            }

        });
        orders = list;
        notifyDataSetChanged();
    }

    public interface OrderAdapterCallback {
        void showOrder (Order order);
        void onAssignOrder(Order order);
        void showProgress();
        void hideProgress();
        void onDeliveringOrder(Order order);
    }
}
