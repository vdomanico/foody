package it.gangofheroes.foody.restaurantowners.model;

import java.util.List;

public class DailyOffer {

    private List<Dish> dishes;

    public List<Dish> getDishes() {
        return dishes;
    }

    public void setDishes(List<Dish> dishes) {
        this.dishes = dishes;
    }
}
