package it.gangofheroes.foody.restaurantowners.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class DishReview  implements Parcelable {

    private String category;
    private String name;
    private int review;
    private int sum;
    private int noReviews;
    private List<Rating> ratings;
    private boolean isCommentPresent;

    public DishReview() {
    }

    public DishReview(String category, String name, int review, int sum, int noReviews, List<Rating> ratings, boolean isCommentPresent) {
        this.category = category;
        this.name = name;
        this.review = review;
        this.sum = sum;
        this.noReviews = noReviews;
        this.ratings = ratings;
        this.isCommentPresent = isCommentPresent;
    }

    private DishReview(Parcel in) {
        category = in.readString();
        name = in.readString();
        review = in.readInt();
        sum = in.readInt();
        noReviews = in.readInt();
        isCommentPresent = in.readByte() != 0;
    }

    public static final Creator<DishReview> CREATOR = new Creator<DishReview>() {
        @Override
        public DishReview createFromParcel(Parcel in) {
            return new DishReview(in);
        }

        @Override
        public DishReview[] newArray(int size) {
            return new DishReview[size];
        }
    };

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    public int getNoReviews() {
        return noReviews;
    }

    public void setNoReviews(int noReviews) {
        this.noReviews = noReviews;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public boolean isCommentPresent() {
        return isCommentPresent;
    }

    public void setCommentPresent(boolean commentPresent) {
        isCommentPresent = commentPresent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(name);
        dest.writeInt(review);
        dest.writeInt(sum);
        dest.writeInt(noReviews);
        dest.writeByte((byte) (isCommentPresent ? 1 : 0));
    }
}
