package it.gangofheroes.foody.restaurantowners.model;

import java.util.ArrayList;

public class Biker {

    public enum BIKER_STATUS {

        AVAILABLE("available"),
        DELIVERING ("delivering"),
        BUSY ("busy"),
        OFFLINE("offline");

        private String userStatus;
        BIKER_STATUS(String userStatus) {
            this.userStatus = userStatus;
        }
        public String getUserStatus(){
            return userStatus;
        }
    }

    private String bikerMail;
    private String bikerName;
    private String bikerSurname;
    private String bikerPhone;
    private String bikerId;
    private Double lat;
    private Double lng;
    private BIKER_STATUS userStatus;
    private ArrayList<Order> orders;
    private int totDeliveries;
    private int todayDeliveries;
    private Float distance; // è una proprietà locale e non del db

    public Biker() { }

    public String getBikerMail() {
        return bikerMail;
    }

    public void setBikerMail(String bikerMail) {
        this.bikerMail = bikerMail;
    }

    public String getBikerName() {
        return bikerName;
    }

    public void setBikerName(String bikerName) {
        this.bikerName = bikerName;
    }

    public String getBikerSurname() {
        return bikerSurname;
    }

    public void setBikerSurname(String bikerSurname) {
        this.bikerSurname = bikerSurname;
    }

    public String getBikerPhone() {
        return bikerPhone;
    }

    public void setBikerPhone(String bikerPhone) {
        this.bikerPhone = bikerPhone;
    }

    public BIKER_STATUS getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(BIKER_STATUS userStatus) {
        this.userStatus = userStatus;
    }

    public ArrayList<Order> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<Order> orders) {
        this.orders = orders;
    }

    public int getTotDeliveries() {
        return totDeliveries;
    }

    public void setTotDeliveries(int totDeliveries) {
        this.totDeliveries = totDeliveries;
    }

    public int getTodayDeliveries() {
        return todayDeliveries;
    }

    public void setTodayDeliveries(int todayDeliveries) {
        this.todayDeliveries = todayDeliveries;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Float getDistance() {
        return distance;
    }

    public void setDistance(Float distance) {
        this.distance = distance;
    }

    public String getBikerId() {
        return bikerId;
    }

    public void setBikerId(String bikerId) {
        this.bikerId = bikerId;
    }
}
