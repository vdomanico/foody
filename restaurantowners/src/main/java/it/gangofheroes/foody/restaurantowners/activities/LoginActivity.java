package it.gangofheroes.foody.restaurantowners.activities;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.LoginFragment;


public class LoginActivity extends AppCompatActivity implements LoginFragment.LoginFragmentInteractionListener{

    private static final String TAG = LoginActivity.class.getCanonicalName();



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        setContentView(R.layout.login_activity);
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mUser == null){
            startFragment(LoginFragment.newInstance(), LoginFragment.TAG);
        }
        else {
            nextActivity();
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentContainer);
        if (fragment == null){
            finishAffinity();
        }
    }
    
    private void startFragment(Fragment fragment, String fragmentTag) {
        Log.d(TAG, "startFragment: " + fragmentTag);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }
    
    @Override
    public void onLoginCompleted(FirebaseUser user) {
        Log.d(TAG, "onLoginCompleted");
        nextActivity();
    }
    
    private void nextActivity() {
        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        finish();
    }

}