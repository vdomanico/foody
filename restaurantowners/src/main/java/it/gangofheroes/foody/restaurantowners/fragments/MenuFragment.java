package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.adapters.MenuAdapter;
import it.gangofheroes.foody.restaurantowners.model.Category;
import it.gangofheroes.foody.restaurantowners.model.Menu;

public class MenuFragment extends Fragment {

    public static Fragment newInstance(Menu menu) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("MENU", menu);
        MenuFragment menuFragment = new MenuFragment();
        menuFragment.setArguments(bundle);
        return menuFragment;
    }

    public interface MenuFragmentEventListener{
        void onMenuCategoryEdit(Category category, int position);
        void onCategoryRemove(Category cRemoved);
    }

    public static final String TAG = MenuFragment.class.getCanonicalName();
    private MenuAdapter menuAdapter;
    private MenuFragmentEventListener eventListener;
    private FragmentActivity fragmentActivity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventListener = (MenuFragmentEventListener) getActivity();
        fragmentActivity.setTitle(R.string.your_menus);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.menu_fragment, container, false);
        Menu menu = null;
        if(getArguments()!=null){
             menu = getArguments().getParcelable("MENU");
        }
        menuAdapter = new MenuAdapter(menu != null ? menu : new Menu());
        menuAdapter.setEventListener(new MenuAdapter.MenuAdapterEventListener() {
            @Override
            public void onMenuCategoryEdit(Category category, int categoryPosition) {
                eventListener.onMenuCategoryEdit(category, categoryPosition);
            }
            @Override
            public void onCategoryRemove(Category cRemoved, int position) {
                eventListener.onCategoryRemove(cRemoved);
            }
        });
        setupView(view);
        return view;
    }

    private void setupView(View view) {
        Button button = view.findViewById(R.id.add_category);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventListener.onMenuCategoryEdit(new Category(),menuAdapter.getItemCount());
            }
        });
        RecyclerView recyclerView = view.findViewById(R.id.menu_categories);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(menuAdapter);
    }
}
