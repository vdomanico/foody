package it.gangofheroes.foody.restaurantowners.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Category implements Parcelable {

    private String category;
    private List<Dish> options;

    public Category(){
        category = "";
        options = new ArrayList<>();
    }

    private Category(Parcel in) {
        category = in.readString();
        options = in.createTypedArrayList(Dish.CREATOR);
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeTypedList(options);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Dish> getOptions() {
        return options;
    }

    public void setOptions(List<Dish> options) {
        this.options = options;
    }
}
