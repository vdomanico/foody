package it.gangofheroes.foody.restaurantowners.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.DishReview;
import it.gangofheroes.foody.restaurantowners.view.FiveStarsView;

public class DishReviewAdapter extends RecyclerView.Adapter<DishReviewAdapter.ViewHolder>{

    private static final String TAG = DishReviewAdapter.class.getCanonicalName();
    private final Context context;
    private List<DishReview> dishReviews;
    private DishReviewAdapterEventListener eventListener;

    public void setEventListener(DishReviewAdapterEventListener eventListener) {
        this.eventListener = eventListener;
    }

    public DishReviewAdapter(Context context, List<DishReview> dishReviews) {
        this.context = context;
        this.dishReviews = dishReviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.review_dish_adapter, parent, false);
        return new DishReviewAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final DishReview dishReview = dishReviews.get(position);
        Log.d(TAG, "onBindViewHolder position " + position + ": " + dishReview.getCategory() +  " " + dishReview.getName() + " " + dishReview.getReview() );
        holder.dishName.setText(dishReview.getName());
        holder.dishCat.setText(dishReview.getCategory());
        holder.fiveStarsView.setStars(dishReview.getReview());
        String nRev = dishReview.getNoReviews()+" " + context.getResources().getString(R.string.your_reviews);
        holder.nReviews.setText(nRev);
        if(dishReview.isCommentPresent()){
            holder.seeComments.setVisibility(View.VISIBLE);
            holder.seeComments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventListener.onDishReviewSelect(dishReview);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dishReviews.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dishName;
        private TextView dishCat;
        private TextView nReviews;
        private TextView seeComments;
        private FiveStarsView fiveStarsView;


        private ViewHolder(@NonNull View itemView) {
            super(itemView);
            Log.d(TAG, "ViewHolder");
            dishName = itemView.findViewById(R.id.dishNameRev);
            dishCat = itemView.findViewById(R.id.dishCatRev);
            nReviews = itemView.findViewById(R.id.noReviews);
            seeComments = itemView.findViewById(R.id.comments);
            fiveStarsView = itemView.findViewById(R.id.starsLayout);
        }
    }

    public interface DishReviewAdapterEventListener{
        public void onDishReviewSelect(DishReview dishReview);
    }
}
