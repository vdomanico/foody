package it.gangofheroes.foody.restaurantowners.model;

public class Rating implements Comparable<Rating>{

    private int review;
    private String commentReview;
    private String name;
    private String surname;
    private Long reviewDate;

    public Rating() {
    }

    public Rating(int review){
        this.review = review;
    }

    public Rating(int review, String commentReview, String name, String surname, Long reviewDate){
        this.review = review;
        this.commentReview = commentReview != null ? commentReview : "";
        this.name = name != null ? name : "";
        this.surname = surname != null ? surname : "";
        this.reviewDate = reviewDate;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public String getCommentReview() {
        return commentReview;
    }

    public void setCommentReview(String commentReview) {
        this.commentReview = commentReview;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getReviewDate() {
        return reviewDate;
    }

    public void setReviewDate(Long rwviewDate) {
        this.reviewDate = rwviewDate;
    }

    @Override
    public int compareTo(Rating o) {
        if(this.reviewDate-o.reviewDate==0){
            return this.review-o.review;
        }
        return (int) (this.reviewDate-o.reviewDate);
    }
}
