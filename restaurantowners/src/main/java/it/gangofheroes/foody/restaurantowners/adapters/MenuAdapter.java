package it.gangofheroes.foody.restaurantowners.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Category;
import it.gangofheroes.foody.restaurantowners.model.Menu;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewOlder> {

    public interface MenuAdapterEventListener {
        void onMenuCategoryEdit(Category category, int position);
        void onCategoryRemove(Category cRemoved, int position);
    }

    private static final String TAG = OrderAdapter.class.getCanonicalName();
    private List<Category> categories;
    private MenuAdapterEventListener eventListener;

    public void setEventListener(MenuAdapterEventListener eventListener){
        this.eventListener = eventListener;
    }

    public MenuAdapter(Menu menu){
        this.categories = menu.getCategories();
    }

    @NonNull
    @Override
    public ViewOlder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.menu_adapter,parent,false);
        return new ViewOlder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewOlder holder, int position) {
        holder.setupViewHolder(categories.get(position), position);
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class ViewOlder extends RecyclerView.ViewHolder {

        private ConstraintLayout editCategoryMenuIv;
        private TextView categoryMenuTv;
        private ImageView removeCategoryIv;
        private Category category;


        ViewOlder(@NonNull View itemView) {
            super(itemView);
            editCategoryMenuIv = itemView.findViewById(R.id.menu_category_layout);
            categoryMenuTv = itemView.findViewById(R.id.menu_category_value);
            removeCategoryIv = itemView.findViewById(R.id.removeMenuCategory);
        }

        private void setupViewHolder(final Category c, final int position){
            this.category = c;
            categoryMenuTv.setText(category.getCategory());
            editCategoryMenuIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventListener.onMenuCategoryEdit(category, position);
                }
            });
            removeCategoryIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alog(v,position);
                }
            });
        }
    }

    private void alog(View view, final int position) {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(view.getContext(), R.style.DialogTheme);
        alertDiaolg.setTitle(R.string.removeDishDialogTitle);
        alertDiaolg.setMessage(R.string.removeDishDialogMsg);
        alertDiaolg.setPositiveButton(R.string.delete,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Category cRemoved = categories.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position,categories.size());
                        eventListener.onCategoryRemove(cRemoved,position);
                    }
                });
        alertDiaolg.setNegativeButton(R.string.cancel,null);
        alertDiaolg.create().show();
    }
}
