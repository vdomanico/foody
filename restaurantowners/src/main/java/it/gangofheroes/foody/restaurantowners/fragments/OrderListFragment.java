package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.activities.FindDelivererActivity;
import it.gangofheroes.foody.restaurantowners.adapters.OrderAdapter;
import it.gangofheroes.foody.restaurantowners.model.Order;
import it.gangofheroes.foody.restaurantowners.model.OrderState;

import static android.app.Activity.RESULT_OK;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKERS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDERS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDER_STATUS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEUR_ID;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.ACCEPTED;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.ASSIGNED;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.DECLINED;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.DELIVERED;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.DELIVERING;
import static it.gangofheroes.foody.restaurantowners.model.OrderState.NEW;

public class OrderListFragment extends Fragment implements OrderAdapter.OrderAdapterCallback {

    public static final String TAG = OrderListFragment.class.getCanonicalName();
    private static final int ASSIGN_ORDER = 111;

    private static List<OrderState> TAB_0 = Arrays.asList(NEW, DECLINED, ASSIGNED);
    private static List<OrderState> TAB_1 = Arrays.asList(DELIVERING, ACCEPTED);
    private static List<OrderState> TAB_2 = Arrays.asList(DELIVERED);

    private OrderListFragmentInteractionListener mListener;
    private OrderAdapter orderAdapter;
    private Map<String,Order> newOrders;
    private Map<String,Order> deliveringOrders;
    private Map<String,Order> deliveredOrders;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FirebaseUser firebaseUser;
    private DatabaseReference ordersRef;
    private DatabaseReference bikersRef;
    private FragmentActivity fragmentActivity;
    private TabLayout tabLayout;

    public static OrderListFragment newInstance(){
        return new OrderListFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        if (context instanceof OrderListFragmentInteractionListener){
            mListener = (OrderListFragmentInteractionListener) context;
        }
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle( R.string.orders);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        newOrders = new HashMap<>();
        deliveringOrders = new HashMap<>();
        deliveredOrders = new HashMap<>();
        ordersRef = FirebaseDatabase.
                getInstance().
                getReference(ORDERS);
        bikersRef = FirebaseDatabase.
                getInstance().
                getReference(BIKERS);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.order_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setup(view);
        retrieveOrders(TAB_0,newOrders);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        fragmentActivity.setTitle( R.string.orders);
    }

    @Override
    public void onResume() {
        super.onResume();
        switch(tabLayout.getSelectedTabPosition()){
            case 0:
                retrieveOrders(TAB_0,newOrders);
                break;
            case 1:
                retrieveOrders(TAB_1,deliveringOrders);
                break;
            case 2:
                retrieveOrders(TAB_2,deliveredOrders);
                break;
        }
    }

    private void setup(View view) {
        tabLayout = view.findViewById(R.id.tab_layout);
        RecyclerView recyclerView = view.findViewById(R.id.rv_orders);

        orderAdapter = new OrderAdapter(fragmentActivity,new ArrayList<>(newOrders.values()));
        orderAdapter.setListener(this);

        LinearLayoutManager llm = new LinearLayoutManager(fragmentActivity);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(orderAdapter);

        swipeRefreshLayout = view.findViewById(R.id.refreshLayout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                Log.d(TAG, "onRefresh");
                switch(tabLayout.getSelectedTabPosition()){
                    case 0:
                        retrieveOrders(TAB_0,newOrders);
                        break;
                    case 1:
                        retrieveOrders(TAB_1,deliveringOrders);
                        break;
                    case 2:
                        retrieveOrders(TAB_2,deliveredOrders);
                        break;
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.d(TAG, "onTabSelected: " + tab.getPosition());
                switch (tab.getPosition()){
                    case 0:
                        retrieveOrders(TAB_0,newOrders);
                        break;
                    case 1:
                        retrieveOrders(TAB_1,deliveringOrders);
                        break;
                    case 2:
                        retrieveOrders(TAB_2,deliveredOrders);
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode == ASSIGN_ORDER){
            if(resultCode==RESULT_OK){
                TabLayout.Tab tab = tabLayout.getTabAt(0);
                Toast.makeText(fragmentActivity,R.string.order_assign_ok,Toast.LENGTH_LONG).show();
                if(tab!=null){
                    tab.select();
                }
            }
        }
    }



    @Override
    public void showOrder(Order order) {
        Log.d(TAG, "showOrder");
        mListener.showOrder(order);
    }

    @Override
    public void onAssignOrder(Order order) {
        Log.d(TAG, "Ready to find Biker..");
        Intent intent = new Intent(fragmentActivity, FindDelivererActivity.class);
        intent.putExtra("ORDER_ID",order.getOrderId());
        startActivityForResult(intent,ASSIGN_ORDER);
    }

    @Override
    public void showProgress() {
        Log.d(TAG, "showProgress");
        mListener.showProgress();
    }

    @Override
    public void hideProgress() {
        Log.d(TAG, "hideProgress");
        mListener.hideProgress();
    }

    @Override
    public void onDeliveringOrder(Order order) {
        ordersRef.child(order.getOrderId()).child(ORDER_STATUS).setValue(DELIVERING);
    }

    public interface OrderListFragmentInteractionListener {
        void showOrder(Order order);
        void showProgress();
        void hideProgress();
    }



    /*
    *
    *
    *
    * */

    private void retrieveOrders(final List<OrderState> orderState, final Map<String, Order> orders) {

        swipeRefreshLayout.setRefreshing(true);

        Query query = ordersRef.orderByChild(RESTAURATEUR_ID);
        query.equalTo(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child: dataSnapshot.getChildren()) {
                    Order order = child.getValue(Order.class);
                    if(order!=null){
                        Log.d(TAG,"ordine " + order.getOrderId() + " " + order.getOrderState());
                    }
                    //se tra gli ordini attuali c'è un ordine che non gli appartiene più perchè ha cambiato stato
                    if(order != null && orders.containsKey(order.getOrderId()) && !orderState.contains(order.getOrderState())){
                        Log.d(TAG,"ordine " + order.getOrderId() + " " + order.getOrderState() + " removed");
                        orders.remove(order.getOrderId());
                    }//aggiorno gli ordini
                    else if(order != null && orderState.contains(order.getOrderState())){
                        Log.d(TAG,"ordine " + order.getOrderId() + " " + order.getOrderState() + " added");
                        orders.put(order.getOrderId(),order);
                    }
                }

                orderAdapter.replaceWithListAndNotify(new ArrayList<>(orders.values()));
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                orderAdapter.replaceWithListAndNotify(new ArrayList<>(orders.values()));
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
}
