package it.gangofheroes.foody.restaurantowners.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import it.gangofheroes.foody.restaurantowners.model.UserType;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.getRefAvatar;
import static it.gangofheroes.foody.restaurantowners.util.SharedPrefKeysJava.SHARED_PREF_KEY;
import static it.gangofheroes.foody.restaurantowners.util.StringsUtil.PROVIDER_NAME;

public class DownloadAvatarUtil {

    public static void downloadUserAvatar(final String userId,
                                          UserType userType,
                                          final ImageView imageView,
                                          final Context context,
                                          final String TAG) {
        final String refAvatar = getRefAvatar(userType);
        if(refAvatar==null){
            Log.e(TAG,"getRefAvatar is null");
            return;
        }
        final StorageReference storageReference = FirebaseStorage.
                getInstance().
                getReference().
                child(userId + "/" + refAvatar);

        Log.d(TAG, "DownloadAvatarUtil.downloadUserAvatar()");
        final File file = getFile(context,refAvatar + "-" + userId);
        final boolean successFromDevice = uploadFromDevice(file,imageView);
        Log.d(TAG, "DownloadAvatarUtil.uploadFromDevice() success:" + successFromDevice);
        storageReference.getMetadata().addOnSuccessListener(new OnSuccessListener<StorageMetadata>() {
            @Override
            public void onSuccess(StorageMetadata storageMetadata) {
                long timeUpdate = storageMetadata.getUpdatedTimeMillis();
                Log.d(TAG, "time of update" + timeUpdate);
                if(timeUpdate>retrieveLastModifiedOnThisDevice(context.getSharedPreferences(SHARED_PREF_KEY,Context.MODE_PRIVATE),refAvatar + "-" + userId) || !successFromDevice){
                    downloadImage(storageReference,file,imageView,timeUpdate,userId, refAvatar, context, TAG);
                }
            }
        });

    }

    public static void uploadUserAvatar(@NotNull Uri uri, final Uri avatarUri, final String userId, UserType userType, final Context context, final String TAG) {

        final String refAvatar = getRefAvatar(userType);
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().
                child(userId + "/"+ refAvatar);

        storageReference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG,"rest avatar uploaded");
                if(taskSnapshot.getMetadata()!=null){
                    saveLastModifiedOnThisDevice(context.getSharedPreferences(SHARED_PREF_KEY,Context.MODE_PRIVATE),refAvatar,taskSnapshot.getMetadata().getCreationTimeMillis());
                }
                copyImage(avatarUri,userId,refAvatar,context,TAG);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Log.d(TAG, "upload failed");
            }
        });
    }

    private static  void downloadImage(StorageReference storageReference,
                                       final File file,
                                       final ImageView imageView,
                                       final Long timeUpdate,
                                       final String userId,
                                       final String refAvatar,
                                       final Context context,
                                       final String TAG){
        Log.d(TAG,"DownloadAvatarUtil.downloadImage()");
        storageReference.getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                Log.d(TAG,"rest avatar downloaded");
                Uri uri = FileProvider.getUriForFile(context, PROVIDER_NAME, file);
                imageView.setImageURI(uri);
                saveLastModifiedOnThisDevice(context.getSharedPreferences(SHARED_PREF_KEY,Context.MODE_PRIVATE),refAvatar + "-" + userId,timeUpdate);
            }
        });
    }

    private static  File getFile(Context context, String filename){
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return new File(storageDir,filename);
    }

    private static  boolean uploadFromDevice(File file, ImageView imageView){
        if(file.exists() && file.length()>0) {
            Uri uri = Uri.fromFile(file);
            imageView.setImageURI(uri);
            return true;
        }
        return false;
    }


    private static  void saveLastModifiedOnThisDevice(SharedPreferences mPreferences, String key, Long time){
        SharedPreferences.Editor preferencesEditor = mPreferences.edit();
        preferencesEditor.putLong(key, time);
        preferencesEditor.apply();
    }

    private static  Long retrieveLastModifiedOnThisDevice(SharedPreferences mPreferences, String key){
        return mPreferences.getLong(key,0);
    }

    private static void copyImage(Uri uri, String userId, String refAvatar, Context context, String TAG){
        try {
            File file = getFile(context, refAvatar + "-" + userId);
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            if(inputStream==null) {
                return;
            }
            FileOutputStream outputStream = new FileOutputStream(file);
            copyStream(inputStream, outputStream);
            outputStream.close();
            inputStream.close();
        }
        catch (Exception e){
            Log.e(TAG,"Unable to copy image from gallery to private directory ");
        }
    }

    private static void copyStream(InputStream input, OutputStream output) throws Exception {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

}
