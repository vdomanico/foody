package it.gangofheroes.foody.restaurantowners.view;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.util.MinMaxFilterInput;

import static it.gangofheroes.foody.restaurantowners.fragments.EditYourProfileFragment.TAG;

public class RangeHourPicker extends LinearLayout {

    public RangeHourPicker(Context context) {
        super(context);
    }
    private EditText fromHourEt;
    private EditText toHourEt;
    private EditText fromMinuteEt;
    private EditText toMinuteEt;

    public RangeHourPicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.hour_picker, this);
        fromHourEt = findViewById(R.id.from_hour);
        fromMinuteEt = findViewById(R.id.from_minute);
        toHourEt = findViewById(R.id.to_hour);
        toMinuteEt = findViewById(R.id.to_minute);

        InputFilter hours = new MinMaxFilterInput(0,23);
        InputFilter minutes = new MinMaxFilterInput(0,59);

        fromHourEt.setFilters(new InputFilter[]{ hours });
        toHourEt.setFilters(new InputFilter[]{ hours });
        fromMinuteEt.setFilters(new InputFilter[]{ minutes });
        toMinuteEt.setFilters(new InputFilter[]{ minutes });

    }

    /*
     * Il formato è 11.30-2.30
     * */
    public String getRange(){

        try{
            int fromH = Integer.valueOf(fromHourEt.getText().toString());
            int toH = Integer.valueOf(toHourEt.getText().toString());
            int fromM = Integer.valueOf(fromMinuteEt.getText().toString());
            int toM = Integer.valueOf(toMinuteEt.getText().toString());

            if(fromH<toH || (fromH==toH && fromM<toM)){
                return fromH + "." + fromM + "-" + toH + "." + toM;
            }

        }catch (Exception e){
            Log.e(TAG,e.getMessage());
            return "";
        }
        return "";
    }

    /*
    * Il formato è 11.30-2.30
    * */
    public void setRange(String lunchAndDinner) {
        try{
            String [] tokens = lunchAndDinner.split("-");
            String [] from = tokens[0].split("\\.");
            String [] to = tokens[1].split("\\.");
            fromHourEt.setText(from[0]);
            toHourEt.setText(to[0]);
            toMinuteEt.setText(from[1]);
            toMinuteEt.setText(to[1]);
        }catch(Exception e){
            Log.e(TAG,e.getMessage());
        }

    }
}
