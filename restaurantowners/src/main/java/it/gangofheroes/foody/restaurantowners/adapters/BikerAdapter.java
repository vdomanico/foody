package it.gangofheroes.foody.restaurantowners.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Biker;

public class BikerAdapter extends RecyclerView.Adapter<BikerAdapter.ViewHolder> {

    private static final String TAG = BikerAdapter.class.getCanonicalName();
    private List<Biker> bikers;
    private BikerAdapter.BikerAdapterEventListener eventListener;

    public interface BikerAdapterEventListener{
        public void onBikerSelect(String delivererId);
    }

    public BikerAdapter(List<Biker> bikers, BikerAdapterEventListener eventListener){
        this.bikers = bikers;
        this.eventListener = eventListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.biker_adapter,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder");
        final Biker biker = bikers.get(position);

        if(biker!=null){
            String name = biker.getBikerName();
            String surname = biker.getBikerSurname();
            String phone = biker.getBikerPhone();
            Float distance = biker.getDistance();
            name = name!=null? name : "";
            surname = surname!=null? surname : "";
            String fullName = name + " " + surname;
            holder.bikerFullNameTv.setText(fullName);
            if(phone!=null){
                holder.bikerPhoneTv.setText(phone);
            }
            if(distance!=null){
                String stringDistance = distance.intValue() + " m";
                holder.bikerDistanceTv.setText(stringDistance);
            }
            holder.assignBikerIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    eventListener.onBikerSelect(biker.getBikerId());
                }
            });
        }
    }
    @Override
    public int getItemCount() {
        return bikers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView bikerFullNameTv;
        private TextView bikerPhoneTv;
        private TextView bikerDistanceTv;
        private ImageView assignBikerIv;

        private ViewHolder(View v) {
            super(v);
            Log.d(TAG, "ViewHolder");
            bikerFullNameTv = v.findViewById(R.id.biker_fullname);
            bikerPhoneTv = v.findViewById(R.id.biker_phone);
            bikerDistanceTv = v.findViewById(R.id.biker_distance);
            assignBikerIv = v.findViewById(R.id.assign_biker);
        }
    }


}
