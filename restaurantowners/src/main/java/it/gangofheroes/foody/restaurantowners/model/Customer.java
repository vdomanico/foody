package it.gangofheroes.foody.restaurantowners.model;

import android.net.Uri;

public class Customer {

    private String name;
    private String email;
    private String phone;
    private Uri profilePic;

    public Customer(String name,String email,String phone,Uri profilePic){
        this.name=name;
        this.email=email;
        this.phone=phone;
        this.profilePic=profilePic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Uri getUriImage() {
        return profilePic;
    }

    public void setImage(Uri image) {
        this.profilePic = image;
    }
}
