package it.gangofheroes.foody.restaurantowners.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import it.gangofheroes.foody.restaurantowners.R;


public class LoginFragment extends Fragment {

    public static final String TAG = LoginFragment.class.getCanonicalName();

    private static final int RC_SIGN_IN = 100;

    private LoginFragmentInteractionListener mListener;
    private List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build(),
            new AuthUI.IdpConfig.PhoneBuilder().build(),
            new AuthUI.IdpConfig.GoogleBuilder().build());
            //new AuthUI.IdpConfig.FacebookBuilder().build(),
            //new AuthUI.IdpConfig.TwitterBuilder().build());

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        view.findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "click on Login btn");
                startActivityForResult(
                        AuthUI.getInstance()
                                .createSignInIntentBuilder()
                                .setAvailableProviders(providers)
                                .build(),
                        RC_SIGN_IN);
            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");

        if (context instanceof LoginFragmentInteractionListener){
            mListener = (LoginFragmentInteractionListener) context;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        Log.d(TAG, "onActivityResult");
        switch (requestCode){
            case RC_SIGN_IN:
                if (resultCode == Activity.RESULT_OK) {
                    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                    mListener.onLoginCompleted(user);
                } else {
                    /* TODO */
                }
                break;
        }
    }

    public interface LoginFragmentInteractionListener {
        void onLoginCompleted(FirebaseUser user);
    }
}
