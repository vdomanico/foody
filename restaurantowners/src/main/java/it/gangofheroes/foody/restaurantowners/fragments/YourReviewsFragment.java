package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.adapters.DishReviewAdapter;
import it.gangofheroes.foody.restaurantowners.model.DishReview;
import it.gangofheroes.foody.restaurantowners.model.Firebase;
import it.gangofheroes.foody.restaurantowners.model.Rating;
import it.gangofheroes.foody.restaurantowners.view.FiveStarsView;

public class YourReviewsFragment extends Fragment {
    public static final String TAG = YourReviewsFragment.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private FirebaseUser firebaseUser;
    private DatabaseReference reviewsRef;
    private FiveStarsView fiveStarsView;
    private TextView nReviewsTv;
    private List<DishReview> dishReviews;
    private boolean orderDec = true;
    private YourReviewsFragmentEventListener eventListener;
    private DishReviewAdapter dishReviewAdapter;
    private Menu menu;

    public static Fragment newInstance() {
        return new YourReviewsFragment();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        eventListener = (YourReviewsFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate");
        setHasOptionsMenu(true);
        fragmentActivity.setTitle(R.string.reviews_title);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        reviewsRef = FirebaseDatabase.
                getInstance().
                getReference(Firebase.REVIEWS).child(firebaseUser.getUid());
        dishReviews = new ArrayList<>();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        inflater.inflate(R.menu.menu_reviews, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        Log.d(TAG,"Sort Item Clicked sorting dec :" + orderDec);
        MenuItem item1 = menu.findItem(R.id.sort_up);
        MenuItem item2 = menu.findItem(R.id.sort_down);

        if(item.getItemId() == R.id.sort_up || item.getItemId() == R.id.sort_down){
            if(orderDec){
                item1.setVisible(true);
                item2.setVisible(false);
            }
            else{
                item1.setVisible(false);
                item2.setVisible(true);
            }
            sortByAsc(orderDec);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView");
        return inflater.inflate(R.layout.your_reviews_fragment, container, false);
    }

    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        fiveStarsView = view.findViewById(R.id.five_stars_global);
        nReviewsTv = view.findViewById(R.id.n_reviews);
        fiveStarsView.setStarsClickable(false);
        RecyclerView recyclerView = view.findViewById(R.id.rev_rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(fragmentActivity));
        dishReviewAdapter = new DishReviewAdapter(fragmentActivity,dishReviews);
        dishReviewAdapter.setEventListener(new DishReviewAdapter.DishReviewAdapterEventListener() {
            @Override
            public void onDishReviewSelect(DishReview dishReview) {
                eventListener.onDishReviewSelect(dishReview);
            }
        });
        recyclerView.setAdapter(dishReviewAdapter);

        getGlobalRatings();
        //se è empty sono appena passato dall'onCreate, se passo solo dall'onViewCreated non riaggiorno
        if(dishReviews.isEmpty()){
            getAllRatings();
        }
    }

    private void sortByAsc(boolean asc){
        if(asc && orderDec && dishReviews!=null && !dishReviews.isEmpty()){
            Collections.sort(dishReviews, new Comparator<DishReview>() {
                @Override
                public int compare(DishReview o1, DishReview o2) {
                    return o2.getReview()-o1.getReview();
                }
            });
            if(dishReviewAdapter!=null){
                dishReviewAdapter.notifyDataSetChanged();
            }
        }
        else if(!asc && !orderDec && dishReviews!=null && !dishReviews.isEmpty()){
            Collections.sort(dishReviews, new Comparator<DishReview>() {
                @Override
                public int compare(DishReview o1, DishReview o2) {
                    return o1.getReview()-o2.getReview();
                }
            });
        }
        if(dishReviewAdapter!=null){
            dishReviewAdapter.notifyDataSetChanged();
        }
        orderDec = !orderDec;
    }

    public interface YourReviewsFragmentEventListener{
        public void onDishReviewSelect(DishReview dishReview);
    }

    private void getGlobalRatings(){
        reviewsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists()){
                    int sumReview = 0;
                    int nReviews = 0;
                    int gReview = 0;
                    for(DataSnapshot ds : dataSnapshot.getChildren()){
                        Integer rev = ds.child(Firebase.GLOBAL_REVIEW).getValue(Integer.class);
                        if(rev!=null && rev!=0){
                            Log.d(TAG,"user Rev :" + rev);
                            sumReview+=rev;
                            nReviews++;
                        }
                    }
                    if(nReviews!=0){
                        gReview = sumReview/nReviews;
                    }
                    fiveStarsView.setStars(gReview);
                    String reviews = nReviews + " " + getResources().getString(R.string.your_reviews);
                    nReviewsTv.setText(reviews);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
    }

    private void getAllRatings(){

        reviewsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Map<Categoria,Map<Piatto,List<Valutazione>>
                Map<String, Map<String,List<Rating>>> map = new HashMap<>();
                if(dataSnapshot.exists()){
                    for(DataSnapshot oSnapShot : dataSnapshot.getChildren()){
                        //per ogni ordine
                        String cName = null;
                        String cSurname = null;
                        Long reviewDate = null;
                        if(oSnapShot.hasChild(Firebase.CUSTOMER_NAME)){
                            cName = oSnapShot.child(Firebase.CUSTOMER_NAME).getValue(String.class);
                        }
                        if(oSnapShot.hasChild(Firebase.CUSTOMER_SURNAME)){
                            cSurname = oSnapShot.child(Firebase.CUSTOMER_SURNAME).getValue(String.class);
                        }
                        if(oSnapShot.hasChild(Firebase.ORDER_REVIEW_DATE)){
                            reviewDate = oSnapShot.child(Firebase.ORDER_REVIEW_DATE).getValue(Long.class);
                        }

                        if(oSnapShot.hasChild(Firebase.DISHES)){
                            for(DataSnapshot mSnapShot : oSnapShot.child(Firebase.DISHES).getChildren()){
                                if(mSnapShot.hasChildren()) {
                                    String category = mSnapShot.getKey();
                                    if (category != null) {
                                        if (!map.containsKey(category)) {
                                            map.put(category, new HashMap<String, List<Rating>>());
                                        }
                                        for (DataSnapshot dish : mSnapShot.getChildren()) {
                                            String dishName = dish.getKey();
                                            Integer review = dish.child(Firebase.REVIEW).getValue(Integer.class);
                                            String commentReview = dish.child(Firebase.COMMENT_REVIEW).getValue(String.class);
                                            if (dishName != null && review != null) {
                                                Map<String, List<Rating>> dishMap = map.get(category);
                                                if (dishMap != null && !dishMap.containsKey(dishName)) {
                                                    dishMap.put(dishName, new ArrayList<Rating>());
                                                }
                                                dishMap.get(dishName).add(new Rating(review,commentReview,cName,cSurname,reviewDate));
                                            }
                                        }
                                    }
                                }
                            }

                        }
                    }
                    if(!map.isEmpty()){
                        Set<String> catEntries = map.keySet();
                        for(String s : catEntries){
                            Map<String, List<Rating>> dMap = map.get(s);
                            if(dMap!=null){
                                boolean anyComment = false;
                                Set<String> dishEntries = dMap.keySet();
                                for (String de : dishEntries){
                                    List<Rating> ratings = dMap.get(de);
                                    int tot = 0;
                                    if(ratings!=null && !ratings.isEmpty()){
                                        for(Rating r : ratings){
                                            if(!r.getCommentReview().equals("")){
                                                anyComment = true;
                                            }
                                            tot += r.getReview();
                                        }
                                        dishReviews.add(new DishReview(s,de,tot/ratings.size(),tot,ratings.size(),ratings, anyComment));
                                    }

                                }
                            }
                        }
                    }
                    if(!dishReviews.isEmpty()){
                        sortByAsc(true);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
            }
        });
    }

}
