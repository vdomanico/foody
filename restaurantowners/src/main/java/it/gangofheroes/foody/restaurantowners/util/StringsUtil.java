package it.gangofheroes.foody.restaurantowners.util;

import static java.lang.reflect.Array.getLength;

public class StringsUtil {

    public static final String PACKAGE_NAME = "it.gangofheroes.foody.restaurantowners";
    public static final String PROVIDER_NAME = "it.gangofheroes.foody.restaurantowners.android.fileprovider";

    public static boolean isAnyBlank(final CharSequence... css) {
        if (isEmpty(css)) {
            return false;
        }
        for (final CharSequence cs : css) {
            if (isBlank(cs)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isAllBlank(final CharSequence... css) {
        if (isEmpty(css)) {
            return true;
        }
        for (final CharSequence cs : css) {
            if (!isBlank(cs)) {
                return false;
            }
        }
        return true;
    }

    private static boolean isBlank(final CharSequence cs) {
        int strLen;
        if (cs == null || (strLen = cs.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static boolean isEmpty(final Object[] array) {
        return getLength(array) == 0;
    }

}
