package it.gangofheroes.foody.restaurantowners.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DishSelection implements Parcelable {

    private String category;
    private String dish;
    private int quantity;
    private double amount;

    public DishSelection() { }

    public DishSelection(String dish, int quantity, double amount){
        this.dish = dish;
        this.quantity=quantity;
        this.amount = amount;
    }

    private DishSelection(Parcel in) {
        category = in.readString();
        dish = in.readString();
        quantity = in.readInt();
        amount = in.readDouble();
    }

    public static final Creator<DishSelection> CREATOR = new Creator<DishSelection>() {
        @Override
        public DishSelection createFromParcel(Parcel in) {
            return new DishSelection(in);
        }

        @Override
        public DishSelection[] newArray(int size) {
            return new DishSelection[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category);
        dest.writeString(dish);
        dest.writeInt(quantity);
        dest.writeDouble(amount);
    }

    public String getDish() {
        return dish;
    }

    public void setDish(String dish) {
        this.dish = dish;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
