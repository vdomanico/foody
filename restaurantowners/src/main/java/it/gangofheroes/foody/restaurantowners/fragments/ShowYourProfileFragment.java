package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Restaurateur;
import it.gangofheroes.foody.restaurantowners.model.UserType;
import it.gangofheroes.foody.restaurantowners.util.DownloadAvatarUtil;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEURS;


public class ShowYourProfileFragment extends Fragment{

    public static final String TAG = ShowYourProfileFragment.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private ShowYourProfileFragmentEventListener eventListener;
    private FirebaseUser firebaseUser;
    private DatabaseReference restaurantsRef;
    private TextView tvName;
    private TextView tvMail;
    private TextView tvShortDescription;
    private TextView tvAddress;
    private TextView tvPhone;
    private TextView tvHours;
    private TextView tvAdditionalInfo; // TODO integrare
    private ImageView profileIv;

    public interface ShowYourProfileFragmentEventListener{
        void onProfileEdit();
    }

    public static Fragment newInstance() {
        return new ShowYourProfileFragment();
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        fragmentActivity.setTitle(R.string.your_profile);
        eventListener = (ShowYourProfileFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        restaurantsRef = FirebaseDatabase.getInstance().getReference(RESTAURATEURS);

    }

    @Override
    public void onCreateOptionsMenu(@NotNull Menu menu, @NotNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Log.d(TAG,"onCreateOptionsMenu");
        inflater.inflate(R.menu.menu_showprofile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        Log.d(TAG,"onOptionsItemSelected");
        if (item.getItemId() == R.id.edit_profile_show) {
            eventListener.onProfileEdit();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_showrestprofile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG, "onViewCreated");
        tvName = view.findViewById(R.id.tvName);
        tvMail = view.findViewById(R.id.tvMail);
        tvPhone = view.findViewById(R.id.tvPhone);
        tvHours = view.findViewById(R.id.tvHours);
        tvAddress = view.findViewById(R.id.tvAddress);
        tvShortDescription = view.findViewById(R.id.tvDesc);
        profileIv = view.findViewById(R.id.profileImage);
        getPreferences();
    }

    private void getPreferences() {
        Log.d(TAG,"getPreferences()");
        DownloadAvatarUtil.downloadUserAvatar(firebaseUser.getUid(),
                UserType.RESTAURATEUR,
                profileIv,
                fragmentActivity, ShowYourProfileFragment.TAG);

        restaurantsRef.
                child(firebaseUser.getUid()).
                addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        Restaurateur restaurateur = null;
                        if (dataSnapshot.exists()) {
                            restaurateur = dataSnapshot.getValue(Restaurateur.class);
                            if(restaurateur!=null){
                                tvName.setText(restaurateur.getRestaurantName());
                                tvMail.setText(restaurateur.getRestaurantMail());
                                tvPhone.setText(restaurateur.getRestaurantPhone());
                                tvAddress.setText(restaurateur.getRestaurantAddress());
                                tvShortDescription.setText(restaurateur.getRestaurantDescription());
                                tvHours.setText(restaurateur.getOpeningHours());
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
    }


}
