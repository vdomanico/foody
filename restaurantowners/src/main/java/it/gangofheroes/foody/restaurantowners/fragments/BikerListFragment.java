package it.gangofheroes.foody.restaurantowners.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.adapters.BikerAdapter;
import it.gangofheroes.foody.restaurantowners.model.Biker;
import it.gangofheroes.foody.restaurantowners.model.Order;
import it.gangofheroes.foody.restaurantowners.model.OrderState;
import it.gangofheroes.foody.restaurantowners.model.Restaurateur;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKERS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKER_STATUS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKER_STATUS_AVAILABLE;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDERS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDER_DELIVERER_ID;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDER_DELIVERY_TIME;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDER_RESTAURANT_ADDRESS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.ORDER_STATUS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEURS;

public class BikerListFragment extends Fragment implements BikerAdapter.BikerAdapterEventListener {

    public static final String TAG = BikerListFragment.class.getCanonicalName();
    private BikerListFragmentEventListener eventListener;
    private FragmentActivity fragmentActivity;
    private DatabaseReference bikersRef;
    private DatabaseReference restaurantsRef;
    private FirebaseUser firebaseUser;
    private Restaurateur restaurateur;
    private List<Biker> bikers = new ArrayList<>();
    private LatLng latLngRestaurant;
    private TextView distanceFilterTv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView noBikersTv;
    private DatabaseReference ordersRef;
    private Order order;

    @Override
    public void onBikerSelect(String delivererId) {
        if(order!=null){
            confirmAssignOrder(delivererId);
        }
    }

    public interface BikerListFragmentEventListener{
        void onAssignOrderCompleted();
    }

    public static Fragment newInstance() {
        return new BikerListFragment();
    }
    public static Fragment newInstance(String orderId) {
        BikerListFragment f = new BikerListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ORDER_ID",orderId);
        f.setArguments(bundle);
        return f;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        eventListener = (BikerListFragmentEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        fragmentActivity.setTitle(R.string.assign_order_title);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        bikersRef = FirebaseDatabase.
                getInstance().
                getReference(BIKERS);
        restaurantsRef = FirebaseDatabase.
                getInstance().
                getReference(RESTAURATEURS);
        ordersRef = FirebaseDatabase.
                getInstance().
                getReference(ORDERS);
        if(getArguments()!=null){
            String orderId = getArguments().getString("ORDER_ID");
            if(orderId!=null){
                ordersRef.child(orderId).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if(dataSnapshot.exists()){
                            order = dataSnapshot.getValue(Order.class);
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Log.e(TAG,databaseError.getMessage());
                    }
                });
            }
            
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.find_delivery_list_fragment, container, false);
    }


    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView bikerRv = view.findViewById(R.id.biker_rv);
        BikerAdapter bikerAdapter = new BikerAdapter(bikers,this);
        LinearLayoutManager llm = new LinearLayoutManager(fragmentActivity);
        bikerRv.setLayoutManager(llm);
        noBikersTv = view.findViewById(R.id.noBikerTv);
        swipeRefreshLayout = view.findViewById(R.id.refreshLayoutBikers);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getBikers(distanceFilterTv.getText().toString());
            }
        });
        SeekBar seekBarTv = view.findViewById(R.id.seekBarBiker);
        distanceFilterTv = view.findViewById(R.id.distance_value);
        seekBarTv.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                String defaultValue = "0.2 km";
                Log.d(TAG,"" +progress);
                if(progress == 0){
                    distanceFilterTv.setText(defaultValue);
                }
                else{
                    double value = progress*0.20;
                    String stringValue = String.format(Locale.UK,"%.2f km", value);
                    distanceFilterTv.setText(stringValue);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                getBikers(distanceFilterTv.getText().toString());

            }
        });
        bikerRv.setAdapter(bikerAdapter);
        restaurantsRef.child(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                restaurateur = dataSnapshot.getValue(Restaurateur.class);
                latLngRestaurant = getRestaurantLatLng();
                getBikers("0.2 km");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
    }

    private void getBikers(final String rangeDistance) {
        Log.d(TAG,"getBikers found()");
        swipeRefreshLayout.setRefreshing(true);
        bikers.clear();
        final String [] tokens = rangeDistance.split(" ");
        if(tokens.length<1){
            swipeRefreshLayout.setRefreshing(false);
            return;
        }
        Query query = bikersRef.orderByChild(BIKER_STATUS).equalTo(BIKER_STATUS_AVAILABLE);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int nBikers = (int) dataSnapshot.getChildrenCount();
                if(nBikers>0 && latLngRestaurant!=null){
                    Iterator itr = dataSnapshot.getChildren().iterator();
                    bikers.clear();
                    for(int i = 0; i < nBikers; i++) {
                        DataSnapshot childSnapshot = (DataSnapshot) itr.next();
                        String bikerId =  childSnapshot.getKey();
                        Biker biker = childSnapshot.getValue(Biker.class);
                        if(biker!=null && biker.getLat()!=null && biker.getLng()!=null){
                            Float distance = getDistance(latLngRestaurant, biker.getLat(), biker.getLng(),tokens[0]);
                            Log.d(TAG,"Biker " + i + " found: " + bikerId + " at " + distance + " m");
                            if(distance!=null){
                                biker.setDistance(distance);
                                bikers.add(biker);
                            }
                        }
                    }
                }
                else{
                    Log.d(TAG,"No biker available found");
                }
                if(bikers.isEmpty()){
                    noBikersTv.setVisibility(View.VISIBLE);
                }
                else {
                    noBikersTv.setVisibility(View.GONE);
                    Collections.sort(bikers, new Comparator<Biker>() {
                        @Override
                        public int compare(Biker o1, Biker o2) {
                            return o1.getDistance() < o2.getDistance() ? -1
                                    : o1.getDistance() > o2.getDistance() ? 1
                                    : 0;
                        }
                    });
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG,databaseError.getMessage());
                swipeRefreshLayout.setRefreshing(false);
                noBikersTv.setVisibility(View.VISIBLE);
                noBikersTv.setText("Errore temporaneo");
            }
        });
    }

    private Float getDistance(LatLng refLocation, double latBiker, double lngBiker, String rangeDistance) {
        float[] results = new float[1]; //metres, while rangeDistance in km
        Location.distanceBetween(refLocation.latitude,refLocation.longitude,latBiker,lngBiker,results);
        Log.d(TAG,"getDistance() " + results[0] + "m >= " + rangeDistance + "km ?");
        return results[0] > Float.parseFloat(rangeDistance)*1000? null : results[0];
    }

    private LatLng getRestaurantLatLng() {
        Log.d(TAG,"setupRestaurantLocation()");
        if(restaurateur!=null && restaurateur.getRestaurantAddress()!=null) {
            return getLocationFromAddress(fragmentActivity, restaurateur.getRestaurantAddress());
        }
        return null;
    }

    private LatLng getLocationFromAddress(Context context, String strAddress){
        Geocoder coder= new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 5);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
        return p1;

    }

    private void assignOrder(final Order order) {
        final DatabaseReference databaseReference = ordersRef.child(order.getOrderId());

        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dataSnapshot.getRef().child(ORDER_DELIVERER_ID).setValue(order.getDelivererId());
                dataSnapshot.getRef().child(ORDER_STATUS).setValue(order.getOrderState());
                dataSnapshot.getRef().child(ORDER_DELIVERY_TIME).setValue(order.getDeliveryTime());
                dataSnapshot.getRef().child(ORDER_RESTAURANT_ADDRESS).setValue(restaurateur.getRestaurantAddress());
                eventListener.onAssignOrderCompleted();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
                showAlertError();
            }
        });
    }

    private void showAlertError() {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(fragmentActivity, R.style.DialogTheme);
        alertDiaolg.setTitle("Errore nell'assegnare l'ordine!"); //FIXME usare R.string.something
        alertDiaolg.setMessage("Errore temporaneo, riprovare");
        alertDiaolg.setNegativeButton("OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
        alertDiaolg.create().show();
    }

    private void confirmAssignOrder(final String delivererId) {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(fragmentActivity, R.style.DialogTheme);
        alertDiaolg.setTitle("Assegna Ordine"); //FIXME usare R.string.something
        alertDiaolg.setMessage("Confermi che vuoi assegnare l'ordine?");
        alertDiaolg.setPositiveButton("SI",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Log.d(TAG,"confirmAssignOrder " + delivererId);
                        order.setDelivererId(delivererId);
                        order.setOrderState(OrderState.ASSIGNED);
                        assignOrder(order);
                    }
                });
        alertDiaolg.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
        alertDiaolg.create().show();
    }
}
