package it.gangofheroes.foody.restaurantowners.activities;

import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.List;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.EditCategoryFragment;
import it.gangofheroes.foody.restaurantowners.fragments.EditDishFragment;
import it.gangofheroes.foody.restaurantowners.fragments.MenuFragment;
import it.gangofheroes.foody.restaurantowners.model.Category;
import it.gangofheroes.foody.restaurantowners.model.Dish;
import it.gangofheroes.foody.restaurantowners.model.Firebase;
import it.gangofheroes.foody.restaurantowners.model.Menu;

public class EditMenuOffer extends AppCompatActivity implements
        MenuFragment.MenuFragmentEventListener,
        EditCategoryFragment.EditMenuFragmentEventListener,
        EditDishFragment.EditDishFragmentEventListener {


    public static final String TAG = EditMenuOffer.class.getCanonicalName();
    private FirebaseUser firebaseUser;
    private CountDownTimer downTimeConnection;
    private Menu menu;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_menu_activity);
        setupToolbar();
        setupActivity();

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.edit_menu_toolbars);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void setupActivity() {
        startWaitingAnimation();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.
                getInstance().
                getReference(Firebase.RESTAURATEURS).
                child(firebaseUser.getUid()).
                child("menu");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    menu = dataSnapshot.getValue(Menu.class);
                    if (menu != null) {
                        try {
                            downloadImages(menu);
                        } catch (IOException e) {
                            Log.d(TAG, "unable to download image");
                        }
                    }
                    Log.d(TAG, "All restaurateur menus references on firebase found");
                }
                else{
                    menu = new Menu();
                }
                loadMenuFragment();
                cancelWaitingAnimation();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                cancelWaitingAnimation();
                finish();
                Log.d(TAG, "Controllare le regole di lettura e scrittura di firebase");
            }
        });
    }

    private void startWaitingAnimation() {
        Log.d(TAG, "startWaitingAnimation");
        downTimeConnection = new CountDownTimer(10 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                Toast.makeText(EditMenuOffer.this, "Not able to connect, try later", Toast.LENGTH_SHORT).show();
                finish();
            }
        };
        downTimeConnection.start();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loader_waiting).setVisibility(View.VISIBLE);
            }
        });
    }

    private void cancelWaitingAnimation() {
        Log.d(TAG, "cancelWaitingAnimation");
        downTimeConnection.cancel();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                findViewById(R.id.loader_waiting).setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.edit_menu_fragment_container);
        if (f instanceof EditCategoryFragment) {
            loadMenuFragment();
        } else if (f instanceof EditDishFragment) {
            getSupportFragmentManager().popBackStack();
        } else {
            cancelWaitingAnimation();
            super.onBackPressed();
        }
    }


    /*
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    * */

    @Override
    public void onDishEdit(Dish dish, int dishPosition, Category category, int categoryPosition) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.edit_menu_fragment_container,
                        EditDishFragment.newInstance(dish, dishPosition, category, categoryPosition))
                .addToBackStack(EditDishFragment.TAG)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onDishSave(Dish dish, int dishPosition, Category category, int categoryPosition) {
        int size = category.getOptions().size();
        if (size == dishPosition) {
            category.getOptions().add(dish);
        }
        else{
            category.getOptions().set(dishPosition,dish);
        }
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.edit_menu_fragment_container,
                        EditCategoryFragment.newInstance(dish, dishPosition, category, categoryPosition))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    @Override
    public void onMenuCategoryEdit(Category category, int position) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.edit_menu_fragment_container,
                        EditCategoryFragment.newInstance(category, position))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    private void loadMenuFragment() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.edit_menu_fragment_container,
                        MenuFragment.newInstance(menu))
                .commit();
    }

    /*
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     *
     * */

    @Override
    public void onCategorySave(Category category, int categoryPosition) {
        if (menu.getCategories().size() == categoryPosition) { //new element
            menu.getCategories().add(category);
        }
        uploadImages(category.getOptions());
        updateMenu();
        loadMenuFragment();
    }

    @Override
    public void onCategoryRemove(Category cRemoved) {
        updateMenu();
        deleteCategory(cRemoved);
    }

    @Override
    public void onDishRemove(Dish dish) {
        updateMenu();
        deleteDish(dish);
    }

    private void updateMenu() {

        DatabaseReference databaseReference = FirebaseDatabase.
                getInstance().
                getReference(Firebase.RESTAURATEURS).
                child(firebaseUser.getUid()).
                child("menu");
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "saving data");
                dataSnapshot.getRef().setValue(menu);
                Toast.makeText(getApplicationContext(), "Menu aggiornato!", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d(TAG, "Controllare le regole di lettura e scrittura di firebase");
            }
        });
    }

    /*
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    *
    * */

    private void downloadImages(Menu menu) throws IOException {
        StorageReference storageReference = FirebaseStorage.
                getInstance().
                getReference().
                child(firebaseUser.getUid() + "/dishes");
        for(Category c : menu.getCategories()){
            for(final Dish dish : c.getOptions()){ Log.d(TAG,"dish filename: " + dish.getFileName());
                final File file = createImageFile(dish.getFileName());
                storageReference.child(dish.getFileName()).getFile(file).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                        Log.d(TAG,"dish filename: " + dish.getFileName() + " downloaded");
                        Uri uri = FileProvider.getUriForFile(getApplicationContext(), "it.gangofheroes.foody.restaurantowners.android.fileprovider", file);
                        dish.setUriPath(uri.toString());
                    }
                });
            }
        }
    }

    private void uploadImages(List<Dish> dishes) {

        for (Dish dish : dishes) {
            String uri = dish.getUriPath();
            if(uri==null){
                return;
            }
            StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(
                    firebaseUser.getUid() +
                            "/dishes/" + dish.getFileName());
            Log.d(TAG,"dish filename: " + dish.getFileName());
            storageReference.putFile(Uri.parse(uri)).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.d(TAG,"dish uploaded");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.d(TAG, "dish upload failed");
                }
            });
        }
    }

    private void deleteCategory(final Category category) {
        for (Dish dish : category.getOptions()) {
            deleteDish(dish);
        }
    }

    private void deleteDish(final Dish dish) {
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child(
                firebaseUser.getUid() + "/" + "dishes" +
                        "/" + dish.getFileName());
        storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "dish " + dish.getFileName() + " deleted" );
            }
        });
    }

    private File createImageFile(String filename) throws IOException {
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(
                filename,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
    }
}




