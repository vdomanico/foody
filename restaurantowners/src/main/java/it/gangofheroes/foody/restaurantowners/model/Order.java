package it.gangofheroes.foody.restaurantowners.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class Order implements Parcelable {

    private String orderId;
    private String restaurateurId;
    private String delivererId;
    private String restaurantAddress;
    private String customerName;
    private String customerSurname;
    private String customerAddress;
    private String customerPhone;
    private String customerId;
    private List<DishSelection> dishes;
    private String notes;
    private OrderState orderState;
    private Long deliveryTime;

    public Order() {
    }

    private Order(Parcel in) {
        orderId = in.readString();
        restaurateurId = in.readString();
        delivererId = in.readString();
        customerId = in.readString();
        restaurantAddress = in.readString();
        customerName = in.readString();
        customerSurname = in.readString();
        customerAddress = in.readString();
        customerPhone = in.readString();
        dishes = in.createTypedArrayList(DishSelection.CREATOR);
        notes = in.readString();
        orderState = OrderState.valueOf(in.readString());
        if (in.readByte() == 0) {
            deliveryTime = null;
        } else {
            deliveryTime = in.readLong();
        }
    }

    public static final Creator<Order> CREATOR = new Creator<Order>() {
        @Override
        public Order createFromParcel(Parcel in) {
            return new Order(in);
        }

        @Override
        public Order[] newArray(int size) {
            return new Order[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderId);
        dest.writeString(restaurateurId);
        dest.writeString(delivererId);
        dest.writeString(customerId);
        dest.writeString(restaurantAddress);
        dest.writeString(customerName);
        dest.writeString(customerSurname);
        dest.writeString(customerAddress);
        dest.writeString(customerPhone);
        dest.writeTypedList(dishes);
        dest.writeString(notes);
        dest.writeString(orderState.name());
        if (deliveryTime == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(deliveryTime);
        }
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public List<DishSelection> getDishes() {
        return dishes;
    }

    public void setDishes(List<DishSelection> dishes) {
        this.dishes = dishes;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public Long getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Long deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getRestaurateurId() {
        return restaurateurId;
    }

    public void setRestaurateurId(String restaurateurId) {
        this.restaurateurId = restaurateurId;
    }

    public String getDelivererId() {
        return delivererId;
    }

    public void setDelivererId(String delivererId) {
        this.delivererId = delivererId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getRestaurantAddress() {
        return restaurantAddress;
    }

    public void setRestaurantAddress(String restaurantAddress) {
        this.restaurantAddress = restaurantAddress;
    }

    public double getAmount(){
        double amount = 0;
        if(dishes!=null && !dishes.isEmpty()){
            for(DishSelection dishSelection : dishes){
                amount+= dishSelection.getAmount();
            }
        }
        return amount;
    }
}
