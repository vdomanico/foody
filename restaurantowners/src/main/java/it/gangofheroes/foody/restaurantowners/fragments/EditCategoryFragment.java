package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.adapters.DishAdapter;
import it.gangofheroes.foody.restaurantowners.model.Category;
import it.gangofheroes.foody.restaurantowners.model.Dish;

public class EditCategoryFragment extends Fragment {

    private EditText categoryEt;
    private Dish dish;
    private int categoryPosition;
    private int dishPosition;
    private FragmentActivity fragmentActivity;

    public static Fragment newInstance(Category category, int categoryPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("CATEGORY", category);
        bundle.putInt("CATEGORY_POSITION",categoryPosition);
        EditCategoryFragment editCategoryFragment = new EditCategoryFragment();
        editCategoryFragment.setArguments(bundle);
        return editCategoryFragment;
    }

    public static Fragment newInstance(Dish dish, int dishPosition, Category category, int categoryPosition) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("DISH",dish);
        bundle.putParcelable("CATEGORY",category);
        bundle.putInt("CATEGORY_POSITION",categoryPosition);
        bundle.putInt("DISH_POSITION",dishPosition);
        EditCategoryFragment editCategoryFragment = new EditCategoryFragment();
        editCategoryFragment.setArguments(bundle);
        return editCategoryFragment;
    }

    public interface EditMenuFragmentEventListener{
        void onDishEdit(Dish dish, int dishPosition, Category category, int categoryPosition);
        void onCategorySave(Category category, int categoryPosition);
        void onDishRemove(Dish dish);
    }

    public static final String TAG = EditCategoryFragment.class.getCanonicalName();
    private EditMenuFragmentEventListener eventListener;
    private DishAdapter dishAdapter;
    private Category category;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach");
        fragmentActivity = (FragmentActivity) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        eventListener = (EditMenuFragmentEventListener) getActivity();
        fragmentActivity.setTitle(R.string.your_dishes);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.edit_category_fragment, container, false);

        if (getArguments()!=null){
            category =  getArguments().getParcelable("CATEGORY");
            dish = getArguments().getParcelable("DISH");
            categoryPosition = getArguments().getInt("CATEGORY_POSITION");
             dishPosition = getArguments().getInt("DISH_POSITION");

            setupView(view);


        }
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_editprofile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.edit_profile_done){
            if(categoryEt.getText().toString().equals("")){
                Toast.makeText(fragmentActivity, R.string.category_empty, Toast.LENGTH_SHORT).show();
                return true;
            }
            category.setCategory(categoryEt.getText().toString());
            eventListener.onCategorySave(category,categoryPosition);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupView(View view) {
        categoryEt = view.findViewById(R.id.edit_category_value);
        Button button = view.findViewById(R.id.add_category_frag);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                category.setCategory(categoryEt.getText().toString());
                eventListener.onDishEdit(null,dishAdapter.getItemCount(),category,categoryPosition);
            }
        });

        if(category!=null){
            if(category.getCategory()!=null){
                categoryEt.setText(category.getCategory());
            }
            dishAdapter = new DishAdapter(category.getOptions());
            dishAdapter.setEventListener(new DishAdapter.DishAdapterEventListener() {
                @Override
                public void onDishEditRequest(Dish dish, int position) {
                    eventListener.onDishEdit(dish,dishPosition,category,categoryPosition);
                }

                @Override
                public void onDishDelete(Dish dish, int dishPosition) {
                    eventListener.onDishRemove(dish);
                }
            });
        }
        RecyclerView recyclerView = view.findViewById(R.id.menu_categories_adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(dishAdapter);
    }
}
