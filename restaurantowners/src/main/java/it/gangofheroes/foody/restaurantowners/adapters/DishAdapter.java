package it.gangofheroes.foody.restaurantowners.adapters;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Currency;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Dish;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.ViewHolder>{

    private static final String TAG = DishAdapter.class.getCanonicalName();
    private List<Dish> dishes;
    private DishAdapterEventListener dishAdapterEventListener;

    public interface DishAdapterEventListener {
        void onDishEditRequest(Dish dish, int position);
        void onDishDelete(Dish dish, int position);
    }

    public void setEventListener(DishAdapterEventListener eventListener){
        this.dishAdapterEventListener = eventListener;
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView dishName;
        private TextView dishDesc;
        private ImageView dishImage;
        private TextView dishPrice;
        private TextView dishCurrency;
        private TextView dishQuantity;
        private ImageView removeDish;

        private ViewHolder(View v) {
            super(v);
            dishName = v.findViewById(R.id.dishNameRv);
            dishDesc = v.findViewById(R.id.dishDescRv);
            dishImage = v.findViewById(R.id.dishImageRv);
            dishPrice = v.findViewById(R.id.dishPriceRv);
            dishCurrency = v.findViewById(R.id.dishCurrencyRv);
            dishQuantity = v.findViewById(R.id.dishQuantityRv);
            removeDish = v.findViewById(R.id.removeDishRv);
        }

        private void uploadDishView(Dish dish) {
            String dishN = dish.getName();
            String dishD = dish.getDescription();
            int dishQ = dish.getQuantity();
            double dishP = dish.getPrice();
            if(dishN!=null && !dishN.equals("")){
                dishName.setText(dishN);
            }
            if(dishD!=null && !dishD.equals("")){
                dishDesc.setText(dishD);
            }
            dishPrice.setText(String.valueOf(dishP));
            dishCurrency.setText(Currency.getInstance(Locale.getDefault()).getSymbol().replaceAll("\\w", ""));
            dishQuantity.setText(String.valueOf(dishQ));
            if(dish.getUriPath()!=null) {
                dishImage.setImageURI(Uri.parse(dish.getUriPath()));
            }
        }
    }

    public DishAdapter(List<Dish> dishes) {
        this.dishes = dishes;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.item_dish, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.removeDish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    launchDialog(v,position);
            }
        });

        holder.dishImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dishAdapterEventListener.onDishEditRequest(dishes.get(position), position);
            }
        });

        holder.uploadDishView(dishes.get(position));
    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    private void launchDialog(View view, final int position) {
        AlertDialog.Builder alertDiaolg = new AlertDialog.Builder(view.getContext(), R.style.DialogTheme);
        alertDiaolg.setTitle(R.string.removeDishDialogTitle);
        alertDiaolg.setMessage(R.string.removeDishDialogMsg);
        alertDiaolg.setPositiveButton(R.string.delete,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Dish dish = dishes.remove(position);
                        notifyItemRemoved(position);
                        notifyItemRangeChanged(position,dishes.size());
                        dishAdapterEventListener.onDishDelete(dish, position);
                    }
                });
        alertDiaolg.setNegativeButton(R.string.cancel,null);
        alertDiaolg.create().show();
    }

}
