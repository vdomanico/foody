package it.gangofheroes.foody.restaurantowners.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.adapters.OrderDetailAdapter;
import it.gangofheroes.foody.restaurantowners.model.Firebase;
import it.gangofheroes.foody.restaurantowners.model.Order;
import it.gangofheroes.foody.restaurantowners.model.OrderState;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKERS;

public class OrderInfoFragment extends Fragment {

    public static final String TAG = OrderInfoFragment.class.getCanonicalName();
    private static final String KEY_ORDER = "KEY_ORDER";
    private Order order;
    private DatabaseReference bikersRef;

    public static Fragment newInstance (Order order){
        Fragment fragment = new OrderInfoFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_ORDER, order);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        if (getArguments() != null){
            order = (Order) getArguments().get(KEY_ORDER);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        return inflater.inflate(R.layout.order_info_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bikersRef = FirebaseDatabase.
                getInstance().
                getReference(BIKERS);
        Log.d(TAG, "onViewCreated");
        TextView tvDeliveryAt = view.findViewById(R.id.tvDeliveryAt);
        TextView tvCustomerName = view.findViewById(R.id.customerName);
        TextView tvCustomerPhone = view.findViewById(R.id.customerPhone);
        TextView tvDelivererName = view.findViewById(R.id.bikerName);
        TextView tvTileDelivererName = view.findViewById(R.id.tile_bikerName);
        TextView tvDelivererPhone = view.findViewById(R.id.bikerPhone);
        TextView tvTileDelivererPhone = view.findViewById(R.id.tile_bikerphone);
        ListView listView = view.findViewById(R.id.lv);
        TextView tvTotalPrice = view.findViewById(R.id.dishes_total_price);
        if(order!=null){
            String name = order.getCustomerName() + " " +  order.getCustomerSurname();
            tvCustomerName.setText(name);
            tvCustomerPhone.setText(order.getCustomerPhone());
            tvDeliveryAt.setText(order.getCustomerAddress());
            tvTotalPrice.setText(String.valueOf(order.getAmount()));
            if(order.getOrderState()!= OrderState.NEW){
                tvDelivererName.setVisibility(View.VISIBLE);
                tvDelivererPhone.setVisibility(View.VISIBLE);
                tvTileDelivererName.setVisibility(View.VISIBLE);
                tvTileDelivererPhone.setVisibility(View.VISIBLE);
                if(order.getDelivererId()!=null)
                getDelivererInfo(order.getDelivererId(), tvDelivererName, tvDelivererPhone);
            }
        }

        listView.setAdapter(new OrderDetailAdapter(getContext(),order.getDishes()));

    }

    private void getDelivererInfo(String delivererId, final TextView tvDelivererName, final TextView tvDelivererPhone) {
        bikersRef.child(delivererId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //TODO
                if(dataSnapshot.exists()){
                    String bikerName = dataSnapshot.child(Firebase.BIKER_NAME).getValue(String.class);
                    String bikerSurname = dataSnapshot.child(Firebase.BIKER_SURNAME).getValue(String.class);
                    String bikerPhone = dataSnapshot.child(Firebase.BIKER_PHONE).getValue(String.class);
                    String bikerMail = dataSnapshot.child(Firebase.BIKER_MAIL).getValue(String.class);
                    String fullName = bikerName + " " + bikerSurname;
                    tvDelivererName.setText(fullName);
                    tvDelivererPhone.setText(bikerPhone);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
    }
}
