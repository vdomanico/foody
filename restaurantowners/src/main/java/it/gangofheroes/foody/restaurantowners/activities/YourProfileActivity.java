package it.gangofheroes.foody.restaurantowners.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.EditYourProfileFragment;
import it.gangofheroes.foody.restaurantowners.fragments.ShowYourProfileFragment;

public class YourProfileActivity extends AppCompatActivity implements
        ShowYourProfileFragment.ShowYourProfileFragmentEventListener,
        EditYourProfileFragment.EditYourProfileFragmentEventListener
 {

     private static final String TAG = YourProfileActivity.class.getCanonicalName();
     @Override
     protected void onCreate(@Nullable Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.your_profile_activity);
         setupToolbar();
         showYourProfileFragment();

         if(getIntent().getExtras()!=null){
             String value = getIntent().getExtras().getString("EDIT");
             if(value!=null && value.equals("EDIT")){
                 editYourProfileFragment();
             }
         }

     }

     private void setupToolbar() {
         Toolbar toolbar = findViewById(R.id.profile_toolbar);
         setSupportActionBar(toolbar);
         if(getSupportActionBar() != null) {
             getSupportActionBar().setDisplayHomeAsUpEnabled(true);
             getSupportActionBar().setDisplayShowHomeEnabled(true);
         }
     }

     @Override
     public boolean onSupportNavigateUp() {
         if(getSupportFragmentManager().getBackStackEntryCount()<=1){
             finish();
         }
         else{
             getSupportFragmentManager().popBackStack();
         }
         return true;
     }

     @Override
     public void onBackPressed() {
         if(getSupportFragmentManager().getBackStackEntryCount()<=1){
             finish();
         }
         else{
             getSupportFragmentManager().popBackStack();
         }
     }

     @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         return super.onCreateOptionsMenu(menu);
     }

     private void showYourProfileFragment(){
        Log.d(TAG,"showYourProfileFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentYourProfile);
        if (!(f instanceof ShowYourProfileFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentYourProfile,
                            ShowYourProfileFragment.newInstance())
                    .addToBackStack(ShowYourProfileFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    private void editYourProfileFragment(){
        Log.d(TAG,"editYourProfileFragment()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentYourProfile);
        if (!(f instanceof EditYourProfileFragment)){
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragmentYourProfile,
                            EditYourProfileFragment.newInstance())
                    .addToBackStack(EditYourProfileFragment.TAG)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

    @Override
    public void onProfileEdit() {
        Log.d(TAG,"onProfileEdit()");
        editYourProfileFragment();
    }

    @Override
    public void onProfileSave() {
        Log.d(TAG,"onProfileSave()");
        onBackPressed();
    }
}




