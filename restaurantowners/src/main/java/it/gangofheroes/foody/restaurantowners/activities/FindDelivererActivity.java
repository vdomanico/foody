package it.gangofheroes.foody.restaurantowners.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.fragments.BikerListFragment;

public class FindDelivererActivity extends AppCompatActivity implements
        BikerListFragment.BikerListFragmentEventListener {

    private static final String TAG = FindDelivererActivity.class.getCanonicalName();
    private String orderId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_deliverer_activity);
        setupToolbar();
        orderId = getIntent().getStringExtra("ORDER_ID");
        startBikerListFragmentActivity();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.find_deliverer_toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void startBikerListFragmentActivity(){
        Log.d(TAG,"startBikersListFragmentActivity()");
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.fragmentFindDeliverer);
        if (!(f instanceof BikerListFragment)){
            if(orderId!=null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentFindDeliverer,
                                BikerListFragment.newInstance(orderId))
                        .addToBackStack(BikerListFragment.TAG)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>0){
            getSupportFragmentManager().popBackStack();
        }
        super.onBackPressed();
    }

    @Override
    public void onAssignOrderCompleted() {
        Intent data = new Intent();
        setResult(RESULT_OK, data);
        finish();
    }
}
