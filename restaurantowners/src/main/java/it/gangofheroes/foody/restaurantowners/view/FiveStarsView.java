package it.gangofheroes.foody.restaurantowners.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import it.gangofheroes.foody.restaurantowners.R;

public class FiveStarsView extends LinearLayout {

    private ImageView starPos0Iv;
    private ImageView starPos1Iv;
    private ImageView starPos2Iv;
    private ImageView starPos3Iv;
    private ImageView starPos4Iv;

    public FiveStarsView(Context context) {
        super(context);
    }

    public FiveStarsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.five_stars, this);
        initStars();
        cleanStars();
    }

    private void initStars(){
        starPos0Iv = findViewById(R.id.star_pos_0);
        starPos1Iv = findViewById(R.id.star_pos_1);
        starPos2Iv = findViewById(R.id.star_pos_2);
        starPos3Iv = findViewById(R.id.star_pos_3);
        starPos4Iv = findViewById(R.id.star_pos_4);
    }

    public void setStars(int stars){

        cleanStars();

        if(stars==0) {
            starPos0Iv.setTag(R.drawable.ic_star_border);
            starPos1Iv.setTag(R.drawable.ic_star_border);
            starPos2Iv.setTag(R.drawable.ic_star_border);
            starPos3Iv.setTag(R.drawable.ic_star_border);
            starPos4Iv.setTag(R.drawable.ic_star_border);
        }
        else if(stars==1){
            starPos0Iv.setImageResource(R.drawable.ic_star_full);
            starPos0Iv.setTag(R.drawable.ic_star_full);
            starPos1Iv.setTag(R.drawable.ic_star_border);
            starPos2Iv.setTag(R.drawable.ic_star_border);
            starPos3Iv.setTag(R.drawable.ic_star_border);
            starPos4Iv.setTag(R.drawable.ic_star_border);

        }else if(stars == 2){
            starPos0Iv.setImageResource(R.drawable.ic_star_full);
            starPos1Iv.setImageResource(R.drawable.ic_star_full);
            starPos0Iv.setTag(R.drawable.ic_star_full);
            starPos1Iv.setTag(R.drawable.ic_star_full);
            starPos2Iv.setTag(R.drawable.ic_star_border);
            starPos3Iv.setTag(R.drawable.ic_star_border);
            starPos4Iv.setTag(R.drawable.ic_star_border);
        }else if(stars == 3){
            starPos0Iv.setImageResource(R.drawable.ic_star_full);
            starPos1Iv.setImageResource(R.drawable.ic_star_full);
            starPos2Iv.setImageResource(R.drawable.ic_star_full);
            starPos0Iv.setTag(R.drawable.ic_star_full);
            starPos1Iv.setTag(R.drawable.ic_star_full);
            starPos2Iv.setTag(R.drawable.ic_star_full);
            starPos3Iv.setTag(R.drawable.ic_star_border);
            starPos4Iv.setTag(R.drawable.ic_star_border);
        }else if(stars==4){
            starPos0Iv.setImageResource(R.drawable.ic_star_full);
            starPos1Iv.setImageResource(R.drawable.ic_star_full);
           starPos2Iv.setImageResource(R.drawable.ic_star_full);
            starPos3Iv.setImageResource(R.drawable.ic_star_full);
            starPos0Iv.setTag(R.drawable.ic_star_full);
            starPos1Iv.setTag(R.drawable.ic_star_full);
            starPos2Iv.setTag(R.drawable.ic_star_full);
            starPos3Iv.setTag(R.drawable.ic_star_full);
            starPos4Iv.setTag(R.drawable.ic_star_border);
        }else if(stars == 5){
            starPos0Iv.setImageResource(R.drawable.ic_star_full);
            starPos1Iv.setImageResource(R.drawable.ic_star_full);
            starPos2Iv.setImageResource(R.drawable.ic_star_full);
            starPos3Iv.setImageResource(R.drawable.ic_star_full);
            starPos4Iv.setImageResource(R.drawable.ic_star_full);
            starPos0Iv.setTag(R.drawable.ic_star_full);
            starPos1Iv.setTag(R.drawable.ic_star_full);
            starPos2Iv.setTag(R.drawable.ic_star_full);
            starPos3Iv.setTag(R.drawable.ic_star_full);
            starPos4Iv.setTag(R.drawable.ic_star_full);
        }
    }

    private void cleanStars() {
        starPos0Iv.setTag(R.drawable.ic_star_border);
        starPos1Iv.setTag(R.drawable.ic_star_border);
        starPos2Iv.setTag(R.drawable.ic_star_border);
        starPos3Iv.setTag(R.drawable.ic_star_border);
        starPos4Iv.setTag(R.drawable.ic_star_border);
        starPos0Iv.setImageResource(R.drawable.ic_star_border);
        starPos1Iv.setImageResource(R.drawable.ic_star_border);
        starPos2Iv.setImageResource(R.drawable.ic_star_border);
        starPos3Iv.setImageResource(R.drawable.ic_star_border);
        starPos4Iv.setImageResource(R.drawable.ic_star_border);
    }

    public int getStars(){
        Integer pos0int = (Integer) starPos0Iv.getTag();
        Integer pos1int = (Integer) starPos1Iv.getTag();
        Integer pos2int = (Integer) starPos2Iv.getTag();
        Integer pos3int = (Integer) starPos3Iv.getTag();
        Integer pos4int = (Integer) starPos4Iv.getTag();

       if(pos4int.equals(R.drawable.ic_star_full)){
           return 5;
       }else if(pos3int.equals(R.drawable.ic_star_full)){
           return 4;
       }else if(pos2int.equals(R.drawable.ic_star_full)){
           return 3;
       }else if(pos1int.equals(R.drawable.ic_star_full)){
           return 2;
       }else if(pos0int.equals(R.drawable.ic_star_full)){
           return 1;
       }else{
           return 0;
       }
    }

    public void setStarsClickable(boolean b) {
        starPos0Iv.setClickable(b);
        starPos1Iv.setClickable(b);
        starPos2Iv.setClickable(b);
        starPos3Iv.setClickable(b);
        starPos4Iv.setClickable(b);
    }

    private OnClickListener starClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            ImageView imageView = (ImageView) v;
            Integer integer = (Integer) imageView.getTag();
            integer = integer == null ? 0 : integer;

            Integer pos2int = (Integer) starPos2Iv.getTag();
            Integer pos3int = (Integer) starPos3Iv.getTag();
            Integer pos4int = (Integer) starPos4Iv.getTag();

            switch(integer) {
                //voglio riempire le stelle da pos0 fino a pos in question selezionata
                case R.drawable.ic_star_border:

                    if(imageView.equals(starPos4Iv)){
                        starPos0Iv.setImageResource(R.drawable.ic_star_full);
                        starPos1Iv.setImageResource(R.drawable.ic_star_full);
                        starPos2Iv.setImageResource(R.drawable.ic_star_full);
                        starPos3Iv.setImageResource(R.drawable.ic_star_full);
                        starPos4Iv.setImageResource(R.drawable.ic_star_full);
                        starPos0Iv.setTag(R.drawable.ic_star_full);
                        starPos1Iv.setTag(R.drawable.ic_star_full);
                        starPos2Iv.setTag(R.drawable.ic_star_full);
                        starPos3Iv.setTag(R.drawable.ic_star_full);
                        starPos4Iv.setTag(R.drawable.ic_star_full);
                    }
                    else if(imageView.equals(starPos3Iv)){
                        starPos0Iv.setImageResource(R.drawable.ic_star_full);
                        starPos1Iv.setImageResource(R.drawable.ic_star_full);
                        starPos2Iv.setImageResource(R.drawable.ic_star_full);
                        starPos3Iv.setImageResource(R.drawable.ic_star_full);
                        starPos0Iv.setTag(R.drawable.ic_star_full);
                        starPos1Iv.setTag(R.drawable.ic_star_full);
                        starPos2Iv.setTag(R.drawable.ic_star_full);
                        starPos3Iv.setTag(R.drawable.ic_star_full);
                    }
                    else if(imageView.equals(starPos2Iv)){
                        starPos0Iv.setImageResource(R.drawable.ic_star_full);
                        starPos1Iv.setImageResource(R.drawable.ic_star_full);
                        starPos2Iv.setImageResource(R.drawable.ic_star_full);
                        starPos0Iv.setTag(R.drawable.ic_star_full);
                        starPos1Iv.setTag(R.drawable.ic_star_full);
                        starPos2Iv.setTag(R.drawable.ic_star_full);
                    }
                    else if(imageView.equals(starPos1Iv)){
                        starPos0Iv.setImageResource(R.drawable.ic_star_full);
                        starPos1Iv.setImageResource(R.drawable.ic_star_full);
                        starPos0Iv.setTag(R.drawable.ic_star_full);
                        starPos1Iv.setTag(R.drawable.ic_star_full);
                    }
                    else if(imageView.equals(starPos0Iv)){
                        starPos0Iv.setImageResource(R.drawable.ic_star_full);
                        starPos0Iv.setTag(R.drawable.ic_star_full);
                    }
                    break;

                //voglio svuotare le stelle da last pos fino a pos selezionata esclusa
                case R.drawable.ic_star_full:
                    if(imageView.equals(starPos0Iv)){
                        starPos1Iv.setImageResource(R.drawable.ic_star_border);
                        starPos2Iv.setImageResource(R.drawable.ic_star_border);
                        starPos3Iv.setImageResource(R.drawable.ic_star_border);
                        starPos4Iv.setImageResource(R.drawable.ic_star_border);
                        starPos1Iv.setTag(R.drawable.ic_star_border);
                        starPos2Iv.setTag(R.drawable.ic_star_border);
                        starPos3Iv.setTag(R.drawable.ic_star_border);
                        starPos4Iv.setTag(R.drawable.ic_star_border);
                    }
                    else if(imageView.equals(starPos1Iv)){
                        if(pos2int!=null && pos2int==R.drawable.ic_star_border){
                            starPos1Iv.setImageResource(R.drawable.ic_star_border);
                            starPos1Iv.setTag(R.drawable.ic_star_border);
                        }
                        starPos1Iv.setImageResource(R.drawable.ic_star_border);
                        starPos1Iv.setTag(R.drawable.ic_star_border);
                        starPos2Iv.setImageResource(R.drawable.ic_star_border);
                        starPos3Iv.setImageResource(R.drawable.ic_star_border);
                        starPos4Iv.setImageResource(R.drawable.ic_star_border);
                        starPos2Iv.setTag(R.drawable.ic_star_border);
                        starPos3Iv.setTag(R.drawable.ic_star_border);
                        starPos4Iv.setTag(R.drawable.ic_star_border);
                    }
                    else if(imageView.equals(starPos2Iv)){
                        if(pos3int!=null && pos3int==R.drawable.ic_star_border){
                            starPos2Iv.setImageResource(R.drawable.ic_star_border);
                            starPos2Iv.setTag(R.drawable.ic_star_border);
                        }
                        starPos2Iv.setImageResource(R.drawable.ic_star_border);
                        starPos2Iv.setTag(R.drawable.ic_star_border);
                        starPos3Iv.setImageResource(R.drawable.ic_star_border);
                        starPos4Iv.setImageResource(R.drawable.ic_star_border);
                        starPos3Iv.setTag(R.drawable.ic_star_border);
                    }
                    else if(imageView.equals(starPos3Iv)){
                        if(pos4int!=null && pos4int==R.drawable.ic_star_border){
                            starPos3Iv.setImageResource(R.drawable.ic_star_border);
                            starPos3Iv.setTag(R.drawable.ic_star_border);
                        }
                        starPos3Iv.setImageResource(R.drawable.ic_star_border);
                        starPos3Iv.setTag(R.drawable.ic_star_border);
                        starPos4Iv.setImageResource(R.drawable.ic_star_border);
                        starPos4Iv.setTag(R.drawable.ic_star_border);
                    }
                    else if(imageView.equals(starPos4Iv)){
                        starPos4Iv.setImageResource(R.drawable.ic_star_border);
                        starPos4Iv.setTag(R.drawable.ic_star_border);
                    }
                    break;
            }
        }
    };

    public void enableListener(boolean b) {
        if(b){
            starPos0Iv.setOnClickListener(starClickListener);
            starPos1Iv.setOnClickListener(starClickListener);
            starPos2Iv.setOnClickListener(starClickListener);
            starPos3Iv.setOnClickListener(starClickListener);
            starPos4Iv.setOnClickListener(starClickListener);
        }
    }
}
