package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Iterator;
import java.util.List;
import java.util.Random;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Restaurateur;

import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKERS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKER_STATUS;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.BIKER_STATUS_AVAILABLE;
import static it.gangofheroes.foody.restaurantowners.model.Firebase.RESTAURATEURS;

public class MapsFragmentActivity extends Fragment implements OnMapReadyCallback {

    public static final String TAG = MapsFragmentActivity.class.getCanonicalName();
    private FragmentActivity fragmentActivity;
    private MapsFragmentActivityEventListener eventListener;
    private MapView mapView;
    private GoogleMap gMap;
    private DatabaseReference bikersRef;
    private DatabaseReference restaurantsRef;
    private FirebaseUser firebaseUser;
    private Restaurateur restaurateur;

    public interface MapsFragmentActivityEventListener{
        void onListViewRequest();
    }

    public static Fragment newInstance() {
        return new MapsFragmentActivity();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        fragmentActivity = (FragmentActivity) context;
        eventListener = (MapsFragmentActivityEventListener) context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate()");
        fragmentActivity.setTitle(R.string.find_biker);
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        bikersRef = FirebaseDatabase.
                getInstance().
                getReference(BIKERS);
        restaurantsRef = FirebaseDatabase.
                getInstance().
                getReference(RESTAURATEURS);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        eventListener.onListViewRequest();
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.find_delivery_map_fragment, container, false);
        mapView = view.findViewById(R.id.gmap);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.d(TAG,"onViewCreated()");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG,"onMapReady()");
        gMap = googleMap;
        restaurantsRef.child(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                restaurateur = dataSnapshot.getValue(Restaurateur.class);
                setupRestaurantLocation();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
    }

    private void setupRestaurantLocation() {
        Log.d(TAG,"setupRestaurantLocation()");
        if(restaurateur!=null && restaurateur.getRestaurantAddress()!=null){
            LatLng loc = getLocationFromAddress(fragmentActivity,restaurateur.getRestaurantAddress());
            if(loc!=null){
                gMap.addMarker(new MarkerOptions().position(loc).title(restaurateur.getRestaurantName()));
                gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,16.0f));
            }
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private void findBikers() {
        Query query = bikersRef.orderByChild(BIKER_STATUS).equalTo(BIKER_STATUS_AVAILABLE);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int bikers = (int) dataSnapshot.getChildrenCount();
                if(bikers>0){
                    Random rand = new Random();
                    int n = rand.nextInt(bikers);
                    Iterator itr = dataSnapshot.getChildren().iterator();
                    for(int i = 0; i < n; i++) {
                        itr.next();
                    }
                    DataSnapshot childSnapshot = (DataSnapshot) itr.next();
                    String bikerId =  childSnapshot.getKey();
                    Log.d(TAG,"Biker available found: " + bikerId);
                }
                else{
                    Toast.makeText(fragmentActivity, "Nessun biker disponibile!", Toast.LENGTH_LONG).show();
                    Log.d(TAG,"No biker found");
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG,databaseError.getMessage());
            }
        });
    }

    private LatLng getLocationFromAddress(Context context, String strAddress){
        Geocoder coder= new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try
        {
            address = coder.getFromLocationName(strAddress, 5);
            if(address==null)
            {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();
            p1 = new LatLng(location.getLatitude(), location.getLongitude());
        }
        catch (Exception e)
        {
            Log.e(TAG,e.getMessage());
        }
        return p1;

    }

}
