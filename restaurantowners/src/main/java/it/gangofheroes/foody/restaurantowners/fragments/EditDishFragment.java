package it.gangofheroes.foody.restaurantowners.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import java.util.Currency;
import java.util.Locale;

import it.gangofheroes.foody.restaurantowners.R;
import it.gangofheroes.foody.restaurantowners.model.Category;
import it.gangofheroes.foody.restaurantowners.model.Dish;

import static android.app.Activity.RESULT_OK;

public class EditDishFragment extends Fragment {

    public interface EditDishFragmentEventListener {
        void onDishSave(Dish dish, int dishPosition, Category category, int categoryPosition);
    }

    public static EditDishFragment newInstance(Dish dish, int dishPosition, Category category, int categoryPosition){
        EditDishFragment dishFragment = new EditDishFragment();
        Bundle args = new Bundle();
        args.putParcelable("DISH",dish);
        args.putInt("DISH_POSITION",dishPosition);
        args.putParcelable("CATEGORY",category);
        args.putInt("CATEGORY_POSITION",categoryPosition);
        dishFragment.setArguments(args);
        return dishFragment;
    }

    public static final String TAG = EditDishFragment.class.getCanonicalName();
    private static final int REQUEST_IMAGE_GALLERY = 11;
    private Uri dishUri;
    private EditText dishName;
    private ImageView dishImage;
    private EditText dishDesc;
    private EditText dishPrice;
    private EditText dishQuantity;
    private EditDishFragmentEventListener eventListener;
    private int dishPosition;
    private Category category;
    private int categoryPosition;
    private FragmentActivity fragmentActivity;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        setHasOptionsMenu(true);
        eventListener = (EditDishFragmentEventListener) getActivity();
        fragmentActivity = (FragmentActivity) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_dish_fragment,container,false);
        setupViews(v);
        if(getArguments()!=null){
            Dish oldDish = (Dish) getArguments().get("DISH");
            if(oldDish!=null){
                uploadDishView(oldDish);
            }
            dishPosition = getArguments().getInt("DISH_POSITION");
            category = getArguments().getParcelable("CATEGORY");
            categoryPosition = getArguments().getInt("CATEGORY_POSITION");
        }
        dishImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPictureFromGallery();
            }
        });
        return v;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_editprofile, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        super.onOptionsItemSelected(item);
        if(item.getItemId() == R.id.edit_profile_done){
            Log.d(TAG,"CATEGORIA category position:" +categoryPosition);
            if(dishName.getText().toString().equals("") ||
            dishDesc.getText().toString().equals("") ||
            dishPrice.getText().toString().equals("") ||
            dishQuantity.getText().toString().equals("")) {
                Toast.makeText(fragmentActivity, R.string.edit_dish_empty, Toast.LENGTH_SHORT).show();
                return true;
            }

            eventListener.onDishSave(updateDish(), dishPosition, category, categoryPosition);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupViews(View v) {
        dishName  = v.findViewById(R.id.dishNameFragment);
        dishDesc  = v.findViewById(R.id.dishDescFragment);
        dishPrice  = v.findViewById(R.id.dishPriceFragment);
        dishQuantity  = v.findViewById(R.id.dishQuantityFragment);
        dishImage = v.findViewById(R.id.dishPicFragment);
    }


    private void selectPictureFromGallery() {
        Intent selectPictureIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        selectPictureIntent.setType("image/*");
        startActivityForResult(selectPictureIntent, REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK && data != null) {
            dishUri = data.getData();
            if (dishUri != null) {
                dishImage.setImageURI(dishUri);
            }
        }
    }

    private Dish updateDish(){
        Dish dish = new Dish();
        double p = 0;
        int q = 0;
        try {
            p = Double.parseDouble((dishPrice.getText().toString().split(" "))[0]);
            q = Integer.parseInt(dishQuantity.getText().toString());
        }catch (Exception e){
            //TODO
        }finally {
            dish.setName(dishName.getText().toString());
            dish.setDescription(dishDesc.getText().toString());
            dish.setPrice(p);
            dish.setQuantity(q);
            if(dishUri!=null){
                dish.setUriPath(dishUri.toString());
            }
            else {
                dish.setUriPath(null);
            }

        }
        return dish;
    }

    private void uploadDishView(Dish dish) {
        String dishN = dish.getName();
        String dishD = dish.getDescription();
        int dishQ = dish.getQuantity();
        double dishP = dish.getPrice();
        String price = String.valueOf(dishP)+ " " + Currency.getInstance(Locale.getDefault()).getSymbol().replaceAll("\\w", "");
        dishName.setText(dishN);
        dishDesc.setText(dishD);
        dishPrice.setText(price);
        dishQuantity.setText(String.valueOf(dishQ));
        if(dish.getUriPath()!=null) {
            dishUri = Uri.parse(dish.getUriPath());
            dishImage.setImageURI(dishUri);
        }

    }
}
