package it.gangofheroes.foody.restaurantowners.model;

public enum UserType {
    RESTAURATEUR, CUSTOMER, BIKER
}
